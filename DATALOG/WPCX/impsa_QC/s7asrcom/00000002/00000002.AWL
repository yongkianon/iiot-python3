FUNCTION "AI_Profi" : VOID
TITLE =read analog inputs
// AI:PROFI    // read in AI, PROFIBUS-DP mod.
FAMILY : LCgener
NAME : AI_PROFI
VERSION : 0.0
//KNOW_HOW_PROTECT

VAR_INPUT
  AnalogIn : INT ;	// analog input value
  WAGO : BOOL ;	// 0=Siemens module 1=WAGO
  BRK : BOOL ;	// 1=wire break monitoring
  CHCKbit : BOOL ;	// check routine is active
  CHCKval : INT ;	// outputvalue if checkroutine is ON
END_VAR
VAR_OUTPUT
  OFLW : BOOL ;	// 1=overflow fault
  WBRK : BOOL ;	// 1=wire break
  WARN : BOOL ;	// 1=warning(out of rated range)
  Output : INT ;	//output signal (converted to 32760)
END_VAR
VAR_TEMP
  Word_Inp : WORD ;	// analog input WORD variable
  Help_Out : INT ;	// memory for conversion
  H_OFlw : BOOL ;	// help flag overflow 
  H_UFlw : BOOL ;	// help flag underflow
  pos_Val : BOOL ;	// positive input value 
END_VAR
BEGIN
NETWORK
TITLE = segm.1 : ET200M/ET200S AI modules
//
//FC 52: The function block reads in 1 analog input chanel
//       for a PROFIBUS-DB module.
//
//the input value becomes converted to +/-32760, it is transferred to
//=DBOU (datablock), =OADR (DW output-address)
//
//
//The FC52 can be used for Siemens ET200M/ET200S AI-modules or WAGO AI-modules.
//If the parameter =S/W is set to '0', the ET200M/ET200S segment is active, else
//the WAGO segment is active.
//
//ET200M/ET200S:
//=============
//
//!!! Select S7-Format with the COM ET200WIN software/ S7 Hardwarekonfigur.  !!!!
//
//the FB can be used for all measuring ranges (+/-10V, 0...20mA, 4..20mA)
//the input is coded as a 2th_complement, bit15= sign, the databits are shift left
//that means, if the resolution is 12bit+sign, the bits 0..2 are not valid
//=> the rated range of the input values is always +/- 27648
//if the power supply L+ fails, the input value is KH 7FFF (32767)
//
//4..20mA: if I < 4mA, the input is negative!    (4mA:input = 0)
//         input = 8000h indicates wire break
//
//monitoring of the ET200 AI-inputs:
//- overflow or power supply failure:
//  if input = KH 7FFF or KH 8000 =>  set fault parameter #OFLW, output = 0
//  (if wire-break monitoring is selected, KH 8000 sets the output #WBRK instead  
//  of #OFLW) 
//- wire break (only active, if parameter #BRK is set to rlo1):
//  => if input < 0 => output = 0  
//  => if input = KH 8000   => set fault param. =WBRK, output = 0
//
//- input outside the rated range (>10V or >20mA):
//  => if input > 27648, set warning parameter =WARN, output limited to +/-32760
//
//
//The ET200 input value becomes converted to the range +/- 32760!!!
//-------------------------------------------------------------------
//
//
//WAGO-modules:
//=============
//the WAGO AI modules have a resolution of 12bit (left hand), the bits 0...2
//are not valid.
//that means : the rated input (20mA, 10V) correspond to KF+32760
//(4mA: input = 0)
//
//no 'overflow' possible
//
//(the PROFIBUS-configuration for the WAGO modules must be without STATUS-byte)
//
//
//
//
//
//
//
//
////;
      CLR   ; 
      =     #OFLW; // reset ET200
      =     #WBRK; // fault outputs
      =     #WARN; 
      =     #H_OFlw; 
      =     #H_UFlw; 
//;
      L     #AnalogIn; // read  analog signal (INT)
      T     #Word_Inp; // transfer to WORD  variable
      A     #WAGO; // ET200 or WAGO modul?
      JC    NW_E; // => jump to segment 2
//;

      L     #AnalogIn; 
      L     W#16#7FFF; // overflow+ or supply L+ fails
      ==I   ; 
      =     #H_OFlw; // overflow or supply fails
//
      L     #AnalogIn; 
      L     W#16#8000; // overflow-
      ==I   ; 
      =     #H_UFlw; // wire break or underflow
//
      O(    ; 
      AN    #H_OFlw; // no fault
      AN    #H_UFlw; 
      )     ; 
      O(    ; // or underflow with wire break
      A     #H_UFlw; //  monitoring
      A     #BRK; 
      )     ; 
      JC    SUV2; // =>jump to next supervision
      =     #OFLW; // else: set fault-bit 'overflow'
      L     32760; 
      T     #Help_Out; // set input to '0', if overflow
      JU    NW_E; 
//;
SUV2: AN    #BRK; // jump, if no wire break monit.
      JC    SUV3; // (only 4...20mA modules!)
      L     #AnalogIn; 
      L     0; // negative inputs if I < 4mA
      >=I   ; 
      JC    SUV3; // jump, if no fault
      L     0; // no negative values allowed=>
      T     #Help_Out; // set input to '0'
      A     #H_UFlw; // input = 8000h
      =     #WBRK; // fault-info wire break
      JU    NW_E; 
//;
SUV3: L     0; 
      L     #AnalogIn; // positive input value?
      T     #Help_Out; 
      <=I   ; 
      =     #pos_Val; 
      JC    POSI; 
      NEGR  ; // else: make positive
POSI: L     27648; // end of rated range ET200
      <=I   ; 
      JC    OKAY; 
      =     #WARN; // warning: AI > 10V, 20mA...
      T     #Help_Out; // limitation of input signal
OKAY: A     #pos_Val; // positive input?
      JC    MULT; 
      L     #Help_Out; 
      NEGR  ; // make negative again
      T     #Help_Out; 
MULT: L     #Help_Out; // max. +/- 27648
      ITD   ; 
      DTR   ; // integer to floating point
      L     1.184896e+000; // multipl.factor
      *R    ; 
      RND   ; // floating point to integer
      T     #Help_Out; // converted AI value max. +/-32760
NW_E: NOP   0; 
NETWORK
TITLE = WAGO AI modul

      AN    #WAGO; // no WAGO selected?
      JC    Sg_E; //NW.E;        // => jump
      L     #Word_Inp; // WAGO input , 12bit resolution
      L     2#1111111111111000; // bit 0..2 not valid
      AW    ; 
      T     #Help_Out; // max. +/- 32760

Sg_E: NOP   0; 
NETWORK
TITLE = output to datablock

      L     #Help_Out; // converted input value
      AN    #CHCKbit; 
      JC    Tout; 
      L     #CHCKval; 
Tout: T     #Output; // transfer to datablock


END_FUNCTION

