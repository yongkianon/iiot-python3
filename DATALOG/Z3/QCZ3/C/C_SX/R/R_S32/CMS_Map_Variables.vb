
(*Group:CMS_Map_Variables*)


VAR_GLOBAL
	ESTP_CMS001	AT %MX1.3500.0 :	BOOL;(*Emergency Stop:PLC/CMS panel*)
	ESTP_CMS002	AT %MX1.3500.1 :	BOOL;(*Emergency Stop:E_room*)
	ESTP_CMS003	AT %MX1.3500.2 :	BOOL;(*Emergency Stop:M_house#1*)
	ESTP_CMS004	AT %MX1.3500.3 :	BOOL;(*Emergency Stop:M_house#2*)
	ESTP_CMS005	AT %MX1.3500.4 :	BOOL;(*Emergency Stop:M_house#3*)
	ESTP_CMS006	AT %MX1.3500.5 :	BOOL;(*Emergency Stop:M_house#4*)
	ESTP_CMS007	AT %MX1.3500.6 :	BOOL;(*Emergency Stop:M_room control station*)
	ESTP_CMS008	AT %MX1.3500.7 :	BOOL;(*Emergency Stop:Cab panel*)
	ESTP_CMS009	AT %MX1.3500.8 :	BOOL;(*Emergency Stop:RC console*)
	ESTP_CMS010	AT %MX1.3500.9 :	BOOL;(*Emergency Stop:TLS control station*)
	ESTP_CMS011	AT %MX1.3500.10 :	BOOL;(*Emergency Stop:Trolley emergency station*)
	ESTP_CMS012	AT %MX1.3500.11 :	BOOL;(*Emergency Stop:Boom control station*)
	ESTP_CMS013	AT %MX1.3500.12 :	BOOL;(*Emergency Stop:Boom tip*)
	ESTP_CMS014	AT %MX1.3500.13 :	BOOL;(*Emergency Stop:Gantry landside#1*)
	ESTP_CMS015	AT %MX1.3500.14 :	BOOL;(*Emergency Stop:Gantry landside#2*)
	ESTP_CMS016	AT %MX1.3500.15 :	BOOL;(*Emergency Stop:Gantry landside#3*)
	ESTP_CMS017	AT %MX1.3501.0 :	BOOL;(*Emergency Stop:Gantry waterside#1*)
	ESTP_CMS018	AT %MX1.3501.1 :	BOOL;(*Emergency Stop:Gantry waterside#2*)
	ESTP_CMS019	AT %MX1.3501.2 :	BOOL;(*Emergency Stop:Gantry waterside#3*)
	ESTP_CMS020	AT %MX1.3501.3 :	BOOL;(*Emergency Stop:Gantry control station*)
	ESTP_CMS021	AT %MX1.3501.4 :	BOOL;
	ESTP_CMS022	AT %MX1.3501.5 :	BOOL;
	ESTP_CMS023	AT %MX1.3501.6 :	BOOL;
	ESTP_CMS024	AT %MX1.3501.7 :	BOOL;
	ESTP_CMS025	AT %MX1.3501.8 :	BOOL;
	ESTP_CMS026	AT %MX1.3501.9 :	BOOL;
	ESTP_CMS027	AT %MX1.3501.10 :	BOOL;
	ESTP_CMS028	AT %MX1.3501.11 :	BOOL;
	ESTP_CMS029	AT %MX1.3501.12 :	BOOL;
	ESTP_CMS030	AT %MX1.3501.13 :	BOOL;
	ESTP_CMS031	AT %MX1.3501.14 :	BOOL;
	ESTP_CMS032	AT %MX1.3501.15 :	BOOL;
	CRANE_CMS001	AT %MX1.3200.0 :	BOOL;
	CRANE_CMS002	AT %MX1.3200.1 :	BOOL;
	CRANE_CMS003	AT %MX1.3200.2 :	BOOL;
	CRANE_CMS004	AT %MX1.3200.3 :	BOOL;
	CRANE_CMS005	AT %MX1.3200.4 :	BOOL;
	CRANE_CMS006	AT %MX1.3200.5 :	BOOL;(*All command off*)
	CRANE_CMS007	AT %MX1.3200.6 :	BOOL;(*No e_stop*)
	CRANE_CMS008	AT %MX1.3200.7 :	BOOL;(*Emergency stop safety relay FB*)
	CRANE_CMS009	AT %MX1.3200.8 :	BOOL;(*All control CB on*)
	CRANE_CMS010	AT %MX1.3200.9 :	BOOL;(*Hoist emergency mode relay FB*)
	CRANE_CMS011	AT %MX1.3200.10 :	BOOL;(*Trolley emergency mode relay FB*)
	CRANE_CMS012	AT %MX1.3200.11 :	BOOL;(*Boom emergency mode relay FB*)
	CRANE_CMS013	AT %MX1.3200.12 :	BOOL;(*Auxiliary device fault*)
	CRANE_CMS014	AT %MX1.3200.13 :	BOOL;(*RHC run permit*)
	CRANE_CMS015	AT %MX1.3200.14 :	BOOL;(*SX bus ok*)
	CRANE_CMS016	AT %MX1.3200.15 :	BOOL;(*Control on permit*)
	CRANE_CMS017	AT %MX1.3201.0 :	BOOL;
	CRANE_CMS018	AT %MX1.3201.1 :	BOOL;
	CRANE_CMS019	AT %MX1.3201.2 :	BOOL;(*Control on*)
	CRANE_CMS020	AT %MX1.3201.3 :	BOOL;(*Control on at E_room*)
	CRANE_CMS021	AT %MX1.3201.4 :	BOOL;(*Control on at Cab*)
	CRANE_CMS022	AT %MX1.3201.5 :	BOOL;(*Control on at TLS station*)
	CRANE_CMS023	AT %MX1.3201.6 :	BOOL;(*Control on at BOS station*)
	CRANE_CMS024	AT %MX1.3201.7 :	BOOL;(*Control on at GOS station*)
	CRANE_CMS025	AT %MX1.3201.8 :	BOOL;
	CRANE_CMS026	AT %MX1.3201.9 :	BOOL;
	CRANE_CMS027	AT %MX1.3201.10 :	BOOL;
	CRANE_CMS028	AT %MX1.3201.11 :	BOOL;
	CRANE_CMS029	AT %MX1.3201.12 :	BOOL;
	CRANE_CMS030	AT %MX1.3201.13 :	BOOL;(*HOIST MOTOR HEATER CONTACTOR FB*)
	CRANE_CMS031	AT %MX1.3201.14 :	BOOL;(*TROLLEY MOTOR HEATER CONTACTOR FB*)
	CRANE_CMS032	AT %MX1.3201.15 :	BOOL;(*BOOM MOTOR HEATER CONTACTOR FB*)
	CRANE_CMS033	AT %MX1.3202.0 :	BOOL;(*TLS MOTOR HEATER CONTACTOR FB*)
	CRANE_CMS034	AT %MX1.3202.1 :	BOOL;(*GANTRY MOTOR HEATER CONTACTOR FB*)
	CRANE_CMS035	AT %MX1.3202.2 :	BOOL;(*BACKREACH HYDRAULIC STATION PUMP CONTACTOR FB*)
	CRANE_CMS036	AT %MX1.3202.3 :	BOOL;
	CRANE_CMS037	AT %MX1.3202.4 :	BOOL;
	CRANE_CMS038	AT %MX1.3202.5 :	BOOL;
	CRANE_CMS039	AT %MX1.3202.6 :	BOOL;
	CRANE_CMS040	AT %MX1.3202.7 :	BOOL;
	CRANE_CMS041	AT %MX1.3202.8 :	BOOL;
	CRANE_CMS042	AT %MX1.3202.9 :	BOOL;
	CRANE_CMS043	AT %MX1.3202.10 :	BOOL;
	CRANE_CMS044	AT %MX1.3202.11 :	BOOL;
	CRANE_CMS045	AT %MX1.3202.12 :	BOOL;
	CRANE_CMS046	AT %MX1.3202.13 :	BOOL;
	CRANE_CMS047	AT %MX1.3202.14 :	BOOL;
	CRANE_CMS048	AT %MX1.3202.15 :	BOOL;
	CRANE_CMS049	AT %MW1.3203 :	INT;(*Load weight (0.01T)*)
	CRANE_CMS050	AT %MW1.3204 :	INT;(*Loadcell#1 weight (0.01T)*)
	CRANE_CMS051	AT %MW1.3205 :	INT;(*Loadcell#2 weight (0.01T)*)
	CRANE_CMS052	AT %MW1.3206 :	INT;(*Loadcell#3 weight (0.01T)*)
	CRANE_CMS053	AT %MW1.3207 :	INT;(*Loadcell#4 weight (0.01T)*)
	CRANE_CMS054	AT %MW1.3208 :	INT;(*Wind speed (0.1m/min)*)
	CRANE_CMS055	AT %MW1.3209 :	WORD;(*Spreader size*)
	CRANE_CMS056	AT %MW1.3210 :	WORD;
	CRANE_CMS057	AT %MW1.3211 :	WORD;
	CRANE_CMS058	AT %MW1.3212 :	WORD;
	CRANE_CMS059	AT %MW1.3213 :	WORD;
	CRANE_CMS060	AT %MW1.3214 :	WORD;
	CRANE_CMS061	AT %MW1.3215 :	WORD;
	CRANE_CMS062	AT %MW1.3216 :	WORD;
	CRANE_CMS063	AT %MW1.3217 :	WORD;
	CRANE_CMS064	AT %MW1.3218 :	WORD;
	CRANE_CMS065	AT %MW1.3219 :	WORD;
	RHC_CMS001	AT %MX1.3510.0 :	BOOL;(*RHC#1 main power CB*)
	RHC_CMS002	AT %MX1.3510.1 :	BOOL;(*RHC#1 synchronization power CB*)
	RHC_CMS003	AT %MX1.3510.2 :	BOOL;(*RHC#1 control power R0 T0 CB*)
	RHC_CMS004	AT %MX1.3510.3 :	BOOL;(*RHC#1 synchronization contactor check fault*)
	RHC_CMS005	AT %MX1.3510.4 :	BOOL;(*RHC#1 ready contactor check fault*)
	RHC_CMS006	AT %MX1.3510.5 :	BOOL;(*RHC#1 charge failed*)
	RHC_CMS007	AT %MX1.3510.6 :	BOOL;(*RHC#1 main contactor check fault*)
	RHC_CMS008	AT %MX1.3510.7 :	BOOL;(*RHC#1 run check fault*)
	RHC_CMS009	AT %MX1.3510.8 :	BOOL;(*RHC#1 charge contactor run time check fault*)
	RHC_CMS010	AT %MX1.3510.9 :	BOOL;(*RHC#1 run permit*)
	RHC_CMS011	AT %MX1.3510.10 :	BOOL;(*RHC#2 main power CB*)
	RHC_CMS012	AT %MX1.3510.11 :	BOOL;(*RHC#2 synchronization power CB*)
	RHC_CMS013	AT %MX1.3510.12 :	BOOL;(*RHC#2 control power R0 T0 CB*)
	RHC_CMS014	AT %MX1.3510.13 :	BOOL;(*RHC#2 synchronization contactor check fault*)
	RHC_CMS015	AT %MX1.3510.14 :	BOOL;(*RHC#2 ready contactor check fault*)
	RHC_CMS016	AT %MX1.3510.15 :	BOOL;(*RHC#2 charge failed*)
	RHC_CMS017	AT %MX1.3511.0 :	BOOL;(*RHC#2 main contactor check fault*)
	RHC_CMS018	AT %MX1.3511.1 :	BOOL;(*RHC#2 run check fault*)
	RHC_CMS019	AT %MX1.3511.2 :	BOOL;(*RHC#2 charge contactor run time check fault*)
	RHC_CMS020	AT %MX1.3511.3 :	BOOL;(*RHC#2 run permit*)
	RHC_CMS021	AT %MX1.3511.4 :	BOOL;
	RHC_CMS022	AT %MX1.3511.5 :	BOOL;(*RHC#1 synchronization contactor FB*)
	RHC_CMS023	AT %MX1.3511.6 :	BOOL;(*RHC#1 ready contactor FB*)
	RHC_CMS024	AT %MX1.3511.7 :	BOOL;(*RHC#1 charge main contactor FB*)
	RHC_CMS025	AT %MX1.3511.8 :	BOOL;(*RHC#1 charge finish relay FB*)
	RHC_CMS026	AT %MX1.3511.9 :	BOOL;(*RHC#1 main contactor FB*)
	RHC_CMS027	AT %MX1.3511.10 :	BOOL;(*RHC#1 run relay FB*)
	RHC_CMS028	AT %MX1.3511.11 :	BOOL;(*RHC#1 in running state*)
	RHC_CMS029	AT %MX1.3511.12 :	BOOL;(*RHC#1 in alarm state*)
	RHC_CMS030	AT %MX1.3511.13 :	BOOL;(*RHC#1 DC link voltage estabilished*)
	RHC_CMS031	AT %MX1.3511.14 :	BOOL;(*RHC#2 synchronization contactor FB*)
	RHC_CMS032	AT %MX1.3511.15 :	BOOL;(*RHC#2 ready contactor FB*)
	RHC_CMS033	AT %MX1.3512.0 :	BOOL;(*RHC#2 charge main contactor FB*)
	RHC_CMS034	AT %MX1.3512.1 :	BOOL;(*RHC#2 charge finish relay FB*)
	RHC_CMS035	AT %MX1.3512.2 :	BOOL;(*RHC#2 main contactor FB*)
	RHC_CMS036	AT %MX1.3512.3 :	BOOL;(*RHC#2 run relay FB*)
	RHC_CMS037	AT %MX1.3512.4 :	BOOL;(*RHC#2 in running state*)
	RHC_CMS038	AT %MX1.3512.5 :	BOOL;(*RHC#2 in alarm state*)
	RHC_CMS039	AT %MX1.3512.6 :	BOOL;(*RHC#2 DC link voltage estabilished*)
	RHC_CMS040	AT %MX1.3512.7 :	BOOL;
	RHC_CMS041	AT %MX1.3512.8 :	BOOL;
	RHC_CMS042	AT %MX1.3512.9 :	BOOL;
	RHC_CMS043	AT %MX1.3512.10 :	BOOL;
	RHC_CMS044	AT %MX1.3512.11 :	BOOL;
	RHC_CMS045	AT %MX1.3512.12 :	BOOL;
	RHC_CMS046	AT %MX1.3512.13 :	BOOL;
	RHC_CMS047	AT %MX1.3512.14 :	BOOL;
	RHC_CMS048	AT %MX1.3512.15 :	BOOL;
	RHC_CMS049	AT %MX1.3513.0 :	BOOL;
	RHC_CMS050	AT %MX1.3513.1 :	BOOL;
	RHC_CMS051	AT %MX1.3513.2 :	BOOL;
	RHC_CMS052	AT %MX1.3513.3 :	BOOL;
	RHC_CMS053	AT %MX1.3513.4 :	BOOL;
	RHC_CMS054	AT %MX1.3513.5 :	BOOL;
	RHC_CMS055	AT %MX1.3513.6 :	BOOL;
	RHC_CMS056	AT %MX1.3513.7 :	BOOL;
	RHC_CMS057	AT %MX1.3513.8 :	BOOL;
	RHC_CMS058	AT %MX1.3513.9 :	BOOL;
	RHC_CMS059	AT %MX1.3513.10 :	BOOL;
	RHC_CMS060	AT %MX1.3513.11 :	BOOL;
	RHC_CMS061	AT %MX1.3513.12 :	BOOL;
	RHC_CMS062	AT %MX1.3513.13 :	BOOL;
	RHC_CMS063	AT %MX1.3513.14 :	BOOL;
	RHC_CMS064	AT %MX1.3513.15 :	BOOL;
	H_CMS001	AT %MX1.3240.0 :	BOOL;(*Hoist up permit*)
	H_CMS002	AT %MX1.3240.1 :	BOOL;(*Hoist down permit*)
	H_CMS003	AT %MX1.3240.2 :	BOOL;(*Hoist run permit*)
	H_CMS004	AT %MX1.3240.3 :	BOOL;(*Hoist masterswitch off*)
	H_CMS005	AT %MX1.3240.4 :	BOOL;(*Hoist up request*)
	H_CMS006	AT %MX1.3240.5 :	BOOL;(*Hoist down request*)
	H_CMS007	AT %MX1.3240.6 :	BOOL;(*Hoist up command*)
	H_CMS008	AT %MX1.3240.7 :	BOOL;(*Hoist down command*)
	H_CMS009	AT %MX1.3240.8 :	BOOL;(*Hoist up slowdown command*)
	H_CMS010	AT %MX1.3240.9 :	BOOL;(*Hoist down slowdown command*)
	H_CMS011	AT %MX1.3240.10 :	BOOL;(*Hoist slowdown command*)
	H_CMS012	AT %MX1.3240.11 :	BOOL;(*Hoist emergency stop relay*)
	H_CMS013	AT %MX1.3240.12 :	BOOL;(*Hoist emergency stop contactor*)
	H_CMS014	AT %MX1.3240.13 :	BOOL;(*Hoist up over travel LS*)
	H_CMS015	AT %MX1.3240.14 :	BOOL;(*Hoist up stop cam LS*)
	H_CMS016	AT %MX1.3240.15 :	BOOL;(*Hoist up stop by position*)
	H_CMS017	AT %MX1.3241.0 :	BOOL;(*Hoist up slowdown check cam LS*)
	H_CMS018	AT %MX1.3241.1 :	BOOL;(*Hoist synchronization cam LS*)
	H_CMS019	AT %MX1.3241.2 :	BOOL;(*Hoist down landside slowdown check cam LS*)
	H_CMS020	AT %MX1.3241.3 :	BOOL;(*Hoist down waterside slowdown cam LS*)
	H_CMS021	AT %MX1.3241.4 :	BOOL;(*Hoist down waterside stop cam LS*)
	H_CMS022	AT %MX1.3241.5 :	BOOL;(*Hoist down stop by position*)
	H_CMS023	AT %MX1.3241.6 :	BOOL;(*Hoist over sillbeam down stop by position*)
	H_CMS024	AT %MX1.3241.7 :	BOOL;(*Hoist over sillbeam down slowdown by position*)
	H_CMS025	AT %MX1.3241.8 :	BOOL;(*Hoist inv#1 DC voltage estabilished*)
	H_CMS026	AT %MX1.3241.9 :	BOOL;(*Hoist inv#2 DC voltage estabilished*)
	H_CMS027	AT %MX1.3241.10 :	BOOL;(*Hoist motor#1 fan contactor FB*)
	H_CMS028	AT %MX1.3241.11 :	BOOL;(*Hoist motor#2 fan contactor FB*)
	H_CMS029	AT %MX1.3241.12 :	BOOL;(*Hoist brake contactor FB*)
	H_CMS030	AT %MX1.3241.13 :	BOOL;(*Hoist brake#1 LS released*)
	H_CMS031	AT %MX1.3241.14 :	BOOL;(*Hoist brake#2 LS released*)
	H_CMS032	AT %MX1.3241.15 :	BOOL;(*Hoist brake#1 manual LS released*)
	H_CMS033	AT %MX1.3242.0 :	BOOL;(*Hoist brake#2 manual LS released*)
	H_CMS034	AT %MX1.3242.1 :	BOOL;(*Hoist brake#1 LS wear*)
	H_CMS035	AT %MX1.3242.2 :	BOOL;(*Hoist brake#1 LS wear*)
	H_CMS036	AT %MX1.3242.3 :	BOOL;(*Hoist drum rope overlap#1 LS*)
	H_CMS037	AT %MX1.3242.4 :	BOOL;(*Hoist drum rope overlap#2 LS*)
	H_CMS038	AT %MX1.3242.5 :	BOOL;(*Hoist hyd. brake#1 LS released*)
	H_CMS039	AT %MX1.3242.6 :	BOOL;(*Hoist hyd. brake#2 LS released*)
	H_CMS040	AT %MX1.3242.7 :	BOOL;(*Hoist hyd. brake#3 LS released*)
	H_CMS041	AT %MX1.3242.8 :	BOOL;(*Hoist hyd. brake#4 LS released*)
	H_CMS042	AT %MX1.3242.9 :	BOOL;(*Snag cylinder#1 pressure LS*)
	H_CMS043	AT %MX1.3242.10 :	BOOL;(*Snag cylinder#2 pressure LS*)
	H_CMS044	AT %MX1.3242.11 :	BOOL;(*Snag cylinder#3 pressure LS*)
	H_CMS045	AT %MX1.3242.12 :	BOOL;(*Snag cylinder#4 pressure LS*)
	H_CMS046	AT %MX1.3242.13 :	BOOL;
	H_CMS047	AT %MX1.3242.14 :	BOOL;(*Spreader mode*)
	H_CMS048	AT %MX1.3242.15 :	BOOL;(*Spreader unlocked*)
	H_CMS049	AT %MX1.3243.0 :	BOOL;(*Spreader locked*)
	H_CMS050	AT %MX1.3243.1 :	BOOL;(*Spreader lock overtime fault*)
	H_CMS051	AT %MX1.3243.2 :	BOOL;(*Spreader unlock overtime fault*)
	H_CMS052	AT %MX1.3243.3 :	BOOL;(*Spreader cable connected*)
	H_CMS053	AT %MX1.3243.4 :	BOOL;(*TTDS fault*)
	H_CMS054	AT %MX1.3243.5 :	BOOL;(*Spreader headblock connected*)
	H_CMS055	AT %MX1.3243.6 :	BOOL;(*Hook mode*)
	H_CMS056	AT %MX1.3243.7 :	BOOL;(*Eye plate mode*)
	H_CMS057	AT %MX1.3243.8 :	BOOL;(*Maintenance mode*)
	H_CMS058	AT %MX1.3243.9 :	BOOL;(*Snag fault*)
	H_CMS059	AT %MX1.3243.10 :	BOOL;(*Hoist overload fault*)
	H_CMS060	AT %MX1.3243.11 :	BOOL;(*Hoist eccentric load fault*)
	H_CMS061	AT %MX1.3243.12 :	BOOL;(*Snag trip by loadcell*)
	H_CMS062	AT %MX1.3243.13 :	BOOL;(*loadcell wire break fault*)
	H_CMS063	AT %MX1.3243.14 :	BOOL;(*Hoist up over travel fault*)
	H_CMS064	AT %MX1.3243.15 :	BOOL;(*Hoist up stop cam LS*)
	H_CMS065	AT %MX1.3244.0 :	BOOL;(*Hoist up stop by position*)
	H_CMS066	AT %MX1.3244.1 :	BOOL;(*Hoist up permit*)
	H_CMS067	AT %MX1.3244.2 :	BOOL;
	H_CMS068	AT %MX1.3244.3 :	BOOL;(*Spreader landed timer*)
	H_CMS069	AT %MX1.3244.4 :	BOOL;(*Spreader rope slack by loadcell*)
	H_CMS070	AT %MX1.3244.5 :	BOOL;(*Spreader rope slack*)
	H_CMS071	AT %MX1.3244.6 :	BOOL;(*Spreader mode*)
	H_CMS072	AT %MX1.3244.7 :	BOOL;(*Hook mode*)
	H_CMS073	AT %MX1.3244.8 :	BOOL;(*Eye plate mode*)
	H_CMS074	AT %MX1.3244.9 :	BOOL;(*Maintenance mode*)
	H_CMS075	AT %MX1.3244.10 :	BOOL;(*Hoist down stop by position*)
	H_CMS076	AT %MX1.3244.11 :	BOOL;(*Hoist over sillbeam down stop by position*)
	H_CMS077	AT %MX1.3244.12 :	BOOL;(*Hoist down waterside stop cam LS*)
	H_CMS078	AT %MX1.3244.13 :	BOOL;(*Hoist down permit*)
	H_CMS079	AT %MX1.3244.14 :	BOOL;
	H_CMS080	AT %MX1.3244.15 :	BOOL;(*Control on at Cab*)
	H_CMS081	AT %MX1.3245.0 :	BOOL;(*Gantry command*)
	H_CMS082	AT %MX1.3245.1 :	BOOL;(*Boom command*)
	H_CMS083	AT %MX1.3245.2 :	BOOL;(*Hoist inv#1 DC voltage estabilished*)
	H_CMS084	AT %MX1.3245.3 :	BOOL;(*Hoist inv#2 DC voltage estabilished*)
	H_CMS085	AT %MX1.3245.4 :	BOOL;(*Hoist motor#1 fan CB*)
	H_CMS086	AT %MX1.3245.5 :	BOOL;(*Hoist motor#2 fan CB*)
	H_CMS087	AT %MX1.3245.6 :	BOOL;(*Hoist motor#1 fan contactor FB*)
	H_CMS088	AT %MX1.3245.7 :	BOOL;(*Hoist motor#2 fan contactor FB*)
	H_CMS089	AT %MX1.3245.8 :	BOOL;(*Hoist motor#1 fan contactor check fault*)
	H_CMS090	AT %MX1.3245.9 :	BOOL;(*Hoist motor#2 fan contactor check fault*)
	H_CMS091	AT %MX1.3245.10 :	BOOL;(*Hoist brak#1 wear fault*)
	H_CMS092	AT %MX1.3245.11 :	BOOL;(*Hoist brak#2 wear fault*)
	H_CMS093	AT %MX1.3245.12 :	BOOL;(*RHC run permit*)
	H_CMS094	AT %MX1.3245.13 :	BOOL;(*Hoist inv#1 and inv#2 current not balance fault*)
	H_CMS095	AT %MX1.3245.14 :	BOOL;(*Hoist emergency on sensor FB*)
	H_CMS096	AT %MX1.3245.15 :	BOOL;(*Hoist emergency off sensor FB*)
	H_CMS097	AT %MX1.3246.0 :	BOOL;(*Hoist emergency mode relay FB*)
	H_CMS098	AT %MX1.3246.1 :	BOOL;(*Boom level*)
	H_CMS099	AT %MX1.3246.2 :	BOOL;(*Boom latch in*)
	H_CMS100	AT %MX1.3246.3 :	BOOL;(*Spreader unlocked*)
	H_CMS101	AT %MX1.3246.4 :	BOOL;(*High wind trip*)
	H_CMS102	AT %MX1.3246.5 :	BOOL;(*Hoist motor#1 high temp. trip*)
	H_CMS103	AT %MX1.3246.6 :	BOOL;(*Hoist motor#2 high temp. trip*)
	H_CMS104	AT %MX1.3246.7 :	BOOL;(*Hoist emergency stop contactor FB*)
	H_CMS105	AT %MX1.3246.8 :	BOOL;(*Hoist run permit*)
	H_CMS106	AT %MX1.3246.9 :	BOOL;
	H_CMS107	AT %MX1.3246.10 :	BOOL;(*Hoist brake power CB*)
	H_CMS108	AT %MX1.3246.11 :	BOOL;(*Hoist brake#1 power CB*)
	H_CMS109	AT %MX1.3246.12 :	BOOL;(*Hoist brake#2 power CB*)
	H_CMS110	AT %MX1.3246.13 :	BOOL;(*H/G safety relay power CB*)
	H_CMS111	AT %MX1.3246.14 :	BOOL;(*Hoist brake contactor check fault*)
	H_CMS112	AT %MX1.3246.15 :	BOOL;(*Hoist brake#1 LS release fault*)
	H_CMS113	AT %MX1.3247.0 :	BOOL;(*Hoist brake#2 LS release fault*)
	H_CMS114	AT %MX1.3247.1 :	BOOL;(*Hoist brake#1 manual release LS open fault*)
	H_CMS115	AT %MX1.3247.2 :	BOOL;(*Hoist brake#2 manual release LS open fault*)
	H_CMS116	AT %MX1.3247.3 :	BOOL;(*Hoist hyd. brake LS release fault*)
	H_CMS117	AT %MX1.3247.4 :	BOOL;(*Emergency brake Hyd. station run permit*)
	H_CMS118	AT %MX1.3247.5 :	BOOL;(*Hoist drum#1 rope overlap fault*)
	H_CMS119	AT %MX1.3247.6 :	BOOL;(*Hoist drum#2 rope overlap fault*)
	H_CMS120	AT %MX1.3247.7 :	BOOL;(*Hoist inv#1 fan power R1 T1 CB*)
	H_CMS121	AT %MX1.3247.8 :	BOOL;(*Hoist inv#2 fan power R1 T1 CB*)
	H_CMS122	AT %MX1.3247.9 :	BOOL;(*Hoist run time check fault*)
	H_CMS123	AT %MX1.3247.10 :	BOOL;(*Hoist command lost while brake open fault*)
	H_CMS124	AT %MX1.3247.11 :	BOOL;(*Hoist emergency stop relay check fault*)
	H_CMS125	AT %MX1.3247.12 :	BOOL;(*Hoist emergency stop contactor check fault*)
	H_CMS126	AT %MX1.3247.13 :	BOOL;(*Hoist up slowdown LS position check fault*)
	H_CMS127	AT %MX1.3247.14 :	BOOL;(*Hoist up slowdown LS speed check fault*)
	H_CMS128	AT %MX1.3247.15 :	BOOL;(*Hoist down slowdown LS position check fault(Landside)*)
	H_CMS129	AT %MX1.3248.0 :	BOOL;(*Hoist down slowdown LS speed check fault(Landside)*)
	H_CMS130	AT %MX1.3248.1 :	BOOL;(*Hes*)
	H_CMS131	AT %MX1.3248.2 :	BOOL;
	H_CMS132	AT %MX1.3248.3 :	BOOL;(*No e_stop*)
	H_CMS133	AT %MX1.3248.4 :	BOOL;(*Control on*)
	H_CMS134	AT %MX1.3248.5 :	BOOL;(*Sx bus ok*)
	H_CMS135	AT %MX1.3248.6 :	BOOL;(*RHC run permit*)
	H_CMS136	AT %MX1.3248.7 :	BOOL;(*RHC#1 fault*)
	H_CMS137	AT %MX1.3248.8 :	BOOL;(*RHC#2 fault*)
	H_CMS138	AT %MX1.3248.9 :	BOOL;(*Hoist#1 inv fault*)
	H_CMS139	AT %MX1.3248.10 :	BOOL;(*Hoist#2 inv fault*)
	H_CMS140	AT %MX1.3248.11 :	BOOL;(*Hoist inv#1 and inv#2 speed not balance fault*)
	H_CMS141	AT %MX1.3248.12 :	BOOL;(*Hoist overspeed fault*)
	H_CMS142	AT %MX1.3248.13 :	BOOL;(*Hoist speed setting and feedback disagree fault*)
	H_CMS143	AT %MX1.3248.14 :	BOOL;(*Hoist inv#1 sx communication error*)
	H_CMS144	AT %MX1.3248.15 :	BOOL;(*Hoist inv#2 sx communication error*)
	H_CMS145	AT %MX1.3249.0 :	BOOL;(*Snag fault*)
	H_CMS146	AT %MX1.3249.1 :	BOOL;(*Hoist up over travel fault*)
	H_CMS147	AT %MX1.3249.2 :	BOOL;(*Hes*)
	H_CMS148	AT %MX1.3249.3 :	BOOL;(*Hesx*)
	H_CMS149	AT %MX1.3249.4 :	BOOL;
	H_CMS150	AT %MX1.3249.5 :	BOOL;
	H_CMS151	AT %MX1.3249.6 :	BOOL;
	H_CMS152	AT %MX1.3249.7 :	BOOL;
	H_CMS153	AT %MX1.3249.8 :	BOOL;
	H_CMS154	AT %MX1.3249.9 :	BOOL;
	H_CMS155	AT %MX1.3249.10 :	BOOL;
	H_CMS156	AT %MX1.3249.11 :	BOOL;
	H_CMS157	AT %MX1.3249.12 :	BOOL;
	H_CMS158	AT %MX1.3249.13 :	BOOL;
	H_CMS159	AT %MX1.3249.14 :	BOOL;
	H_CMS160	AT %MX1.3249.15 :	BOOL;
	H_CMS161	AT %MW1.3250 :	INT;(*Hoist masterswitch reference in*)
	H_CMS162	AT %MW1.3251 :	INT;(*Hoist motor#1 voltage (Rated:470V)*)
	H_CMS163	AT %MW1.3252 :	INT;(*Hoist motor#1 current (Rated:1095A)*)
	H_CMS164	AT %MW1.3253 :	INT;(*Hoist motor#1 speed feedback (Rated:1796rpm, 130m/min)*)
	H_CMS165	AT %MW1.3254 :	INT;(*Hoist motor#2 voltage (Rated:470V)*)
	H_CMS166	AT %MW1.3255 :	INT;(*Hoist motor#2 current (Rated:1095A)*)
	H_CMS167	AT %MW1.3256 :	INT;(*Hoist motor#2 speed feedback (Rated:1796rpm, 130m/min)*)
	H_CMS168	AT %MW1.3257 :	INT;
	H_CMS169	AT %MW1.3258 :	INT;(*Hoist motor#1 temperature*)
	H_CMS170	AT %MW1.3259 :	INT;(*Hoist motor#2 temperature*)
	H_CMS171	AT %MD1.3260 :	DINT;(*Hoist position (mm)*)
	H_CMS173	AT %MW1.3262 :	WORD;
	H_CMS174	AT %MW1.3263 :	WORD;
	H_CMS175	AT %MW1.3264 :	WORD;
	H_CMS176	AT %MW1.3265 :	WORD;
	H_CMS177	AT %MW1.3266 :	WORD;
	H_CMS178	AT %MW1.3267 :	WORD;
	H_CMS179	AT %MW1.3268 :	WORD;
	H_CMS180	AT %MW1.3269 :	WORD;
	H_CMS181	AT %MW1.3270 :	WORD;
	H_CMS182	AT %MW1.3271 :	WORD;
	H_CMS183	AT %MW1.3272 :	WORD;
	H_CMS184	AT %MW1.3273 :	WORD;
	H_CMS185	AT %MW1.3274 :	WORD;
	H_CMS186	AT %MW1.3275 :	WORD;
	H_CMS187	AT %MW1.3276 :	WORD;
	H_CMS188	AT %MW1.3277 :	WORD;
	H_CMS189	AT %MW1.3278 :	WORD;
	H_CMS190	AT %MW1.3279 :	WORD;
	T_CMS001	AT %MX1.3280.0 :	BOOL;(*Trolley forward permit*)
	T_CMS002	AT %MX1.3280.1 :	BOOL;(*Trolley reverse permit*)
	T_CMS003	AT %MX1.3280.2 :	BOOL;(*Trolley run permit*)
	T_CMS004	AT %MX1.3280.3 :	BOOL;(*Trolley masterswitch off*)
	T_CMS005	AT %MX1.3280.4 :	BOOL;(*Trolley forward request*)
	T_CMS006	AT %MX1.3280.5 :	BOOL;(*Trolley reverse request*)
	T_CMS007	AT %MX1.3280.6 :	BOOL;(*Trolley forward command*)
	T_CMS008	AT %MX1.3280.7 :	BOOL;(*Trolley reverse command*)
	T_CMS009	AT %MX1.3280.8 :	BOOL;(*Trolley forward slowdown command*)
	T_CMS010	AT %MX1.3280.9 :	BOOL;(*Trolley reverse slowdown command*)
	T_CMS011	AT %MX1.3280.10 :	BOOL;(*Trolley slowdown command*)
	T_CMS012	AT %MX1.3280.11 :	BOOL;(*Trolley emergency stop relay*)
	T_CMS013	AT %MX1.3280.12 :	BOOL;(*Trolley emergency stop contactor*)
	T_CMS014	AT %MX1.3280.13 :	BOOL;(*Trolley forward over travel LS*)
	T_CMS015	AT %MX1.3280.14 :	BOOL;(*Trolley reverse over travel LS*)
	T_CMS016	AT %MX1.3280.15 :	BOOL;(*Trolley forward stop LS*)
	T_CMS017	AT %MX1.3281.0 :	BOOL;(*Trolley reverse stop LS*)
	T_CMS018	AT %MX1.3281.1 :	BOOL;(*Trolley forward stop by position*)
	T_CMS019	AT %MX1.3281.2 :	BOOL;(*Trolley reverse stop by position*)
	T_CMS020	AT %MX1.3281.3 :	BOOL;(*Trolley forward stop by position when boom up*)
	T_CMS021	AT %MX1.3281.4 :	BOOL;(*Trolley forward slowdown check LS*)
	T_CMS022	AT %MX1.3281.5 :	BOOL;(*Trolley reverse slowdown check LS*)
	T_CMS023	AT %MX1.3281.6 :	BOOL;(*Trolley forward slowdown by position*)
	T_CMS024	AT %MX1.3281.7 :	BOOL;(*Trolley forward slowdown by position when boom up*)
	T_CMS025	AT %MX1.3281.8 :	BOOL;(*Trolley reverse slowdown by position*)
	T_CMS026	AT %MX1.3281.9 :	BOOL;(*Trolley park position LS*)
	T_CMS027	AT %MX1.3281.10 :	BOOL;(*Trolley/boom inv DC voltage estabilished*)
	T_CMS028	AT %MX1.3281.11 :	BOOL;(*Trolley motor Fan contactor FB*)
	T_CMS029	AT %MX1.3281.12 :	BOOL;(*Trolley brake contactor FB*)
	T_CMS030	AT %MX1.3281.13 :	BOOL;(*Trolley brake#1 LS released*)
	T_CMS031	AT %MX1.3281.14 :	BOOL;(*Trolley brake#2 LS released*)
	T_CMS032	AT %MX1.3281.15 :	BOOL;(*Trolley brake#3 LS released*)
	T_CMS033	AT %MX1.3282.0 :	BOOL;(*Trolley brake#4 LS released*)
	T_CMS034	AT %MX1.3282.1 :	BOOL;(*Trolley brake#1 manual LS released*)
	T_CMS035	AT %MX1.3282.2 :	BOOL;(*Trolley brake#2 manual LS released*)
	T_CMS036	AT %MX1.3282.3 :	BOOL;(*Trolley brake#3 manual LS released*)
	T_CMS037	AT %MX1.3282.4 :	BOOL;(*Trolley brake#4 manual LS released*)
	T_CMS038	AT %MX1.3282.5 :	BOOL;(*Trolley brake#1 LS wear*)
	T_CMS039	AT %MX1.3282.6 :	BOOL;(*Trolley brake#2 LS wear*)
	T_CMS040	AT %MX1.3282.7 :	BOOL;(*Trolley brake#3 LS wear*)
	T_CMS041	AT %MX1.3282.8 :	BOOL;(*Trolley brake#4 LS wear*)
	T_CMS042	AT %MX1.3282.9 :	BOOL;(*Trolley left anchor release LS*)
	T_CMS043	AT %MX1.3282.10 :	BOOL;(*Trolley right anchor release LS*)
	T_CMS044	AT %MX1.3282.11 :	BOOL;(*Trolley door gate LS#1*)
	T_CMS045	AT %MX1.3282.12 :	BOOL;(*Trolley door gate LS#2*)
	T_CMS046	AT %MX1.3282.13 :	BOOL;(*Trolley washing platform gate LS*)
	T_CMS047	AT %MX1.3282.14 :	BOOL;
	T_CMS048	AT %MX1.3282.15 :	BOOL;(*Trolley forward stop LS*)
	T_CMS049	AT %MX1.3283.0 :	BOOL;(*Trolley forward stop by position*)
	T_CMS050	AT %MX1.3283.1 :	BOOL;(*Trolley forward stop by position when boom up*)
	T_CMS051	AT %MX1.3283.2 :	BOOL;(*Trolley forward over travel fault*)
	T_CMS052	AT %MX1.3283.3 :	BOOL;(*Trolley forward permit*)
	T_CMS053	AT %MX1.3283.4 :	BOOL;
	T_CMS054	AT %MX1.3283.5 :	BOOL;(*Trolley reverse stop LS*)
	T_CMS055	AT %MX1.3283.6 :	BOOL;(*Trolley reverse stop by position*)
	T_CMS056	AT %MX1.3283.7 :	BOOL;(*Trolley reverse over travel fault*)
	T_CMS057	AT %MX1.3283.8 :	BOOL;(*Trolley reverse permit*)
	T_CMS058	AT %MX1.3283.9 :	BOOL;
	T_CMS059	AT %MX1.3283.10 :	BOOL;(*Control on at Cab*)
	T_CMS060	AT %MX1.3283.11 :	BOOL;(*First come first serve boom*)
	T_CMS061	AT %MX1.3283.12 :	BOOL;(*Trolley/boom inv DC voltage estabilished*)
	T_CMS062	AT %MX1.3283.13 :	BOOL;(*Trolley motor main contactor FB*)
	T_CMS063	AT %MX1.3283.14 :	BOOL;(*Boom motor main contactor FB*)
	T_CMS064	AT %MX1.3283.15 :	BOOL;(*Trolley motor encoder selected*)
	T_CMS065	AT %MX1.3284.0 :	BOOL;(*Boom motor encoder selected*)
	T_CMS066	AT %MX1.3284.1 :	BOOL;(*Trolley/boom inv in boom data mode*)
	T_CMS067	AT %MX1.3284.2 :	BOOL;(*Rope tensioner permit*)
	T_CMS068	AT %MX1.3284.3 :	BOOL;(*Boom command*)
	T_CMS069	AT %MX1.3284.4 :	BOOL;(*Trolley motor fan main CB*)
	T_CMS070	AT %MX1.3284.5 :	BOOL;(*Trolley motor#1 fan CB*)
	T_CMS071	AT %MX1.3284.6 :	BOOL;(*Trolley motor#2 fan CB*)
	T_CMS072	AT %MX1.3284.7 :	BOOL;(*Trolley motor#3 fan CB*)
	T_CMS073	AT %MX1.3284.8 :	BOOL;(*Trolley motor#4 fan CB*)
	T_CMS074	AT %MX1.3284.9 :	BOOL;(*Trolley motor fan contactor FB*)
	T_CMS075	AT %MX1.3284.10 :	BOOL;(*Trolley motor fan contactor check fault*)
	T_CMS076	AT %MX1.3284.11 :	BOOL;(*Trolley brake#1 wear fault*)
	T_CMS077	AT %MX1.3284.12 :	BOOL;(*Trolley brake#2 wear fault*)
	T_CMS078	AT %MX1.3284.13 :	BOOL;(*Trolley brake#3 wear fault*)
	T_CMS079	AT %MX1.3284.14 :	BOOL;(*Trolley brake#4 wear fault*)
	T_CMS080	AT %MX1.3284.15 :	BOOL;(*Trolley emergency on sensor#1 FB*)
	T_CMS081	AT %MX1.3285.0 :	BOOL;(*Trolley emergency off sensor#1 FB*)
	T_CMS082	AT %MX1.3285.1 :	BOOL;(*Trolley emergency on sensor#2 FB*)
	T_CMS083	AT %MX1.3285.2 :	BOOL;(*Trolley emergency off sensor#2 FB*)
	T_CMS084	AT %MX1.3285.3 :	BOOL;(*Trolley emergency mode relay FB*)
	T_CMS085	AT %MX1.3285.4 :	BOOL;(*Trolley motor#1 high temp. trip*)
	T_CMS086	AT %MX1.3285.5 :	BOOL;(*Trolley motor#2 high temp. trip*)
	T_CMS087	AT %MX1.3285.6 :	BOOL;(*Trolley motor#3 high temp. trip*)
	T_CMS088	AT %MX1.3285.7 :	BOOL;(*Trolley motor#4 high temp. trip*)
	T_CMS089	AT %MX1.3285.8 :	BOOL;(*Snag fault*)
	T_CMS090	AT %MX1.3285.9 :	BOOL;(*Snag trip by loadcell*)
	T_CMS091	AT %MX1.3285.10 :	BOOL;(*loadcell wire break fault*)
	T_CMS092	AT %MX1.3285.11 :	BOOL;(*Hoist overload fault*)
	T_CMS093	AT %MX1.3285.12 :	BOOL;(*Hoist eccentric load fault*)
	T_CMS094	AT %MX1.3285.13 :	BOOL;(*Spreader unlocked*)
	T_CMS095	AT %MX1.3285.14 :	BOOL;(*Spreader locked*)
	T_CMS096	AT %MX1.3285.15 :	BOOL;(*Spreader landed timer*)
	T_CMS097	AT %MX1.3286.0 :	BOOL;(*Spreader rope slack by loadcell*)
	T_CMS098	AT %MX1.3286.1 :	BOOL;(*Spreader rope slack*)
	T_CMS099	AT %MX1.3286.2 :	BOOL;(*Spreader mode*)
	T_CMS100	AT %MX1.3286.3 :	BOOL;(*Boom level*)
	T_CMS101	AT %MX1.3286.4 :	BOOL;(*Boom latch in*)
	T_CMS102	AT %MX1.3286.5 :	BOOL;(*Spreader unlocked*)
	T_CMS103	AT %MX1.3286.6 :	BOOL;(*Trolley left anchor not released*)
	T_CMS104	AT %MX1.3286.7 :	BOOL;(*Trolley right anchor not released*)
	T_CMS105	AT %MX1.3286.8 :	BOOL;(*Trolley door gate LS#1 open fault*)
	T_CMS106	AT %MX1.3286.9 :	BOOL;(*Trolley door gate LS#2 open fault*)
	T_CMS107	AT %MX1.3286.10 :	BOOL;(*Trolley washing platform gate LS open fault*)
	T_CMS108	AT %MX1.3286.11 :	BOOL;(*High wind trip*)
	T_CMS109	AT %MX1.3286.12 :	BOOL;(*RHC run permit*)
	T_CMS110	AT %MX1.3286.13 :	BOOL;(*Trolley emergency stop contactor FB*)
	T_CMS111	AT %MX1.3286.14 :	BOOL;(*Trolley run permit*)
	T_CMS112	AT %MX1.3286.15 :	BOOL;
	T_CMS113	AT %MX1.3287.0 :	BOOL;(*Trolley motor#1 power CB*)
	T_CMS114	AT %MX1.3287.1 :	BOOL;(*Trolley motor#2 power CB*)
	T_CMS115	AT %MX1.3287.2 :	BOOL;(*Trolley motor#3 power CB*)
	T_CMS116	AT %MX1.3287.3 :	BOOL;(*Trolley motor#4 power CB*)
	T_CMS117	AT %MX1.3287.4 :	BOOL;(*Trolley brake power CB*)
	T_CMS118	AT %MX1.3287.5 :	BOOL;(*Trolley brake#1 power CB*)
	T_CMS119	AT %MX1.3287.6 :	BOOL;(*Trolley brake#2 power CB*)
	T_CMS120	AT %MX1.3287.7 :	BOOL;(*Trolley brake#3 power CB*)
	T_CMS121	AT %MX1.3287.8 :	BOOL;(*Trolley brake#4 power CB*)
	T_CMS122	AT %MX1.3287.9 :	BOOL;(*T/B safety relay power CB*)
	T_CMS123	AT %MX1.3287.10 :	BOOL;(*T/B motor contactor changeover power CB*)
	T_CMS124	AT %MX1.3287.11 :	BOOL;(*Trolley brake#1 LS release fault*)
	T_CMS125	AT %MX1.3287.12 :	BOOL;(*Trolley brake#2 LS release fault*)
	T_CMS126	AT %MX1.3287.13 :	BOOL;(*Trolley brake#3 LS release fault*)
	T_CMS127	AT %MX1.3287.14 :	BOOL;(*Trolley brake#4 LS release fault*)
	T_CMS128	AT %MX1.3287.15 :	BOOL;(*Trolley brake contactor check fault*)
	T_CMS129	AT %MX1.3288.0 :	BOOL;(*Trolley brake#1 manual release LS open fault*)
	T_CMS130	AT %MX1.3288.1 :	BOOL;(*Trolley brake#2 manual release LS open fault*)
	T_CMS131	AT %MX1.3288.2 :	BOOL;(*Trolley brake#3 manual release LS open fault*)
	T_CMS132	AT %MX1.3288.3 :	BOOL;(*Trolley brake#4 manual release LS open fault*)
	T_CMS133	AT %MX1.3288.4 :	BOOL;(*Trolley/boom inv fan power R1 T1 CB*)
	T_CMS134	AT %MX1.3288.5 :	BOOL;(*Trolley run time check fault*)
	T_CMS135	AT %MX1.3288.6 :	BOOL;(*Trolley command lost while brake open fault*)
	T_CMS136	AT %MX1.3288.7 :	BOOL;(*Trolley changeover motor main contactor check fault*)
	T_CMS137	AT %MX1.3288.8 :	BOOL;(*Trolley changeover encoder relay check fault*)
	T_CMS138	AT %MX1.3288.9 :	BOOL;(*Trolley emergency stop relay check fault*)
	T_CMS139	AT %MX1.3288.10 :	BOOL;(*Trolley emergency stop contactor check fault*)
	T_CMS140	AT %MX1.3288.11 :	BOOL;(*Trolley forward slowdown LS position check fault*)
	T_CMS141	AT %MX1.3288.12 :	BOOL;(*Trolley reverse slowdown LS position check fault*)
	T_CMS142	AT %MX1.3288.13 :	BOOL;(*Trolley forward slowdown LS speed check fault*)
	T_CMS143	AT %MX1.3288.14 :	BOOL;(*Trolley reverse slowdown LS speed check fault*)
	T_CMS144	AT %MX1.3288.15 :	BOOL;(*Tes*)
	T_CMS145	AT %MX1.3289.0 :	BOOL;
	T_CMS146	AT %MX1.3289.1 :	BOOL;(*No e_stop*)
	T_CMS147	AT %MX1.3289.2 :	BOOL;(*Control on*)
	T_CMS148	AT %MX1.3289.3 :	BOOL;(*Sx bus ok*)
	T_CMS149	AT %MX1.3289.4 :	BOOL;(*RHC run permit*)
	T_CMS150	AT %MX1.3289.5 :	BOOL;(*RHC#1 fault*)
	T_CMS151	AT %MX1.3289.6 :	BOOL;(*RHC#2 fault*)
	T_CMS152	AT %MX1.3289.7 :	BOOL;(*Trolley/boom inv fault*)
	T_CMS153	AT %MX1.3289.8 :	BOOL;(*Trolley/boom inv sx communication error*)
	T_CMS154	AT %MX1.3289.9 :	BOOL;(*Tes*)
	T_CMS155	AT %MX1.3289.10 :	BOOL;(*Snag fault*)
	T_CMS156	AT %MX1.3289.11 :	BOOL;(*Trolley forward over travel fault*)
	T_CMS157	AT %MX1.3289.12 :	BOOL;(*Trolley reverse over travel fault*)
	T_CMS158	AT %MX1.3289.13 :	BOOL;(*Tesx*)
	T_CMS159	AT %MX1.3289.14 :	BOOL;
	T_CMS160	AT %MX1.3289.15 :	BOOL;
	T_CMS161	AT %MW1.3290 :	INT;(*Trolley masterswitch reference in*)
	T_CMS162	AT %MW1.3291 :	INT;(*Trolley motor voltage (Rated:450V)*)
	T_CMS163	AT %MW1.3292 :	INT;(*Trolley motor current (Rated:215A)*)
	T_CMS164	AT %MW1.3293 :	INT;(*Trolley motor speed feedback (Rated:1750rpm, 220m/min)*)
	T_CMS165	AT %MW1.3294 :	INT;
	T_CMS166	AT %MW1.3295 :	INT;(*Trolley motor#1 temperature*)
	T_CMS167	AT %MW1.3296 :	INT;(*Trolley motor#2 temperature*)
	T_CMS168	AT %MW1.3297 :	INT;(*Trolley motor#3 temperature*)
	T_CMS169	AT %MW1.3298 :	INT;(*Trolley motor#4 temperature*)
	T_CMS170	AT %MW1.3299 :	WORD;
	T_CMS171	AT %MD1.3300 :	DINT;(*Trolley position (mm)*)
	T_CMS173	AT %MW1.3302 :	WORD;
	T_CMS174	AT %MW1.3303 :	WORD;
	T_CMS175	AT %MW1.3304 :	WORD;
	T_CMS176	AT %MW1.3305 :	WORD;
	T_CMS177	AT %MW1.3306 :	WORD;
	T_CMS178	AT %MW1.3307 :	WORD;
	T_CMS179	AT %MW1.3308 :	WORD;
	T_CMS180	AT %MW1.3309 :	WORD;
	T_CMS181	AT %MW1.3310 :	WORD;
	T_CMS182	AT %MW1.3311 :	WORD;
	T_CMS183	AT %MW1.3312 :	WORD;
	T_CMS184	AT %MW1.3313 :	WORD;
	T_CMS185	AT %MW1.3314 :	WORD;
	T_CMS186	AT %MW1.3315 :	WORD;
	T_CMS187	AT %MW1.3316 :	WORD;
	T_CMS188	AT %MW1.3317 :	WORD;
	T_CMS189	AT %MW1.3318 :	WORD;
	T_CMS190	AT %MW1.3319 :	WORD;
	B_CMS001	AT %MX1.3320.0 :	BOOL;(*Boom up permit*)
	B_CMS002	AT %MX1.3320.1 :	BOOL;(*Boom down permit*)
	B_CMS003	AT %MX1.3320.2 :	BOOL;(*Boom run permit*)
	B_CMS004	AT %MX1.3320.3 :	BOOL;(*Boom up request*)
	B_CMS005	AT %MX1.3320.4 :	BOOL;(*Boom down request*)
	B_CMS006	AT %MX1.3320.5 :	BOOL;(*Boom up command*)
	B_CMS007	AT %MX1.3320.6 :	BOOL;(*Boom down command*)
	B_CMS008	AT %MX1.3320.7 :	BOOL;(*Boom up slowdown command*)
	B_CMS009	AT %MX1.3320.8 :	BOOL;(*Boom down slowdown command*)
	B_CMS010	AT %MX1.3320.9 :	BOOL;(*Boom slowdown command*)
	B_CMS011	AT %MX1.3320.10 :	BOOL;(*Boom emergency stop relay*)
	B_CMS012	AT %MX1.3320.11 :	BOOL;(*Boom emergency stop contactor*)
	B_CMS013	AT %MX1.3320.12 :	BOOL;(*Boom up over travel LS*)
	B_CMS014	AT %MX1.3320.13 :	BOOL;(*Boom up stop LS*)
	B_CMS015	AT %MX1.3320.14 :	BOOL;(*Boom up stop by position*)
	B_CMS016	AT %MX1.3320.15 :	BOOL;(*Boom up slowdown cam LS*)
	B_CMS017	AT %MX1.3321.0 :	BOOL;(*Boom up slowdown by position*)
	B_CMS018	AT %MX1.3321.1 :	BOOL;(*Boom down slowdown cam LS*)
	B_CMS019	AT %MX1.3321.2 :	BOOL;(*Boom down slowdown by position*)
	B_CMS020	AT %MX1.3321.3 :	BOOL;(*Boom down stop LS*)
	B_CMS021	AT %MX1.3321.4 :	BOOL;(*Boom level*)
	B_CMS022	AT %MX1.3321.5 :	BOOL;(*Boom motor fan contactor FB*)
	B_CMS023	AT %MX1.3321.6 :	BOOL;(*Boom brake contactor FB*)
	B_CMS024	AT %MX1.3321.7 :	BOOL;(*Boom brake LS released*)
	B_CMS025	AT %MX1.3321.8 :	BOOL;(*Boom brake manual LS released*)
	B_CMS026	AT %MX1.3321.9 :	BOOL;(*Boom brake LS wear*)
	B_CMS027	AT %MX1.3321.10 :	BOOL;(*Boom drum rope overlap#1 LS*)
	B_CMS028	AT %MX1.3321.11 :	BOOL;(*Boom drum rope overlap#2 LS*)
	B_CMS029	AT %MX1.3321.12 :	BOOL;(*Boom left wire rope slack LS*)
	B_CMS030	AT %MX1.3321.13 :	BOOL;(*Boom right wire rope slack LS*)
	B_CMS031	AT %MX1.3321.14 :	BOOL;(*Boom hyd. brake#1 LS released*)
	B_CMS032	AT %MX1.3321.15 :	BOOL;(*Boom hyd. brake#2 LS released*)
	B_CMS033	AT %MX1.3322.0 :	BOOL;(*Boom hyd. brake#3 LS released*)
	B_CMS034	AT %MX1.3322.1 :	BOOL;(*Boom hyd. brake#4 LS released*)
	B_CMS035	AT %MX1.3322.2 :	BOOL;(*Boom latch#1 contactor FB*)
	B_CMS036	AT %MX1.3322.3 :	BOOL;(*Boom latch#2 contactor FB*)
	B_CMS037	AT %MX1.3322.4 :	BOOL;(*Boom latch#1 up LS*)
	B_CMS038	AT %MX1.3322.5 :	BOOL;(*Boom latch#2 up LS*)
	B_CMS039	AT %MX1.3322.6 :	BOOL;(*Boom anti-collision left LS*)
	B_CMS040	AT %MX1.3322.7 :	BOOL;(*Boom anti-collision right LS*)
	B_CMS041	AT %MX1.3322.8 :	BOOL;(*Boom access left gate LS*)
	B_CMS042	AT %MX1.3322.9 :	BOOL;(*Boom access right gate LS*)
	B_CMS043	AT %MX1.3322.10 :	BOOL;
	B_CMS044	AT %MX1.3322.11 :	BOOL;(*Boom up stop LS*)
	B_CMS045	AT %MX1.3322.12 :	BOOL;(*Boom up stop by position*)
	B_CMS046	AT %MX1.3322.13 :	BOOL;(*Boom up over travel fault*)
	B_CMS047	AT %MX1.3322.14 :	BOOL;(*Catenary rope tensioner high pressure fault*)
	B_CMS048	AT %MX1.3322.15 :	BOOL;(*Boom up permit*)
	B_CMS049	AT %MX1.3323.0 :	BOOL;
	B_CMS050	AT %MX1.3323.1 :	BOOL;(*Boom level*)
	B_CMS051	AT %MX1.3323.2 :	BOOL;(*Boom drum rope overlap#1 fault*)
	B_CMS052	AT %MX1.3323.3 :	BOOL;(*Boom drum rope overlap#2 fault*)
	B_CMS053	AT %MX1.3323.4 :	BOOL;(*Boom down permit*)
	B_CMS054	AT %MX1.3323.5 :	BOOL;
	B_CMS055	AT %MX1.3323.6 :	BOOL;(*Control on at Cab*)
	B_CMS056	AT %MX1.3323.7 :	BOOL;(*Control on at BOS station*)
	B_CMS057	AT %MX1.3323.8 :	BOOL;(*First come first serve boom*)
	B_CMS058	AT %MX1.3323.9 :	BOOL;(*Trolley/boom inv DC voltage estabilished*)
	B_CMS059	AT %MX1.3323.10 :	BOOL;(*Boom motor main contactor FB*)
	B_CMS060	AT %MX1.3323.11 :	BOOL;(*Trolley motor main contactor FB*)
	B_CMS061	AT %MX1.3323.12 :	BOOL;(*Boom motor encoder selected*)
	B_CMS062	AT %MX1.3323.13 :	BOOL;(*Trolley motor encoder selected*)
	B_CMS063	AT %MX1.3323.14 :	BOOL;(*Trolley/boom inv in boom data mode*)
	B_CMS064	AT %MX1.3323.15 :	BOOL;(*Rope tensioner permit*)
	B_CMS065	AT %MX1.3324.0 :	BOOL;(*Hoist command*)
	B_CMS066	AT %MX1.3324.1 :	BOOL;(*Trolley command*)
	B_CMS067	AT %MX1.3324.2 :	BOOL;(*Gantry command*)
	B_CMS068	AT %MX1.3324.3 :	BOOL;(*Boom motor fan CB*)
	B_CMS069	AT %MX1.3324.4 :	BOOL;(*Boom motor fan contactor FB*)
	B_CMS070	AT %MX1.3324.5 :	BOOL;(*Boom motor fan contactor check fault*)
	B_CMS071	AT %MX1.3324.6 :	BOOL;(*Boom brake wear fault*)
	B_CMS072	AT %MX1.3324.7 :	BOOL;(*Boom emergency on sensor FB*)
	B_CMS073	AT %MX1.3324.8 :	BOOL;(*Boom emergency off sensor FB*)
	B_CMS074	AT %MX1.3324.9 :	BOOL;(*Boom emergency mode relay FB*)
	B_CMS075	AT %MX1.3324.10 :	BOOL;(*Boom motor high temp. trip*)
	B_CMS076	AT %MX1.3324.11 :	BOOL;(*Spreader locked*)
	B_CMS077	AT %MX1.3324.12 :	BOOL;(*Boom left access gate LS open fault*)
	B_CMS078	AT %MX1.3324.13 :	BOOL;(*Boom right access gate LS open fault*)
	B_CMS079	AT %MX1.3324.14 :	BOOL;(*High wind trip*)
	B_CMS080	AT %MX1.3324.15 :	BOOL;(*RHC run permit*)
	B_CMS081	AT %MX1.3325.0 :	BOOL;(*Boom emergency stop contactor FB*)
	B_CMS082	AT %MX1.3325.1 :	BOOL;(*Trolley parked*)
	B_CMS083	AT %MX1.3325.2 :	BOOL;(*Boom run permit*)
	B_CMS084	AT %MX1.3325.3 :	BOOL;
	B_CMS085	AT %MX1.3325.4 :	BOOL;(*Boom brake power CB*)
	B_CMS086	AT %MX1.3325.5 :	BOOL;(*T/B safety relay power CB*)
	B_CMS087	AT %MX1.3325.6 :	BOOL;(*T/B motor contactor changeover power CB*)
	B_CMS088	AT %MX1.3325.7 :	BOOL;(*Boom brake contactor check fault*)
	B_CMS089	AT %MX1.3325.8 :	BOOL;(*Boom brake LS release fault*)
	B_CMS090	AT %MX1.3325.9 :	BOOL;(*Boom brake manual release LS open fault*)
	B_CMS091	AT %MX1.3325.10 :	BOOL;(*Boom hyd. brake LS release fault*)
	B_CMS092	AT %MX1.3325.11 :	BOOL;(*Emergency brake Hyd. station run permit*)
	B_CMS093	AT %MX1.3325.12 :	BOOL;(*Boom drum rope overlap#1 fault*)
	B_CMS094	AT %MX1.3325.13 :	BOOL;(*Boom drum rope overlap#2 fault*)
	B_CMS095	AT %MX1.3325.14 :	BOOL;(*Trolley/boom inv fan power R1 T1 CB*)
	B_CMS096	AT %MX1.3325.15 :	BOOL;(*Boom run time check fault*)
	B_CMS097	AT %MX1.3326.0 :	BOOL;(*Boom command lost while brake open fault*)
	B_CMS098	AT %MX1.3326.1 :	BOOL;(*Boom changeover motor main contactor check fault*)
	B_CMS099	AT %MX1.3326.2 :	BOOL;(*Boom changeover encoder relay check fault*)
	B_CMS100	AT %MX1.3326.3 :	BOOL;(*Boom emergency stop relay check fault*)
	B_CMS101	AT %MX1.3326.4 :	BOOL;(*Boom emergency stop contactor check fault*)
	B_CMS102	AT %MX1.3326.5 :	BOOL;(*Boom up slowdown LS position check fault*)
	B_CMS103	AT %MX1.3326.6 :	BOOL;(*Boom down slowdown LS position check fault*)
	B_CMS104	AT %MX1.3326.7 :	BOOL;(*Boom up slowdown LS speed check fault*)
	B_CMS105	AT %MX1.3326.8 :	BOOL;(*Boom down slowdown LS speed check fault*)
	B_CMS106	AT %MX1.3326.9 :	BOOL;(*Bes*)
	B_CMS107	AT %MX1.3326.10 :	BOOL;
	B_CMS108	AT %MX1.3326.11 :	BOOL;(*No e_stop*)
	B_CMS109	AT %MX1.3326.12 :	BOOL;(*Control on*)
	B_CMS110	AT %MX1.3326.13 :	BOOL;(*Sx bus ok*)
	B_CMS111	AT %MX1.3326.14 :	BOOL;(*RHC run permit*)
	B_CMS112	AT %MX1.3326.15 :	BOOL;(*RHC#1 fault*)
	B_CMS113	AT %MX1.3327.0 :	BOOL;(*RHC#2 fault*)
	B_CMS114	AT %MX1.3327.1 :	BOOL;(*Trolley/boom inv fault*)
	B_CMS115	AT %MX1.3327.2 :	BOOL;(*Boom overspeed fault*)
	B_CMS116	AT %MX1.3327.3 :	BOOL;(*Boom speed setting and feedback disagree fault*)
	B_CMS117	AT %MX1.3327.4 :	BOOL;(*Trolley/boom inv sx communication error*)
	B_CMS118	AT %MX1.3327.5 :	BOOL;(*Boom up over travel fault*)
	B_CMS119	AT %MX1.3327.6 :	BOOL;(*Bes*)
	B_CMS120	AT %MX1.3327.7 :	BOOL;(*Besx*)
	B_CMS121	AT %MX1.3327.8 :	BOOL;
	B_CMS122	AT %MX1.3327.9 :	BOOL;(*Boom latch#1 in LS*)
	B_CMS123	AT %MX1.3327.10 :	BOOL;(*Boom latch#2 in LS*)
	B_CMS124	AT %MX1.3327.11 :	BOOL;
	B_CMS125	AT %MX1.3327.12 :	BOOL;
	B_CMS126	AT %MX1.3327.13 :	BOOL;
	B_CMS127	AT %MX1.3327.14 :	BOOL;
	B_CMS128	AT %MX1.3327.15 :	BOOL;
	B_CMS129	AT %MW1.3328 :	INT;(*Boom motor voltage (Rated:450V)*)
	B_CMS130	AT %MW1.3329 :	INT;(*Boom motor current (Rated:570A)*)
	B_CMS131	AT %MW1.3330 :	INT;(*Boom motor speed feedback (Rated:1750rpm)*)
	B_CMS132	AT %MW1.3331 :	INT;(*Boom position (cm)*)
	B_CMS133	AT %MW1.3332 :	INT;(*Boom motor temperature*)
	B_CMS134	AT %MW1.3333 :	WORD;
	B_CMS135	AT %MW1.3334 :	WORD;
	B_CMS136	AT %MW1.3335 :	WORD;
	B_CMS137	AT %MW1.3336 :	WORD;
	B_CMS138	AT %MW1.3337 :	WORD;
	B_CMS139	AT %MW1.3338 :	WORD;
	B_CMS140	AT %MW1.3339 :	WORD;
	B_CMS141	AT %MW1.3340 :	WORD;
	B_CMS142	AT %MW1.3341 :	WORD;
	B_CMS143	AT %MW1.3342 :	WORD;
	B_CMS144	AT %MW1.3343 :	WORD;
	B_CMS145	AT %MW1.3344 :	WORD;
	B_CMS146	AT %MW1.3345 :	WORD;
	B_CMS147	AT %MW1.3346 :	WORD;
	B_CMS148	AT %MW1.3347 :	WORD;
	B_CMS149	AT %MW1.3348 :	WORD;
	B_CMS150	AT %MW1.3349 :	WORD;
	B_CMS151	AT %MW1.3350 :	WORD;
	B_CMS152	AT %MW1.3351 :	WORD;
	B_CMS153	AT %MW1.3352 :	WORD;
	B_CMS154	AT %MW1.3353 :	WORD;
	B_CMS155	AT %MW1.3354 :	WORD;
	B_CMS156	AT %MW1.3355 :	WORD;
	B_CMS157	AT %MW1.3356 :	WORD;
	B_CMS158	AT %MW1.3357 :	WORD;
	B_CMS159	AT %MW1.3358 :	WORD;
	B_CMS160	AT %MW1.3359 :	WORD;
	G_CMS001	AT %MX1.3360.0 :	BOOL;(*Gantry left permit*)
	G_CMS002	AT %MX1.3360.1 :	BOOL;(*Gantry right permit*)
	G_CMS003	AT %MX1.3360.2 :	BOOL;(*Gantry run permit*)
	G_CMS004	AT %MX1.3360.3 :	BOOL;(*Gantry left request*)
	G_CMS005	AT %MX1.3360.4 :	BOOL;(*Gantry right request*)
	G_CMS006	AT %MX1.3360.5 :	BOOL;(*Gantry left command*)
	G_CMS007	AT %MX1.3360.6 :	BOOL;(*Gantry right command*)
	G_CMS008	AT %MX1.3360.7 :	BOOL;(*Gantry left slowdown command*)
	G_CMS009	AT %MX1.3360.8 :	BOOL;(*Gantry right slowdown command*)
	G_CMS010	AT %MX1.3360.9 :	BOOL;(*Gantry slowdown command*)
	G_CMS011	AT %MX1.3360.10 :	BOOL;(*Gantry emergency stop relay*)
	G_CMS012	AT %MX1.3360.11 :	BOOL;(*Gantry emergency stop contactor*)
	G_CMS013	AT %MX1.3360.12 :	BOOL;(*Gantry left stop LS*)
	G_CMS014	AT %MX1.3360.13 :	BOOL;(*Gantry right stop LS*)
	G_CMS015	AT %MX1.3360.14 :	BOOL;(*Gantry left slowdown LS*)
	G_CMS016	AT %MX1.3360.15 :	BOOL;(*Gantry right slowdown LS*)
	G_CMS017	AT %MX1.3361.0 :	BOOL;(*Gantry brake contactor FB*)
	G_CMS018	AT %MX1.3361.1 :	BOOL;(*Gantry brake#1 LS released*)
	G_CMS019	AT %MX1.3361.2 :	BOOL;(*Gantry brake#2 LS released*)
	G_CMS020	AT %MX1.3361.3 :	BOOL;(*Gantry brake#3 LS released*)
	G_CMS021	AT %MX1.3361.4 :	BOOL;(*Gantry brake#4 LS released*)
	G_CMS022	AT %MX1.3361.5 :	BOOL;(*Gantry brake#5 LS released*)
	G_CMS023	AT %MX1.3361.6 :	BOOL;(*Gantry brake#6 LS released*)
	G_CMS024	AT %MX1.3361.7 :	BOOL;(*Gantry brake#7 LS released*)
	G_CMS025	AT %MX1.3361.8 :	BOOL;(*Gantry brake#8 LS released*)
	G_CMS026	AT %MX1.3361.9 :	BOOL;(*Gantry brake#9 LS released*)
	G_CMS027	AT %MX1.3361.10 :	BOOL;(*Gantry brake#10 LS released*)
	G_CMS028	AT %MX1.3361.11 :	BOOL;(*Gantry brake#11 LS released*)
	G_CMS029	AT %MX1.3361.12 :	BOOL;(*Gantry brake#12 LS released*)
	G_CMS030	AT %MX1.3361.13 :	BOOL;(*Gantry brake#13 LS released*)
	G_CMS031	AT %MX1.3361.14 :	BOOL;(*Gantry brake#14 LS released*)
	G_CMS032	AT %MX1.3361.15 :	BOOL;(*Gantry brake#15 LS released*)
	G_CMS033	AT %MX1.3362.0 :	BOOL;(*Gantry brake#16 LS released*)
	G_CMS034	AT %MX1.3362.1 :	BOOL;(*Gantry brake#17 LS released*)
	G_CMS035	AT %MX1.3362.2 :	BOOL;(*Gantry brake#18 LS released*)
	G_CMS036	AT %MX1.3362.3 :	BOOL;(*Gantry brake#19 LS released*)
	G_CMS037	AT %MX1.3362.4 :	BOOL;(*Gantry brake#20 LS released*)
	G_CMS038	AT %MX1.3362.5 :	BOOL;(*Gantry brake#1 manual LS released*)
	G_CMS039	AT %MX1.3362.6 :	BOOL;(*Gantry brake#2 manual LS released*)
	G_CMS040	AT %MX1.3362.7 :	BOOL;(*Gantry brake#3 manual LS released*)
	G_CMS041	AT %MX1.3362.8 :	BOOL;(*Gantry brake#4 manual LS released*)
	G_CMS042	AT %MX1.3362.9 :	BOOL;(*Gantry brake#5 manual LS released*)
	G_CMS043	AT %MX1.3362.10 :	BOOL;(*Gantry brake#6 manual LS released*)
	G_CMS044	AT %MX1.3362.11 :	BOOL;(*Gantry brake#7 manual LS released*)
	G_CMS045	AT %MX1.3362.12 :	BOOL;(*Gantry brake#8 manual LS released*)
	G_CMS046	AT %MX1.3362.13 :	BOOL;(*Gantry brake#9 manual LS released*)
	G_CMS047	AT %MX1.3362.14 :	BOOL;(*Gantry brake#10 manual LS released*)
	G_CMS048	AT %MX1.3362.15 :	BOOL;(*Gantry brake#11 manual LS released*)
	G_CMS049	AT %MX1.3363.0 :	BOOL;(*Gantry brake#12 manual LS released*)
	G_CMS050	AT %MX1.3363.1 :	BOOL;(*Gantry brake#13 manual LS released*)
	G_CMS051	AT %MX1.3363.2 :	BOOL;(*Gantry brake#14 manual LS released*)
	G_CMS052	AT %MX1.3363.3 :	BOOL;(*Gantry brake#15 manual LS released*)
	G_CMS053	AT %MX1.3363.4 :	BOOL;(*Gantry brake#16 manual LS released*)
	G_CMS054	AT %MX1.3363.5 :	BOOL;(*Gantry brake#17 manual LS released*)
	G_CMS055	AT %MX1.3363.6 :	BOOL;(*Gantry brake#18 manual LS released*)
	G_CMS056	AT %MX1.3363.7 :	BOOL;(*Gantry brake#19 manual LS released*)
	G_CMS057	AT %MX1.3363.8 :	BOOL;(*Gantry brake#20 manual LS released*)
	G_CMS058	AT %MX1.3363.9 :	BOOL;(*Gantry brake#1 LS wear*)
	G_CMS059	AT %MX1.3363.10 :	BOOL;(*Gantry brake#2 LS wear*)
	G_CMS060	AT %MX1.3363.11 :	BOOL;(*Gantry brake#3 LS wear*)
	G_CMS061	AT %MX1.3363.12 :	BOOL;(*Gantry brake#4 LS wear*)
	G_CMS062	AT %MX1.3363.13 :	BOOL;(*Gantry brake#5 LS wear*)
	G_CMS063	AT %MX1.3363.14 :	BOOL;(*Gantry brake#6 LS wear*)
	G_CMS064	AT %MX1.3363.15 :	BOOL;(*Gantry brake#7 LS wear*)
	G_CMS065	AT %MX1.3364.0 :	BOOL;(*Gantry brake#8 LS wear*)
	G_CMS066	AT %MX1.3364.1 :	BOOL;(*Gantry brake#9 LS wear*)
	G_CMS067	AT %MX1.3364.2 :	BOOL;(*Gantry brake#10 LS wear*)
	G_CMS068	AT %MX1.3364.3 :	BOOL;(*Gantry brake#11 LS wear*)
	G_CMS069	AT %MX1.3364.4 :	BOOL;(*Gantry brake#12 LS wear*)
	G_CMS070	AT %MX1.3364.5 :	BOOL;(*Gantry brake#13 LS wear*)
	G_CMS071	AT %MX1.3364.6 :	BOOL;(*Gantry brake#14 LS wear*)
	G_CMS072	AT %MX1.3364.7 :	BOOL;(*Gantry brake#15 LS wear*)
	G_CMS073	AT %MX1.3364.8 :	BOOL;(*Gantry brake#16 LS wear*)
	G_CMS074	AT %MX1.3364.9 :	BOOL;(*Gantry brake#17 LS wear*)
	G_CMS075	AT %MX1.3364.10 :	BOOL;(*Gantry brake#18 LS wear*)
	G_CMS076	AT %MX1.3364.11 :	BOOL;(*Gantry brake#19 LS wear*)
	G_CMS077	AT %MX1.3364.12 :	BOOL;(*Gantry brake#20 LS wear*)
	G_CMS078	AT %MX1.3364.13 :	BOOL;(*Gantry warning device contactor FB*)
	G_CMS079	AT %MX1.3364.14 :	BOOL;(*Gantry motor#1 over temp. SW*)
	G_CMS080	AT %MX1.3364.15 :	BOOL;(*Gantry motor#2 over temp. SW*)
	G_CMS081	AT %MX1.3365.0 :	BOOL;(*Gantry motor#3 over temp. SW*)
	G_CMS082	AT %MX1.3365.1 :	BOOL;(*Gantry motor#4 over temp. SW*)
	G_CMS083	AT %MX1.3365.2 :	BOOL;(*Gantry motor#5 over temp. SW*)
	G_CMS084	AT %MX1.3365.3 :	BOOL;(*Gantry motor#6 over temp. SW*)
	G_CMS085	AT %MX1.3365.4 :	BOOL;(*Gantry motor#7 over temp. SW*)
	G_CMS086	AT %MX1.3365.5 :	BOOL;(*Gantry motor#8 over temp. SW*)
	G_CMS087	AT %MX1.3365.6 :	BOOL;(*Gantry motor#9 over temp. SW*)
	G_CMS088	AT %MX1.3365.7 :	BOOL;(*Gantry motor#10 over temp. SW*)
	G_CMS089	AT %MX1.3365.8 :	BOOL;(*Gantry motor#11 over temp. SW*)
	G_CMS090	AT %MX1.3365.9 :	BOOL;(*Gantry motor#12 over temp. SW*)
	G_CMS091	AT %MX1.3365.10 :	BOOL;(*Gantry motor#13 over temp. SW*)
	G_CMS092	AT %MX1.3365.11 :	BOOL;(*Gantry motor#14 over temp. SW*)
	G_CMS093	AT %MX1.3365.12 :	BOOL;(*Gantry motor#15 over temp. SW*)
	G_CMS094	AT %MX1.3365.13 :	BOOL;(*Gantry motor#16 over temp. SW*)
	G_CMS095	AT %MX1.3365.14 :	BOOL;(*Gantry motor#17 over temp. SW*)
	G_CMS096	AT %MX1.3365.15 :	BOOL;(*Gantry motor#18 over temp. SW*)
	G_CMS097	AT %MX1.3366.0 :	BOOL;(*Gantry motor#19 over temp. SW*)
	G_CMS098	AT %MX1.3366.1 :	BOOL;(*Gantry motor#20 over temp. SW*)
	G_CMS099	AT %MX1.3366.2 :	BOOL;(*Gantry stowed landside LS*)
	G_CMS100	AT %MX1.3366.3 :	BOOL;(*Gantry stowed waterside LS*)
	G_CMS101	AT %MX1.3366.4 :	BOOL;(*GCR no fault signal*)
	G_CMS102	AT %MX1.3366.5 :	BOOL;(*GCR run signal*)
	G_CMS103	AT %MX1.3366.6 :	BOOL;(*GCR local selected*)
	G_CMS104	AT %MX1.3366.7 :	BOOL;(*GCR reel in signal*)
	G_CMS105	AT %MX1.3366.8 :	BOOL;(*GCR reel out signal*)
	G_CMS106	AT %MX1.3366.9 :	BOOL;(*GCR over temp. signal*)
	G_CMS107	AT %MX1.3366.10 :	BOOL;(*GCR drum empty signal*)
	G_CMS108	AT %MX1.3366.11 :	BOOL;(*GCR drum full signal*)
	G_CMS109	AT %MX1.3366.12 :	BOOL;(*GCR guide left LS*)
	G_CMS110	AT %MX1.3366.13 :	BOOL;(*GCR guide right LS*)
	G_CMS111	AT %MX1.3366.14 :	BOOL;(*GCR slack LS*)
	G_CMS112	AT %MX1.3366.15 :	BOOL;(*GCR overtension left LS*)
	G_CMS113	AT %MX1.3367.0 :	BOOL;(*GCR overtension right LS*)
	G_CMS114	AT %MX1.3367.1 :	BOOL;(*Waterside wheel clamp hyd. pump contactor FB*)
	G_CMS115	AT %MX1.3367.2 :	BOOL;(*Landside wheel clamp hyd. pump contactor FB*)
	G_CMS116	AT %MX1.3367.3 :	BOOL;(*Wheel clamp#1 LS released*)
	G_CMS117	AT %MX1.3367.4 :	BOOL;(*Wheel clamp#2 LS released*)
	G_CMS118	AT %MX1.3367.5 :	BOOL;(*Wheel clamp#3 LS released*)
	G_CMS119	AT %MX1.3367.6 :	BOOL;(*Wheel clamp#4 LS released*)
	G_CMS120	AT %MX1.3367.7 :	BOOL;(*Wheel clamp#5 LS released*)
	G_CMS121	AT %MX1.3367.8 :	BOOL;(*Wheel clamp#6 LS released*)
	G_CMS122	AT %MX1.3367.9 :	BOOL;(*Wheel clamp#7 LS released*)
	G_CMS123	AT %MX1.3367.10 :	BOOL;(*Wheel clamp#8 LS released*)
	G_CMS124	AT %MX1.3367.11 :	BOOL;(*Wheel clamp#9 LS released*)
	G_CMS125	AT %MX1.3367.12 :	BOOL;(*Wheel clamp#10 LS released*)
	G_CMS126	AT %MX1.3367.13 :	BOOL;(*Wheel clamp#11 LS released*)
	G_CMS127	AT %MX1.3367.14 :	BOOL;(*Wheel clamp#12 LS released*)
	G_CMS128	AT %MX1.3367.15 :	BOOL;(*Wheel clamp#13 LS released*)
	G_CMS129	AT %MX1.3368.0 :	BOOL;(*Wheel clamp#14 LS released*)
	G_CMS130	AT %MX1.3368.1 :	BOOL;(*Wheel clamp#15 LS released*)
	G_CMS131	AT %MX1.3368.2 :	BOOL;(*Wheel clamp#16 LS released*)
	G_CMS132	AT %MX1.3368.3 :	BOOL;(*Gantry masterswitch off*)
	G_CMS133	AT %MX1.3368.4 :	BOOL;(*Gantry left stop LS*)
	G_CMS134	AT %MX1.3368.5 :	BOOL;(*Gantry left permit by GCR*)
	G_CMS135	AT %MX1.3368.6 :	BOOL;(*Boom left anti-collisiton fault*)
	G_CMS136	AT %MX1.3368.7 :	BOOL;(*Gantry left permit*)
	G_CMS137	AT %MX1.3368.8 :	BOOL;
	G_CMS138	AT %MX1.3368.9 :	BOOL;(*Gantry right stop LS*)
	G_CMS139	AT %MX1.3368.10 :	BOOL;(*Gantry right permit by GCR*)
	G_CMS140	AT %MX1.3368.11 :	BOOL;(*Boom right anti-collisiton fault*)
	G_CMS141	AT %MX1.3368.12 :	BOOL;(*Gantry right permit*)
	G_CMS142	AT %MX1.3368.13 :	BOOL;
	G_CMS143	AT %MX1.3368.14 :	BOOL;(*Control on at Cab*)
	G_CMS144	AT %MX1.3368.15 :	BOOL;(*Control on at GOS station*)
	G_CMS145	AT %MX1.3369.0 :	BOOL;(*Boom level*)
	G_CMS146	AT %MX1.3369.1 :	BOOL;(*Boom latch in*)
	G_CMS147	AT %MX1.3369.2 :	BOOL;(*Spreader unlocked*)
	G_CMS148	AT %MX1.3369.3 :	BOOL;(*Boom command*)
	G_CMS149	AT %MX1.3369.4 :	BOOL;(*Hoist command*)
	G_CMS150	AT %MX1.3369.5 :	BOOL;(*Gantry landside stow pin not released*)
	G_CMS151	AT %MX1.3369.6 :	BOOL;(*Gantry waterside stow pin not released*)
	G_CMS152	AT %MX1.3369.7 :	BOOL;(*Gantry motor over temp. trip*)
	G_CMS153	AT %MX1.3369.8 :	BOOL;(*Gantry brake wear fault*)
	G_CMS154	AT %MX1.3369.9 :	BOOL;(*Wheel clamp run permit*)
	G_CMS155	AT %MX1.3369.10 :	BOOL;(*High wind trip*)
	G_CMS156	AT %MX1.3369.11 :	BOOL;(*Spreader landed timer*)
	G_CMS157	AT %MX1.3369.12 :	BOOL;(*Spreader rope slack by loadcell*)
	G_CMS158	AT %MX1.3369.13 :	BOOL;(*Spreader rope slack*)
	G_CMS159	AT %MX1.3369.14 :	BOOL;(*Spreader mode*)
	G_CMS160	AT %MX1.3369.15 :	BOOL;(*Snag fault*)
	G_CMS161	AT %MX1.3370.0 :	BOOL;(*Snag trip by loadcell*)
	G_CMS162	AT %MX1.3370.1 :	BOOL;(*loadcell wire break fault*)
	G_CMS163	AT %MX1.3370.2 :	BOOL;(*Hoist overload fault*)
	G_CMS164	AT %MX1.3370.3 :	BOOL;(*Hoist eccentric load fault*)
	G_CMS165	AT %MX1.3370.4 :	BOOL;(*RHC run permit*)
	G_CMS166	AT %MX1.3370.5 :	BOOL;(*Gantry emergency stop contactor FB*)
	G_CMS167	AT %MX1.3370.6 :	BOOL;(*Gantry run permit*)
	G_CMS168	AT %MX1.3370.7 :	BOOL;
	G_CMS169	AT %MX1.3370.8 :	BOOL;(*Gantry motor#1 power CB*)
	G_CMS170	AT %MX1.3370.9 :	BOOL;(*Gantry motor#2 power CB*)
	G_CMS171	AT %MX1.3370.10 :	BOOL;(*Gantry motor#3 power CB*)
	G_CMS172	AT %MX1.3370.11 :	BOOL;(*Gantry motor#4 power CB*)
	G_CMS173	AT %MX1.3370.12 :	BOOL;(*Gantry motor#5 power CB*)
	G_CMS174	AT %MX1.3370.13 :	BOOL;(*Gantry motor#6 power CB*)
	G_CMS175	AT %MX1.3370.14 :	BOOL;(*Gantry motor#7 power CB*)
	G_CMS176	AT %MX1.3370.15 :	BOOL;(*Gantry motor#8 power CB*)
	G_CMS177	AT %MX1.3371.0 :	BOOL;(*Gantry motor#9 power CB*)
	G_CMS178	AT %MX1.3371.1 :	BOOL;(*Gantry motor#10 power CB*)
	G_CMS179	AT %MX1.3371.2 :	BOOL;(*Gantry motor#11 power CB*)
	G_CMS180	AT %MX1.3371.3 :	BOOL;(*Gantry motor#12 power CB*)
	G_CMS181	AT %MX1.3371.4 :	BOOL;(*Gantry motor#13 power CB*)
	G_CMS182	AT %MX1.3371.5 :	BOOL;(*Gantry motor#14 power CB*)
	G_CMS183	AT %MX1.3371.6 :	BOOL;(*Gantry motor#15 power CB*)
	G_CMS184	AT %MX1.3371.7 :	BOOL;(*Gantry motor#16 power CB*)
	G_CMS185	AT %MX1.3371.8 :	BOOL;(*Gantry motor#17 power CB*)
	G_CMS186	AT %MX1.3371.9 :	BOOL;(*Gantry motor#18 power CB*)
	G_CMS187	AT %MX1.3371.10 :	BOOL;(*Gantry motor#19 power CB*)
	G_CMS188	AT %MX1.3371.11 :	BOOL;(*Gantry motor#20 power CB*)
	G_CMS189	AT %MX1.3371.12 :	BOOL;(*Gantry brake power CB*)
	G_CMS190	AT %MX1.3371.13 :	BOOL;(*Gantry brake#1 power CB*)
	G_CMS191	AT %MX1.3371.14 :	BOOL;(*Gantry brake#2 power CB*)
	G_CMS192	AT %MX1.3371.15 :	BOOL;(*Gantry brake#3 power CB*)
	G_CMS193	AT %MX1.3372.0 :	BOOL;(*Gantry brake#4 power CB*)
	G_CMS194	AT %MX1.3372.1 :	BOOL;(*Gantry brake#5 power CB*)
	G_CMS195	AT %MX1.3372.2 :	BOOL;(*Gantry brake#6 power CB*)
	G_CMS196	AT %MX1.3372.3 :	BOOL;(*Gantry brake#7 power CB*)
	G_CMS197	AT %MX1.3372.4 :	BOOL;(*Gantry brake#8 power CB*)
	G_CMS198	AT %MX1.3372.5 :	BOOL;(*Gantry brake#9 power CB*)
	G_CMS199	AT %MX1.3372.6 :	BOOL;(*Gantry brake#10 power CB*)
	G_CMS200	AT %MX1.3372.7 :	BOOL;(*Gantry brake#11 power CB*)
	G_CMS201	AT %MX1.3372.8 :	BOOL;(*Gantry brake#12 power CB*)
	G_CMS202	AT %MX1.3372.9 :	BOOL;(*Gantry brake#13 power CB*)
	G_CMS203	AT %MX1.3372.10 :	BOOL;(*Gantry brake#14 power CB*)
	G_CMS204	AT %MX1.3372.11 :	BOOL;(*Gantry brake#15 power CB*)
	G_CMS205	AT %MX1.3372.12 :	BOOL;(*Gantry brake#16 power CB*)
	G_CMS206	AT %MX1.3372.13 :	BOOL;(*Gantry brake#17 power CB*)
	G_CMS207	AT %MX1.3372.14 :	BOOL;(*Gantry brake#18 power CB*)
	G_CMS208	AT %MX1.3372.15 :	BOOL;(*Gantry brake#19 power CB*)
	G_CMS209	AT %MX1.3373.0 :	BOOL;(*Gantry brake#20 power CB*)
	G_CMS210	AT %MX1.3373.1 :	BOOL;(*H/G safety relay power CB*)
	G_CMS211	AT %MX1.3373.2 :	BOOL;(*Gantry brake contactor check fault*)
	G_CMS212	AT %MX1.3373.3 :	BOOL;(*Gantry brake LS release fault*)
	G_CMS213	AT %MX1.3373.4 :	BOOL;(*Gantry brake manual release LS open fault*)
	G_CMS214	AT %MX1.3373.5 :	BOOL;(*Gantry inv fan power R1 T1 CB*)
	G_CMS215	AT %MX1.3373.6 :	BOOL;(*Gantry run time check fault*)
	G_CMS216	AT %MX1.3373.7 :	BOOL;(*Gantry command lost while brake open fault*)
	G_CMS217	AT %MX1.3373.8 :	BOOL;(*Gantry emergency stop relay check fault*)
	G_CMS218	AT %MX1.3373.9 :	BOOL;(*Gantry emergency stop contactor check fault*)
	G_CMS219	AT %MX1.3373.10 :	BOOL;(*GCR run permit*)
	G_CMS220	AT %MX1.3373.11 :	BOOL;(*Boom left anti-collisiton fault*)
	G_CMS221	AT %MX1.3373.12 :	BOOL;(*Boom right anti-collisiton fault*)
	G_CMS222	AT %MX1.3373.13 :	BOOL;(*Ges*)
	G_CMS223	AT %MX1.3373.14 :	BOOL;
	G_CMS224	AT %MX1.3373.15 :	BOOL;(*No e_stop*)
	G_CMS225	AT %MX1.3374.0 :	BOOL;(*Control on*)
	G_CMS226	AT %MX1.3374.1 :	BOOL;(*Sx bus ok*)
	G_CMS227	AT %MX1.3374.2 :	BOOL;(*RHC run permit*)
	G_CMS228	AT %MX1.3374.3 :	BOOL;(*RHC#1 fault*)
	G_CMS229	AT %MX1.3374.4 :	BOOL;(*RHC#2 fault*)
	G_CMS230	AT %MX1.3374.5 :	BOOL;(*Gantry inv fault*)
	G_CMS231	AT %MX1.3374.6 :	BOOL;(*Gantry inv sx communication error*)
	G_CMS232	AT %MX1.3374.7 :	BOOL;(*Ges*)
	G_CMS233	AT %MX1.3374.8 :	BOOL;(*Gesx*)
	G_CMS234	AT %MX1.3374.9 :	BOOL;
	G_CMS235	AT %MX1.3374.10 :	BOOL;
	G_CMS236	AT %MX1.3374.11 :	BOOL;
	G_CMS237	AT %MX1.3374.12 :	BOOL;
	G_CMS238	AT %MX1.3374.13 :	BOOL;
	G_CMS239	AT %MX1.3374.14 :	BOOL;
	G_CMS240	AT %MX1.3374.15 :	BOOL;
	G_CMS241	AT %MW1.3375 :	INT;(*Gantry masterswitch reference in*)
	G_CMS242	AT %MW1.3376 :	INT;(*Gantry motor voltage (Rated:440V)*)
	G_CMS243	AT %MW1.3377 :	INT;(*Gantry motor current (Rated:66A)*)
	G_CMS244	AT %MW1.3378 :	INT;(*Gantry motor speed feedback (Rated:1460rpm, 46m/min)*)
	G_CMS245	AT %MW1.3379 :	WORD;(*GCR status word*)
	G_CMS246	AT %MW1.3380 :	INT;(*GCR actual value*)
	G_CMS247	AT %MW1.3381 :	INT;(*GCR output frequency*)
	G_CMS248	AT %MW1.3382 :	INT;(*GCR motor current*)
	G_CMS249	AT %MW1.3383 :	INT;(*GCR motor torque*)
	G_CMS250	AT %MW1.3384 :	INT;(*GCR active fault code*)
	G_CMS251	AT %MW1.3385 :	WORD;
	G_CMS252	AT %MW1.3386 :	WORD;
	G_CMS253	AT %MW1.3387 :	WORD;
	G_CMS254	AT %MW1.3388 :	WORD;
	G_CMS255	AT %MW1.3389 :	WORD;
	G_CMS256	AT %MW1.3390 :	WORD;
	G_CMS257	AT %MW1.3391 :	WORD;
	G_CMS258	AT %MW1.3392 :	WORD;
	G_CMS259	AT %MW1.3393 :	WORD;
	G_CMS260	AT %MW1.3394 :	WORD;
	G_CMS261	AT %MW1.3395 :	WORD;
	G_CMS262	AT %MW1.3396 :	WORD;
	G_CMS263	AT %MW1.3397 :	WORD;
	G_CMS264	AT %MW1.3398 :	WORD;
	G_CMS265	AT %MW1.3399 :	WORD;
	TLS_CMS001	AT %MX1.3400.0 :	BOOL;(*TLS emergency stop*)
	TLS_CMS002	AT %MX1.3400.1 :	BOOL;(*TLS run permit*)
	TLS_CMS003	AT %MX1.3400.2 :	BOOL;(*TLS auto run permit*)
	TLS_CMS004	AT %MX1.3400.3 :	BOOL;(*Trim left command*)
	TLS_CMS005	AT %MX1.3400.4 :	BOOL;(*Trim right command*)
	TLS_CMS006	AT %MX1.3400.5 :	BOOL;(*List forward command*)
	TLS_CMS007	AT %MX1.3400.6 :	BOOL;(*List reverse command*)
	TLS_CMS008	AT %MX1.3400.7 :	BOOL;(*Skew left command*)
	TLS_CMS009	AT %MX1.3400.8 :	BOOL;(*Skew right command*)
	TLS_CMS010	AT %MX1.3400.9 :	BOOL;(*TLS home command*)
	TLS_CMS011	AT %MX1.3400.10 :	BOOL;(*TLS#1 forward command*)
	TLS_CMS012	AT %MX1.3400.11 :	BOOL;(*TLS#1 reverse command*)
	TLS_CMS013	AT %MX1.3400.12 :	BOOL;(*TLS#2 forward command*)
	TLS_CMS014	AT %MX1.3400.13 :	BOOL;(*TLS#2 reverse command*)
	TLS_CMS015	AT %MX1.3400.14 :	BOOL;(*TLS#3 forward command*)
	TLS_CMS016	AT %MX1.3400.15 :	BOOL;(*TLS#3 reverse command*)
	TLS_CMS017	AT %MX1.3401.0 :	BOOL;(*TLS#4 forward command*)
	TLS_CMS018	AT %MX1.3401.1 :	BOOL;(*TLS#4 reverse command*)
	TLS_CMS019	AT %MX1.3401.2 :	BOOL;(*TLS#1 brake contactor FB*)
	TLS_CMS020	AT %MX1.3401.3 :	BOOL;(*TLS#2 brake contactor FB*)
	TLS_CMS021	AT %MX1.3401.4 :	BOOL;(*TLS#3 brake contactor FB*)
	TLS_CMS022	AT %MX1.3401.5 :	BOOL;(*TLS#4 brake contactor FB*)
	TLS_CMS023	AT %MX1.3401.6 :	BOOL;(*TLS#1 brake LS released*)
	TLS_CMS024	AT %MX1.3401.7 :	BOOL;(*TLS#2 brake LS released*)
	TLS_CMS025	AT %MX1.3401.8 :	BOOL;(*TLS#3 brake LS released*)
	TLS_CMS026	AT %MX1.3401.9 :	BOOL;(*TLS#4 brake LS released*)
	TLS_CMS027	AT %MX1.3401.10 :	BOOL;(*TLS#1 motor over temp. SW*)
	TLS_CMS028	AT %MX1.3401.11 :	BOOL;(*TLS#2 motor over temp. SW*)
	TLS_CMS029	AT %MX1.3401.12 :	BOOL;(*TLS#3 motor over temp. SW*)
	TLS_CMS030	AT %MX1.3401.13 :	BOOL;(*TLS#4 motor over temp. SW*)
	TLS_CMS031	AT %MX1.3401.14 :	BOOL;(*TLS#1 worm home LS*)
	TLS_CMS032	AT %MX1.3401.15 :	BOOL;(*TLS#2 worm home LS*)
	TLS_CMS033	AT %MX1.3402.0 :	BOOL;(*TLS#3 worm home LS*)
	TLS_CMS034	AT %MX1.3402.1 :	BOOL;(*TLS#4 worm home LS*)
	TLS_CMS035	AT %MX1.3402.2 :	BOOL;(*TLS#1 worm extend overtravel LS*)
	TLS_CMS036	AT %MX1.3402.3 :	BOOL;(*TLS#2 worm extend overtravel LS*)
	TLS_CMS037	AT %MX1.3402.4 :	BOOL;(*TLS#3 worm extend overtravel LS*)
	TLS_CMS038	AT %MX1.3402.5 :	BOOL;(*TLS#4 worm extend overtravel LS*)
	TLS_CMS039	AT %MX1.3402.6 :	BOOL;(*TLS#1 worm retract overtravel LS*)
	TLS_CMS040	AT %MX1.3402.7 :	BOOL;(*TLS#2 worm retract overtravel LS*)
	TLS_CMS041	AT %MX1.3402.8 :	BOOL;(*TLS#3 worm retract overtravel LS*)
	TLS_CMS042	AT %MX1.3402.9 :	BOOL;(*TLS#4 worm retract overtravel LS*)
	TLS_CMS043	AT %MX1.3402.10 :	BOOL;
	TLS_CMS044	AT %MX1.3402.11 :	BOOL;
	TLS_CMS045	AT %MX1.3402.12 :	BOOL;
	TLS_CMS046	AT %MX1.3402.13 :	BOOL;
	TLS_CMS047	AT %MX1.3402.14 :	BOOL;
	TLS_CMS048	AT %MX1.3402.15 :	BOOL;
	TLS_CMS049	AT %MW1.3403 :	INT;(*TLS#1 motor voltage (Rated:415V)*)
	TLS_CMS050	AT %MW1.3404 :	INT;(*TLS#1 motor current (Rated:24A)*)
	TLS_CMS051	AT %MW1.3405 :	INT;(*TLS#1 motor speed feedback (Rated:1500rpm)*)
	TLS_CMS052	AT %MW1.3406 :	INT;(*TLS#2 motor voltage (Rated:415V)*)
	TLS_CMS053	AT %MW1.3407 :	INT;(*TLS#2 motor current (Rated:24A)*)
	TLS_CMS054	AT %MW1.3408 :	INT;(*TLS#2 motor speed feedback (Rated:1500rpm)*)
	TLS_CMS055	AT %MW1.3409 :	INT;(*TLS#3 motor voltage (Rated:415V)*)
	TLS_CMS056	AT %MW1.3410 :	INT;(*TLS#3 motor current (Rated:24A)*)
	TLS_CMS057	AT %MW1.3411 :	INT;(*TLS#3 motor speed feedback (Rated:1500rpm)*)
	TLS_CMS058	AT %MW1.3412 :	INT;(*TLS#4 motor voltage (Rated:415V)*)
	TLS_CMS059	AT %MW1.3413 :	INT;(*TLS#4 motor current (Rated:24A)*)
	TLS_CMS060	AT %MW1.3414 :	INT;(*TLS#4 motor speed feedback (Rated:1500rpm)*)
	TLS_CMS061	AT %MW1.3415 :	INT;(*TLS#1 position (mm)*)
	TLS_CMS062	AT %MW1.3416 :	INT;(*TLS#2 position (mm)*)
	TLS_CMS063	AT %MW1.3417 :	INT;(*TLS#3 position (mm)*)
	TLS_CMS064	AT %MW1.3418 :	INT;(*TLS#4 position (mm)*)
	TLS_CMS065	AT %MW1.3419 :	INT;(*Trim angle*)
	TLS_CMS066	AT %MW1.3420 :	INT;(*List angle*)
	TLS_CMS067	AT %MW1.3421 :	INT;(*Skew angle*)
	TLS_CMS068	AT %MW1.3422 :	WORD;
	TLS_CMS069	AT %MW1.3423 :	WORD;
	TLS_CMS070	AT %MW1.3424 :	WORD;
	TLS_CMS071	AT %MW1.3425 :	WORD;
	TLS_CMS072	AT %MW1.3426 :	WORD;
	TLS_CMS073	AT %MW1.3427 :	WORD;
	TLS_CMS074	AT %MW1.3428 :	WORD;
	TLS_CMS075	AT %MW1.3429 :	WORD;
	TLS_CMS076	AT %MW1.3430 :	WORD;
	TLS_CMS077	AT %MW1.3431 :	WORD;
	TLS_CMS078	AT %MW1.3432 :	WORD;
	TLS_CMS079	AT %MW1.3433 :	WORD;
	TLS_CMS080	AT %MW1.3434 :	WORD;
	TLS_CMS081	AT %MW1.3435 :	WORD;
	TLS_CMS082	AT %MW1.3436 :	WORD;
	TLS_CMS083	AT %MW1.3437 :	WORD;
	TLS_CMS084	AT %MW1.3438 :	WORD;
	TLS_CMS085	AT %MW1.3439 :	WORD;
	SP_CMS001	AT %MX1.3440.0 :	BOOL;(*Spreader mode*)
	SP_CMS002	AT %MX1.3440.1 :	BOOL;(*Hook mode*)
	SP_CMS003	AT %MX1.3440.2 :	BOOL;(*Eye plate mode*)
	SP_CMS004	AT %MX1.3440.3 :	BOOL;(*Maintenance mode*)
	SP_CMS005	AT %MX1.3440.4 :	BOOL;(*Spreader pump power CB*)
	SP_CMS006	AT %MX1.3440.5 :	BOOL;(*Spreader control power CB*)
	SP_CMS007	AT %MX1.3440.6 :	BOOL;(*Spreader pump run permit*)
	SP_CMS008	AT %MX1.3440.7 :	BOOL;(*Spreader twist lock permit*)
	SP_CMS009	AT %MX1.3440.8 :	BOOL;(*Spreader telescope permit*)
	SP_CMS010	AT %MX1.3440.9 :	BOOL;(*Spreader twin down permit*)
	SP_CMS011	AT %MX1.3440.10 :	BOOL;(*Spreader twin move permit*)
	SP_CMS012	AT %MX1.3440.11 :	BOOL;(*Spreader 20 feet signal*)
	SP_CMS013	AT %MX1.3440.12 :	BOOL;(*Spreader 40 feet signal*)
	SP_CMS014	AT %MX1.3440.13 :	BOOL;(*Spreader 45 feet signal*)
	SP_CMS015	AT %MX1.3440.14 :	BOOL;(*Spreader left landed signal*)
	SP_CMS016	AT %MX1.3440.15 :	BOOL;(*Spreader right landed signal*)
	SP_CMS017	AT %MX1.3441.0 :	BOOL;(*Spreader TTDS fault signal*)
	SP_CMS018	AT %MX1.3441.1 :	BOOL;(*Spreader locked signal*)
	SP_CMS019	AT %MX1.3441.2 :	BOOL;(*Spreader unlocked signal*)
	SP_CMS020	AT %MX1.3441.3 :	BOOL;(*Spreader twistlock down signal*)
	SP_CMS021	AT %MX1.3441.4 :	BOOL;(*Spreader flipper up signal*)
	SP_CMS022	AT %MX1.3441.5 :	BOOL;(*Spreader headblock slack signal*)
	SP_CMS023	AT %MX1.3441.6 :	BOOL;(*Spreader pump contactor FB*)
	SP_CMS024	AT %MX1.3441.7 :	BOOL;(*Spreader land bypass relay*)
	SP_CMS025	AT %MX1.3441.8 :	BOOL;(*Spreader telescope extend relay*)
	SP_CMS026	AT %MX1.3441.9 :	BOOL;(*Spreader telescope retract relay*)
	SP_CMS027	AT %MX1.3441.10 :	BOOL;(*Spreader twin down relay*)
	SP_CMS028	AT %MX1.3441.11 :	BOOL;(*Spreader twist lock relay*)
	SP_CMS029	AT %MX1.3441.12 :	BOOL;(*Spreader twist unlock relay*)
	SP_CMS030	AT %MX1.3441.13 :	BOOL;(*Spreader flipper#1 down relay*)
	SP_CMS031	AT %MX1.3441.14 :	BOOL;(*Spreader flipper#2 down relay*)
	SP_CMS032	AT %MX1.3441.15 :	BOOL;(*Spreader flipper#3 down relay*)
	SP_CMS033	AT %MX1.3442.0 :	BOOL;(*Spreader flipper#4 down relay*)
	SP_CMS034	AT %MX1.3442.1 :	BOOL;(*Spreader TTDS bypass relay*)
	SP_CMS035	AT %MX1.3442.2 :	BOOL;(*Spreader memory position#1 set relay*)
	SP_CMS036	AT %MX1.3442.3 :	BOOL;(*Spreader return to memory position#1 relay*)
	SP_CMS037	AT %MX1.3442.4 :	BOOL;(*Spreader memory position#2 set relay*)
	SP_CMS038	AT %MX1.3442.5 :	BOOL;(*Spreader return to memory position#2 relay*)
	SP_CMS039	AT %MX1.3442.6 :	BOOL;
	SP_CMS040	AT %MX1.3442.7 :	BOOL;
	SP_CMS041	AT %MX1.3442.8 :	BOOL;
	SP_CMS042	AT %MX1.3442.9 :	BOOL;
	SP_CMS043	AT %MX1.3442.10 :	BOOL;
	SP_CMS044	AT %MX1.3442.11 :	BOOL;
	SP_CMS045	AT %MX1.3442.12 :	BOOL;
	SP_CMS046	AT %MX1.3442.13 :	BOOL;
	SP_CMS047	AT %MX1.3442.14 :	BOOL;
	SP_CMS048	AT %MX1.3442.15 :	BOOL;
	MAINT_CMS001	AT %MD1.3560 :	DINT;(*Hoist brake counter*)
	MAINT_CMS002	AT %MD1.3562 :	DINT;(*Gantry brake counter*)
	MAINT_CMS003	AT %MD1.3564 :	DINT;(*Trolley brake counter*)
	MAINT_CMS004	AT %MD1.3566 :	DINT;(*Boom brake counter*)
	MAINT_CMS005	AT %MD1.3568 :	DINT;(*TLS#1 brake counter*)
	MAINT_CMS006	AT %MD1.3570 :	DINT;(*TLS#2 brake counter*)
	MAINT_CMS007	AT %MD1.3572 :	DINT;(*TLS#3 brake counter*)
	MAINT_CMS008	AT %MD1.3574 :	DINT;(*TLS#4 brake counter*)
	MAINT_CMS009	AT %MD1.3576 :	DINT;(*Spreader twist lock counter*)
	MAINT_CMS010	AT %MD1.3578 :	DINT;(*Hoist run timer*)
	MAINT_CMS011	AT %MD1.3580 :	DINT;(*Gantry run timer*)
	MAINT_CMS012	AT %MD1.3582 :	DINT;(*Trolley run timer*)
	MAINT_CMS013	AT %MD1.3584 :	DINT;(*Boom run timer*)
	MAINT_CMS014	AT %MD1.3586 :	DINT;(*Hoist and trolley run timer*)
	MAINT_CMS015	AT %MD1.3588 :	DINT;(*TLS#1 run timer*)
	MAINT_CMS016	AT %MD1.3590 :	DINT;(*TLS#2 run timer*)
	MAINT_CMS017	AT %MD1.3592 :	DINT;(*TLS#3 run timer*)
	MAINT_CMS018	AT %MD1.3594 :	DINT;(*TLS#4 run timer*)
	MAINT_CMS019	AT %MD1.3596 :	DINT;(*Crane run timer*)
	MAINT_CMS020	AT %MD1.3598 :	DINT;
	MAINT_CMS021	AT %MD1.3600 :	DINT;
	MAINT_CMS022	AT %MD1.3602 :	DINT;
	MAINT_CMS023	AT %MD1.3604 :	DINT;
	MAINT_CMS024	AT %MD1.3606 :	DINT;
	MAINT_CMS025	AT %MD1.3608 :	DINT;
	MAINT_CMS026	AT %MD1.3610 :	DINT;
	MAINT_CMS027	AT %MD1.3612 :	DINT;
	MAINT_CMS028	AT %MD1.3614 :	DINT;
	MAINT_CMS029	AT %MD1.3616 :	DINT;
	MAINT_CMS030	AT %MD1.3618 :	DINT;
	MAINT_CMS031	AT %MX1.3620.0 :	BOOL;(*Counter reset from CMS*)
END_VAR

