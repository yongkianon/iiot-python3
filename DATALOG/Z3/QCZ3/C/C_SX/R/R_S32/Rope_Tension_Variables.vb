
(*Group:Rope_Tension_Variables*)


VAR_GLOBAL
	BHYDPCHK :	BOOL;(*BACKREACH HYDRAULIC PUMP CONTACTOR CHECK FAULT*)
	BHYDHCHK :	BOOL;(*BACKREACH HYDRAULIC PUMP HEATER CONTACTOR CHECK FAULT*)
	BHYDHITFLT :	BOOL;(*BACKREACH HYDRAULIC STATION HIGH TEMP. FAULT*)
	BHYDOTFLT :	BOOL;(*BACKREACH HYDRAULIC PUMP OVER TEMP. FAULT*)
	BHYDLOFLT :	BOOL;(*BACKREACH HYDRAULIC STATION OIL LOW LEVEL FAULT*)
	BHYDBVFLT :	BOOL;(*BACKREACH HYDRAULIC STATION BUTTERFLY VALVE CLOSE FAULT*)
	CATLOPFLT :	BOOL;(*CATENARY ROPE TENSION LOW PRESSURE FAULT*)
	CATHIPFLT :	BOOL;(*CATENARY ROPE TENSION HIGH PRESSURE FAULT*)
	CATOTFLT1 :	BOOL;(*CATENARY ROPE TENSION OVERTENSION#1 FAULT*)
	CATOTFLT2 :	BOOL;(*CATENARY ROPE TENSION OVERTENSION#2 FAULT*)
	CATSLKFLT1 :	BOOL;(*CATENARY ROPE TENSION SLACK#1 FAULT*)
	CATSLKFLT2 :	BOOL;(*CATENARY ROPE TENSION SLACK#2 FAULT*)
	BHSOL3RNTMR :	BOOL;(*BACKREACH HYDRAULIC STATION SOLENOID RUN TIME CHECK FAULT*)
	RTPRM :	BOOL;(*ROPE TENSION PERMIT*)
	RTATST :	BOOL;(*ROPE TENSION PUMP AUTO START*)
END_VAR

