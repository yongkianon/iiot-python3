
(*Group:TLS_Variables*)


VAR_GLOBAL
	TLSB1CHK :	BOOL;(*TLS BRAKE#1 CONTACTOR CHECK FAULT*)
	TLSB2CHK :	BOOL;(*TLS BRAKE#2 CONTACTOR CHECK FAULT*)
	TLSB3CHK :	BOOL;(*TLS BRAKE#3 CONTACTOR CHECK FAULT*)
	TLSB4CHK :	BOOL;(*TLS BRAKE#4 CONTACTOR CHECK FAULT*)
	TLSBR1CHK :	BOOL;(*TLS BRAKE#1 LS RELEASE CHECK FAULT*)
	TLSBR2CHK :	BOOL;(*TLS BRAKE#2 LS RELEASE CHECK FAULT*)
	TLSBR3CHK :	BOOL;(*TLS BRAKE#3 LS RELEASE CHECK FAULT*)
	TLSBR4CHK :	BOOL;(*TLS BRAKE#4 LS RELEASE CHECK FAULT*)
	TLSM1OTFLT :	BOOL;(*TLS MOTOR#1 OVER TEMP. FAULT*)
	TLSM2OTFLT :	BOOL;(*TLS MOTOR#2 OVER TEMP. FAULT*)
	TLSM3OTFLT :	BOOL;(*TLS MOTOR#3 OVER TEMP. FAULT*)
	TLSM4OTFLT :	BOOL;(*TLS MOTOR#4 OVER TEMP. FAULT*)
	TLSMHCHK :	BOOL;(*TLS MOTOR HEATER CONTACTOR CHECK FAULT*)
	TLS1RNTMR :	BOOL;(*TLS#1 RUN TIME CHECK FAULT*)
	TLS2RNTMR :	BOOL;(*TLS#2 RUN TIME CHECK FAULT*)
	TLS3RNTMR :	BOOL;(*TLS#3 RUN TIME CHECK FAULT*)
	TLS4RNTMR :	BOOL;(*TLS#4 RUN TIME CHECK FAULT*)
	TLSESP :	BOOL;(*TLS EMERGENCY STOP*)
	TLSRNPRM :	BOOL;(*TLS RUN PERMIT*)
	TLSAPRM :	BOOL;(*TLS AUTO RUN PERMIT*)
	TRMLREQ :	BOOL;(*TRIM LEFT REEQUEST*)
	TRMRREQ :	BOOL;(*TRIM RIGHT REEQUEST*)
	LSTFREQ :	BOOL;(*LIST FORWARD REQUEST*)
	LSTRREQ :	BOOL;(*LIST REVERSE REQUEST*)
	SKWLREQ :	BOOL;(*SKEW LEFT REQUEST*)
	SKWRREQ :	BOOL;(*SKEW RIGHT REQUEST*)
	TRMLCMD :	BOOL;(*TRIM LEFT COMMAND*)
	TRMRCMD :	BOOL;(*TRIM RIGHT COMMAND*)
	LSTFCMD :	BOOL;(*LIST FORWARD COMMAND*)
	LSTRCMD :	BOOL;(*LIST REVERSE COMMAND*)
	SKWLCMD :	BOOL;(*SKEW LEFT COMMAND*)
	SKWRCMD :	BOOL;(*SKEW RIGHT COMMAND*)
	TLSAREQ :	BOOL;(*TLS AUTO REQUEST*)
	TLS1AF :	BOOL;(*TLS#1 AUTO FORWARD*)
	TLS1AR :	BOOL;(*TLS#1 AUTO REVERSE*)
	TLS2AF :	BOOL;(*TLS#2 AUTO FORWARD*)
	TLS2AR :	BOOL;(*TLS#2 AUTO REVERSE*)
	TLS3AF :	BOOL;(*TLS#3 AUTO FORWARD*)
	TLS3AR :	BOOL;(*TLS#3 AUTO REVERSE*)
	TLS4AF :	BOOL;(*TLS#4 AUTO FORWARD*)
	TLS4AR :	BOOL;(*TLS#4 AUTO REVERSE*)
	TLS1MF :	BOOL;(*TLS#1 MANUAL FORWARD*)
	TLS1MR :	BOOL;(*TLS#1 MANUAL REVERSE*)
	TLS2MF :	BOOL;(*TLS#2 MANUAL FORWARD*)
	TLS2MR :	BOOL;(*TLS#2 MANUAL REVERSE*)
	TLS3MF :	BOOL;(*TLS#3 MANUAL FORWARD*)
	TLS3MR :	BOOL;(*TLS#3 MANUAL REVERSE*)
	TLS4MF :	BOOL;(*TLS#4 MANUAL FORWARD*)
	TLS4MR :	BOOL;(*TLS#4 MANUAL REVERSE*)
	TLS1FCMD :	BOOL;(*TLS#1 FORWARD COMMAND*)
	TLS1RCMD :	BOOL;(*TLS#1 REVERSE COMMAND*)
	TLS2FCMD :	BOOL;(*TLS#2 FORWARD COMMAND*)
	TLS2RCMD :	BOOL;(*TLS#2 REVERSE COMMAND*)
	TLS3FCMD :	BOOL;(*TLS#3 FORWARD COMMAND*)
	TLS3RCMD :	BOOL;(*TLS#3 REVERSE COMMAND*)
	TLS4FCMD :	BOOL;(*TLS#4 FORWARD COMMAND*)
	TLS4RCMD :	BOOL;(*TLS#4 REVERSE COMMAND*)
	TLS1CMD :	BOOL;(*TLS#1 COMMAND*)
	TLS2CMD :	BOOL;(*TLS#2 COMMAND*)
	TLS3CMD :	BOOL;(*TLS#3 COMMAND*)
	TLS4CMD :	BOOL;(*TLS#4 COMMAND*)
	TLSBR :	BOOL;(*TLS THRUSTOR BRAKE ANY RELEASED*)
	TLS1_HOME_TRIG :	BOOL;(*TLS#1 POSITION HOME TRIG COMMAND*)
	TLS2_HOME_TRIG :	BOOL;(*TLS#2 POSITION HOME TRIG COMMAND*)
	TLS3_HOME_TRIG :	BOOL;(*TLS#3 POSITION HOME TRIG COMMAND*)
	TLS4_HOME_TRIG :	BOOL;(*TLS#4 POSITION HOME TRIG COMMAND*)
	TLS1POS :	REAL;(*TLS#1 POSITION*)
	TLS2POS :	REAL;(*TLS#2 POSITION*)
	TLS3POS :	REAL;(*TLS#3 POSITION*)
	TLS4POS :	REAL;(*TLS#4 POSITION*)
	TLS1TEXTPOS :	REAL;(*TLS#1 TRIM EXTEND POSITION*)
	TLS1TRETPOS :	REAL;(*TLS#1 TRIM RETRACT POSITION*)
	TLS1LEXTPOS :	REAL;(*TLS#1 LIST EXTEND POSITION*)
	TLS1LRETPOS :	REAL;(*TLS#1 LIST RETRACT POSITION*)
	TLS1SEXTPOS :	REAL;(*TLS#1 SKEW EXTEND POSITION*)
	TLS1SRETPOS :	REAL;(*TLS#1 SKEW RETRACT POSITION*)
	TLS1EXTOTPOS :	REAL;(*TLS#1 EXTEND OVER TRAVEL POSITION*)
	TLS1RETOTPOS :	REAL;(*TLS#1 RETRACT OVER TRAVEL POSITION*)
	TLS2TEXTPOS :	REAL;(*TLS#2 TRIM EXTEND POSITION*)
	TLS2TRETPOS :	REAL;(*TLS#2 TRIM RETRACT POSITION*)
	TLS2LEXTPOS :	REAL;(*TLS#2 LIST EXTEND POSITION*)
	TLS2LRETPOS :	REAL;(*TLS#2 LIST RETRACT POSITION*)
	TLS2SEXTPOS :	REAL;(*TLS#2 SKEW EXTEND POSITION*)
	TLS2SRETPOS :	REAL;(*TLS#2 SKEW RETRACT POSITION*)
	TLS2EXTOTPOS :	REAL;(*TLS#2 EXTEND OVER TRAVEL POSITION*)
	TLS2RETOTPOS :	REAL;(*TLS#2 RETRACT OVER TRAVEL POSITION*)
	TLS3TEXTPOS :	REAL;(*TLS#3 TRIM EXTEND POSITION*)
	TLS3TRETPOS :	REAL;(*TLS#3 TRIM RETRACT POSITION*)
	TLS3LEXTPOS :	REAL;(*TLS#3 LIST EXTEND POSITION*)
	TLS3LRETPOS :	REAL;(*TLS#3 LIST RETRACT POSITION*)
	TLS3SEXTPOS :	REAL;(*TLS#3 SKEW EXTEND POSITION*)
	TLS3SRETPOS :	REAL;(*TLS#3 SKEW RETRACT POSITION*)
	TLS3EXTOTPOS :	REAL;(*TLS#3 EXTEND OVER TRAVEL POSITION*)
	TLS3RETOTPOS :	REAL;(*TLS#3 RETRACT OVER TRAVEL POSITION*)
	TLS4TEXTPOS :	REAL;(*TLS#4 TRIM EXTEND POSITION*)
	TLS4TRETPOS :	REAL;(*TLS#4 TRIM RETRACT POSITION*)
	TLS4LEXTPOS :	REAL;(*TLS#4 LIST EXTEND POSITION*)
	TLS4LRETPOS :	REAL;(*TLS#4 LIST RETRACT POSITION*)
	TLS4SEXTPOS :	REAL;(*TLS#4 SKEW EXTEND POSITION*)
	TLS4SRETPOS :	REAL;(*TLS#4 SKEW RETRACT POSITION*)
	TLS4EXTOTPOS :	REAL;(*TLS#4 EXTEND OVER TRAVEL POSITION*)
	TLS4RETOTPOS :	REAL;(*TLS#4 RETRACT OVER TRAVEL POSITION*)
	TLS1FTARPOS :	REAL;(*TLS#1 FORWARD TARGET POSITION*)
	TLS2FTARPOS :	REAL;(*TLS#2 FORWARD TARGET POSITION*)
	TLS3FTARPOS :	REAL;(*TLS#3 FORWARD TARGET POSITION*)
	TLS4FTARPOS :	REAL;(*TLS#4 FORWARD TARGET POSITION*)
	TLS1RTARPOS :	REAL;(*TLS#1 REVERSE TARGET POSITION*)
	TLS2RTARPOS :	REAL;(*TLS#2 REVERSE TARGET POSITION*)
	TLS3RTARPOS :	REAL;(*TLS#3 REVERSE TARGET POSITION*)
	TLS4RTARPOS :	REAL;(*TLS#4 REVERSE TARGET POSITION*)
	TLS1ATHM :	BOOL;(*TLS#1 AT HOME POSITION*)
	TLS2ATHM :	BOOL;(*TLS#2 AT HOME POSITION*)
	TLS3ATHM :	BOOL;(*TLS#3 AT HOME POSITION*)
	TLS4ATHM :	BOOL;(*TLS#4 AT HOME POSITION*)
	TLS1TEXTST :	BOOL;(*TLS#1 TRIM EXTEND STOP BY POSITION*)
	TLS1TRETST :	BOOL;(*TLS#1 TRIM RETRACT STOP BY POSITION*)
	TLS1LEXTST :	BOOL;(*TLS#1 LIST EXTEND STOP BY POSITION*)
	TLS1LRETST :	BOOL;(*TLS#1 LIST RETRACT STOP BY POSITION*)
	TLS1SEXTST :	BOOL;(*TLS#1 SKEW EXTEND STOP BY POSITION*)
	TLS1SRETST :	BOOL;(*TLS#1 SKEW RETRACT STOP BY POSITION*)
	TLS1EOTST :	BOOL;(*TLS#1 EXTEND OVER TRAVEL STOP BY POSITION*)
	TLS1ROTST :	BOOL;(*TLS#1 RETRACT OVER TRAVEL STOP BY POSITION*)
	TLS2TEXTST :	BOOL;(*TLS#2 TRIM EXTEND STOP BY POSITION*)
	TLS2TRETST :	BOOL;(*TLS#2 TRIM RETRACT STOP BY POSITION*)
	TLS2LEXTST :	BOOL;(*TLS#2 LIST EXTEND STOP BY POSITION*)
	TLS2LRETST :	BOOL;(*TLS#2 LIST RETRACT STOP BY POSITION*)
	TLS2SEXTST :	BOOL;(*TLS#2 SKEW EXTEND STOP BY POSITION*)
	TLS2SRETST :	BOOL;(*TLS#2 SKEW RETRACT STOP BY POSITION*)
	TLS2EOTST :	BOOL;(*TLS#2 EXTEND OVER TRAVEL STOP BY POSITION*)
	TLS2ROTST :	BOOL;(*TLS#2 RETRACT OVER TRAVEL STOP BY POSITION*)
	TLS3TEXTST :	BOOL;(*TLS#3 TRIM EXTEND STOP BY POSITION*)
	TLS3TRETST :	BOOL;(*TLS#3 TRIM RETRACT STOP BY POSITION*)
	TLS3LEXTST :	BOOL;(*TLS#3 LIST EXTEND STOP BY POSITION*)
	TLS3LRETST :	BOOL;(*TLS#3 LIST RETRACT STOP BY POSITION*)
	TLS3SEXTST :	BOOL;(*TLS#3 SKEW EXTEND STOP BY POSITION*)
	TLS3SRETST :	BOOL;(*TLS#3 SKEW RETRACT STOP BY POSITION*)
	TLS3EOTST :	BOOL;(*TLS#3 EXTEND OVER TRAVEL STOP BY POSITION*)
	TLS3ROTST :	BOOL;(*TLS#3 RETRACT OVER TRAVEL STOP BY POSITION*)
	TLS4TEXTST :	BOOL;(*TLS#4 TRIM EXTEND STOP BY POSITION*)
	TLS4TRETST :	BOOL;(*TLS#4 TRIM RETRACT STOP BY POSITION*)
	TLS4LEXTST :	BOOL;(*TLS#4 LIST EXTEND STOP BY POSITION*)
	TLS4LRETST :	BOOL;(*TLS#4 LIST RETRACT STOP BY POSITION*)
	TLS4SEXTST :	BOOL;(*TLS#4 SKEW EXTEND STOP BY POSITION*)
	TLS4SRETST :	BOOL;(*TLS#4 SKEW RETRACT STOP BY POSITION*)
	TLS4EOTST :	BOOL;(*TLS#4 EXTEND OVER TRAVEL STOP BY POSITION*)
	TLS4ROTST :	BOOL;(*TLS#4 RETRACT OVER TRAVEL STOP BY POSITION*)
	TLS1EXTST :	BOOL;(*TLS#1 EXTEND STOP BY POSITION*)
	TLS1RETST :	BOOL;(*TLS#1 RETRACT STOP BY POSITION*)
	TLS2EXTST :	BOOL;(*TLS#2 EXTEND STOP BY POSITION*)
	TLS2RETST :	BOOL;(*TLS#2 RETRACT STOP BY POSITION*)
	TLS3EXTST :	BOOL;(*TLS#3 EXTEND STOP BY POSITION*)
	TLS3RETST :	BOOL;(*TLS#3 RETRACT STOP BY POSITION*)
	TLS4EXTST :	BOOL;(*TLS#4 EXTEND STOP BY POSITION*)
	TLS4RETST :	BOOL;(*TLS#4 RETRACT STOP BY POSITION*)
	TRMANG :	REAL;(*TRIM ANGLE*)
	LSTANG :	REAL;(*LIST ANGLE*)
	SKWANG :	REAL;(*SKEW ANGLE*)
	TLS1SPDREF :	INT;(*TLS#1 SPEED REFERENCE*)
	TLS2SPDREF :	INT;(*TLS#2 SPEED REFERENCE*)
	TLS3SPDREF :	INT;(*TLS#3 SPEED REFERENCE*)
	TLS4SPDREF :	INT;(*TLS#4 SPEED REFERENCE*)
	TLS1INVREF :	INT;(*TLS#1 INVERTER REFERENCE*)
	TLS2INVREF :	INT;(*TLS#2 INVERTER REFERENCE*)
	TLS3INVREF :	INT;(*TLS#3 INVERTER REFERENCE*)
	TLS4INVREF :	INT;(*TLS#4 INVERTER REFERENCE*)
	TLS1_RUN_FWD :	BOOL;(*TLS INVERTER#1 RUN FORWARD*)
	TLS1_RUN_REV :	BOOL;(*TLS INVERTER#1 RUN REVERSE*)
	TLS1_IN_TRIP :	BOOL;(*TLS INVERTER#1 SHUT OFF*)
	TLS1INVFLT :	BOOL;(*TLS INVERTER#1 FAULT*)
	TLS1RUNX :	BOOL;(*TLS INVERTER#1 DC VOLTAGE ESTABLISHED*)
	TLS1SFB :	INT;(*TLS INVERTER#1 SPEED FEEDBACK*)
	TLS1FRQ :	INT;(*TLS INVERTER#1 FREQUENCY*)
	TLS1VOLT :	INT;(*TLS INVERTER#1 VOLTAGE*)
	TLS1AMP :	INT;(*TLS INVERTER#1 CURRENT*)
	TLS1FLTCODE :	INT;(*TLS INVERTER#1 FAULT CODE*)
	TLS1_POLL_DATA1 :	WORD;(*TLS INVERTER#1 POLLING FUNCTION CODE 1 DATA*)
	TLS1_BREL :	BOOL;(*TLS#1 BRAKE RELEASE SIGNAL FROM INV*)
	TLS2_RUN_FWD :	BOOL;(*TLS INVERTER#2 RUN FORWARD*)
	TLS2_RUN_REV :	BOOL;(*TLS INVERTER#2 RUN REVERSE*)
	TLS2_IN_TRIP :	BOOL;(*TLS INVERTER#2 SHUT OFF*)
	TLS2INVFLT :	BOOL;(*TLS INVERTER#2 FAULT*)
	TLS2RUNX :	BOOL;(*TLS INVERTER#2 DC VOLTAGE ESTABLISHED*)
	TLS2SFB :	INT;(*TLS INVERTER#2 SPEED FEEDBACK*)
	TLS2FRQ :	INT;(*TLS INVERTER#2 FREQUENCY*)
	TLS2VOLT :	INT;(*TLS INVERTER#2 VOLTAGE*)
	TLS2AMP :	INT;(*TLS INVERTER#2 CURRENT*)
	TLS2FLTCODE :	INT;(*TLS INVERTER#2 FAULT CODE*)
	TLS2_POLL_DATA1 :	WORD;(*TLS INVERTER#2 POLLING FUNCTION CODE 1 DATA*)
	TLS2_BREL :	BOOL;(*TLS#2 BRAKE RELEASE SIGNAL FROM INV*)
	TLS3_RUN_FWD :	BOOL;(*TLS INVERTER#3 RUN FORWARD*)
	TLS3_RUN_REV :	BOOL;(*TLS INVERTER#3 RUN REVERSE*)
	TLS3_IN_TRIP :	BOOL;(*TLS INVERTER#3 SHUT OFF*)
	TLS3INVFLT :	BOOL;(*TLS INVERTER#3 FAULT*)
	TLS3RUNX :	BOOL;(*TLS INVERTER#3 DC VOLTAGE ESTABLISHED*)
	TLS3SFB :	INT;(*TLS INVERTER#3 SPEED FEEDBACK*)
	TLS3FRQ :	INT;(*TLS INVERTER#3 FREQUENCY*)
	TLS3VOLT :	INT;(*TLS INVERTER#3 VOLTAGE*)
	TLS3AMP :	INT;(*TLS INVERTER#3 CURRENT*)
	TLS3FLTCODE :	INT;(*TLS INVERTER#3 FAULT CODE*)
	TLS3_POLL_DATA1 :	WORD;(*TLS INVERTER#3 POLLING FUNCTION CODE 1 DATA*)
	TLS3_BREL :	BOOL;(*TLS#3 BRAKE RELEASE SIGNAL FROM INV*)
	TLS4_RUN_FWD :	BOOL;(*TLS INVERTER#4 RUN FORWARD*)
	TLS4_RUN_REV :	BOOL;(*TLS INVERTER#4 RUN REVERSE*)
	TLS4_IN_TRIP :	BOOL;(*TLS INVERTER#4 SHUT OFF*)
	TLS4INVFLT :	BOOL;(*TLS INVERTER#4 FAULT*)
	TLS4RUNX :	BOOL;(*TLS INVERTER#4 DC VOLTAGE ESTABLISHED*)
	TLS4SFB :	INT;(*TLS INVERTER#4 SPEED FEEDBACK*)
	TLS4FRQ :	INT;(*TLS INVERTER#4 FREQUENCY*)
	TLS4VOLT :	INT;(*TLS INVERTER#4 VOLTAGE*)
	TLS4AMP :	INT;(*TLS INVERTER#4 CURRENT*)
	TLS4FLTCODE :	INT;(*TLS INVERTER#4 FAULT CODE*)
	TLS4_POLL_DATA1 :	WORD;(*TLS INVERTER#4 POLLING FUNCTION CODE 1 DATA*)
	TLS4_BREL :	BOOL;(*TLS#4 BRAKE RELEASE SIGNAL FROM INV*)
END_VAR


VAR_GLOBAL RETAIN 
	TLS1POSVLD	AT %MX3.0.3 :	BOOL;(*TLS1 POSITION VALID*)
	TLS2POSVLD	AT %MX3.0.4 :	BOOL;(*TLS2 POSITION VALID*)
	TLS3POSVLD	AT %MX3.0.5 :	BOOL;(*TLS3 POSITION VALID*)
	TLS4POSVLD	AT %MX3.0.6 :	BOOL;(*TLS4 POSITION VALID*)
END_VAR

