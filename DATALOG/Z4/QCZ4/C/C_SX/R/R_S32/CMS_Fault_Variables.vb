
(*Group:CMS_Fault_Variables*)


VAR_GLOBAL
	FLT001	AT %MX1.4000.0 :	BOOL;(*Emergency Stop:PLC/CMS panel*)
	FLT002	AT %MX1.4000.1 :	BOOL;(*Emergency Stop:E_room*)
	FLT003	AT %MX1.4000.2 :	BOOL;(*Emergency Stop:M_house#1*)
	FLT004	AT %MX1.4000.3 :	BOOL;(*Emergency Stop:M_house#2*)
	FLT005	AT %MX1.4000.4 :	BOOL;(*Emergency Stop:M_house#3*)
	FLT006	AT %MX1.4000.5 :	BOOL;(*Emergency Stop:M_house#4*)
	FLT007	AT %MX1.4000.6 :	BOOL;(*Emergency Stop:M_room control station*)
	FLT008	AT %MX1.4000.7 :	BOOL;(*Emergency Stop:Cab panel*)
	FLT009	AT %MX1.4000.8 :	BOOL;(*Emergency Stop:RC console*)
	FLT010	AT %MX1.4000.9 :	BOOL;(*Emergency Stop:TLS control station*)
	FLT011	AT %MX1.4000.10 :	BOOL;(*Emergency Stop:Trolley emergency station*)
	FLT012	AT %MX1.4000.11 :	BOOL;(*Emergency Stop:Boom control station*)
	FLT013	AT %MX1.4000.12 :	BOOL;(*Emergency Stop:Boom tip*)
	FLT014	AT %MX1.4000.13 :	BOOL;(*Emergency Stop:Gantry landside#1*)
	FLT015	AT %MX1.4000.14 :	BOOL;(*Emergency Stop:Gantry landside#2*)
	FLT016	AT %MX1.4000.15 :	BOOL;(*Emergency Stop:Gantry landside#3*)
	FLT017	AT %MX1.4001.0 :	BOOL;(*Emergency Stop:Gantry waterside#1*)
	FLT018	AT %MX1.4001.1 :	BOOL;(*Emergency Stop:Gantry waterside#2*)
	FLT019	AT %MX1.4001.2 :	BOOL;(*Emergency Stop:Gantry waterside#3*)
	FLT020	AT %MX1.4001.3 :	BOOL;(*Emergency Stop:Gantry control station*)
	FLT021	AT %MX1.4001.4 :	BOOL;
	FLT022	AT %MX1.4001.5 :	BOOL;
	FLT023	AT %MX1.4001.6 :	BOOL;
	FLT024	AT %MX1.4001.7 :	BOOL;
	FLT025	AT %MX1.4001.8 :	BOOL;
	FLT026	AT %MX1.4001.9 :	BOOL;(*Auxiliary power voltage fault*)
	FLT027	AT %MX1.4001.10 :	BOOL;(*Main transformer over temp. alarm*)
	FLT028	AT %MX1.4001.11 :	BOOL;(*Main transformer over temp. trip*)
	FLT029	AT %MX1.4001.12 :	BOOL;(*Auxiliary transformer over temp. alarm*)
	FLT030	AT %MX1.4001.13 :	BOOL;(*Auxiliary transformer over temp. trip*)
	FLT031	AT %MX1.4001.14 :	BOOL;(*RHC#1 filter panel high temp. fault*)
	FLT032	AT %MX1.4001.15 :	BOOL;(*RHC#2 filter panel high temp. fault*)
	FLT033	AT %MX1.4002.0 :	BOOL;(*E_room high temperature fault*)
	FLT034	AT %MX1.4002.1 :	BOOL;(*M_house place#1 high temp. fault*)
	FLT035	AT %MX1.4002.2 :	BOOL;(*M_house place#2 high temp. fault*)
	FLT036	AT %MX1.4002.3 :	BOOL;(*M_house place#3 high temp. fault*)
	FLT037	AT %MX1.4002.4 :	BOOL;(*M_house place#4 high temp. fault*)
	FLT038	AT %MX1.4002.5 :	BOOL;(*M_house place#5 high temp. fault*)
	FLT039	AT %MX1.4002.6 :	BOOL;(*M_house place#6 high temp. fault*)
	FLT040	AT %MX1.4002.7 :	BOOL;(*M_house place#7 high temp. fault*)
	FLT041	AT %MX1.4002.8 :	BOOL;(*M_house place#8 high temp. fault*)
	FLT042	AT %MX1.4002.9 :	BOOL;(*E_room smoke#1 fault*)
	FLT043	AT %MX1.4002.10 :	BOOL;(*E_room smoke#2 fault*)
	FLT044	AT %MX1.4002.11 :	BOOL;(*E_room smoke#3 fault*)
	FLT045	AT %MX1.4002.12 :	BOOL;(*E_house air-conditioning#1 fault*)
	FLT046	AT %MX1.4002.13 :	BOOL;(*E_house air-conditioning#2 fault*)
	FLT047	AT %MX1.4002.14 :	BOOL;(*E_house air-conditioning#3 fault*)
	FLT048	AT %MX1.4002.15 :	BOOL;(*E_house air-conditioning#4 fault*)
	FLT049	AT %MX1.4003.0 :	BOOL;(*E_house air-conditioning#5 fault*)
	FLT050	AT %MX1.4003.1 :	BOOL;(*High wind alarm*)
	FLT051	AT %MX1.4003.2 :	BOOL;(*High wind trip*)
	FLT052	AT %MX1.4003.3 :	BOOL;(*Wind speed sensor wire break fault*)
	FLT053	AT %MX1.4003.4 :	BOOL;(*Loadcell#1 wire break fault*)
	FLT054	AT %MX1.4003.5 :	BOOL;(*Loadcell#2 wire break fault*)
	FLT055	AT %MX1.4003.6 :	BOOL;(*Loadcell#3 wire break fault*)
	FLT056	AT %MX1.4003.7 :	BOOL;(*Loadcell#4 wire break fault*)
	FLT057	AT %MX1.4003.8 :	BOOL;(*Over load fault*)
	FLT058	AT %MX1.4003.9 :	BOOL;(*Eccentric load fault*)
	FLT059	AT %MX1.4003.10 :	BOOL;(*Over load alarm*)
	FLT060	AT %MX1.4003.11 :	BOOL;(*Snag trip by loadcell*)
	FLT061	AT %MX1.4003.12 :	BOOL;(*GCR run time check fault*)
	FLT062	AT %MX1.4003.13 :	BOOL;(*GCR drum empty fault*)
	FLT063	AT %MX1.4003.14 :	BOOL;(*GCR reel out check fault*)
	FLT064	AT %MX1.4003.15 :	BOOL;(*GCR slack fault*)
	FLT065	AT %MX1.4004.0 :	BOOL;(*GCR overtension fault*)
	FLT066	AT %MX1.4004.1 :	BOOL;
	FLT067	AT %MX1.4004.2 :	BOOL;
	FLT068	AT %MX1.4004.3 :	BOOL;
	FLT069	AT %MX1.4004.4 :	BOOL;
	FLT070	AT %MX1.4004.5 :	BOOL;
	FLT071	AT %MX1.4004.6 :	BOOL;
	FLT072	AT %MX1.4004.7 :	BOOL;
	FLT073	AT %MX1.4004.8 :	BOOL;
	FLT074	AT %MX1.4004.9 :	BOOL;
	FLT075	AT %MX1.4004.10 :	BOOL;
	FLT076	AT %MX1.4004.11 :	BOOL;(*Wheel clamp LS#1 release fault*)
	FLT077	AT %MX1.4004.12 :	BOOL;(*Wheel clamp LS#2 release fault*)
	FLT078	AT %MX1.4004.13 :	BOOL;(*Wheel clamp LS#3 release fault*)
	FLT079	AT %MX1.4004.14 :	BOOL;(*Wheel clamp LS#4 release fault*)
	FLT080	AT %MX1.4004.15 :	BOOL;(*Wheel clamp LS#5 release fault*)
	FLT081	AT %MX1.4005.0 :	BOOL;(*Wheel clamp LS#6 release fault*)
	FLT082	AT %MX1.4005.1 :	BOOL;(*Wheel clamp LS#7 release fault*)
	FLT083	AT %MX1.4005.2 :	BOOL;(*Wheel clamp LS#8 release fault*)
	FLT084	AT %MX1.4005.3 :	BOOL;(*Wheel clamp LS#9 release fault*)
	FLT085	AT %MX1.4005.4 :	BOOL;(*Wheel clamp LS#10 release fault*)
	FLT086	AT %MX1.4005.5 :	BOOL;(*Wheel clamp LS#11 release fault*)
	FLT087	AT %MX1.4005.6 :	BOOL;(*Wheel clamp LS#12 release fault*)
	FLT088	AT %MX1.4005.7 :	BOOL;(*Wheel clamp LS#13 release fault*)
	FLT089	AT %MX1.4005.8 :	BOOL;(*Wheel clamp LS#14 release fault*)
	FLT090	AT %MX1.4005.9 :	BOOL;(*Wheel clamp LS#15 release fault*)
	FLT091	AT %MX1.4005.10 :	BOOL;(*Wheel clamp LS#16 release fault*)
	FLT092	AT %MX1.4005.11 :	BOOL;(*Waterside wheel clamp pump contactor check fault*)
	FLT093	AT %MX1.4005.12 :	BOOL;(*Waterside wheel clamp pump run time check fault*)
	FLT094	AT %MX1.4005.13 :	BOOL;(*Waterside wheel clamp hyd. station over temp. fault*)
	FLT095	AT %MX1.4005.14 :	BOOL;(*Waterside wheel clamp hyd. station high pressure fault*)
	FLT096	AT %MX1.4005.15 :	BOOL;(*Waterside wheel clamp hyd. station oil low level fault*)
	FLT097	AT %MX1.4006.0 :	BOOL;(*Landside wheel clamp pump contactor check fault*)
	FLT098	AT %MX1.4006.1 :	BOOL;(*Landside wheel clamp pump run time check fault*)
	FLT099	AT %MX1.4006.2 :	BOOL;(*Landside wheel clamp hyd. station over temp. fault*)
	FLT100	AT %MX1.4006.3 :	BOOL;(*Landside wheel clamp hyd. station high pressure fault*)
	FLT101	AT %MX1.4006.4 :	BOOL;(*Landside wheel clamp hyd. station oil low level fault*)
	FLT102	AT %MX1.4006.5 :	BOOL;
	FLT103	AT %MX1.4006.6 :	BOOL;
	FLT104	AT %MX1.4006.7 :	BOOL;(*Backreach hyd. pump contactor check fault*)
	FLT105	AT %MX1.4006.8 :	BOOL;(*Backreach hyd. pump heater contactor check fault*)
	FLT106	AT %MX1.4006.9 :	BOOL;(*Backreach hyd. station high temp. fault*)
	FLT107	AT %MX1.4006.10 :	BOOL;(*Backreach hyd. pump motor over temp. fault*)
	FLT108	AT %MX1.4006.11 :	BOOL;(*Backreach hyd. station oil low level fault*)
	FLT109	AT %MX1.4006.12 :	BOOL;(*Backreach hyd. station butterfly valve close fault*)
	FLT110	AT %MX1.4006.13 :	BOOL;(*Catenary rope tensioner low pressure fault*)
	FLT111	AT %MX1.4006.14 :	BOOL;(*Catenary rope tensioner high pressure fault*)
	FLT112	AT %MX1.4006.15 :	BOOL;(*Catenary rope tensioner overtension#1 fault*)
	FLT113	AT %MX1.4007.0 :	BOOL;(*Catenary rope tensioner overtension#2 fault*)
	FLT114	AT %MX1.4007.1 :	BOOL;(*Catenary rope tensioner slack#1 fault*)
	FLT115	AT %MX1.4007.2 :	BOOL;(*Catenary rope tensioner slack#2 fault*)
	FLT116	AT %MX1.4007.3 :	BOOL;(*Backreach hyd. station solenoid 3 run time check fault*)
	FLT117	AT %MX1.4007.4 :	BOOL;
	FLT118	AT %MX1.4007.5 :	BOOL;
	FLT119	AT %MX1.4007.6 :	BOOL;(*Hoist hyd. brake#1 LS release fault*)
	FLT120	AT %MX1.4007.7 :	BOOL;(*Hoist hyd. brake#2 LS release fault*)
	FLT121	AT %MX1.4007.8 :	BOOL;(*Hoist hyd. brake#3 LS release fault*)
	FLT122	AT %MX1.4007.9 :	BOOL;(*Hoist hyd. brake#4 LS release fault*)
	FLT123	AT %MX1.4007.10 :	BOOL;(*Boom hyd. brake#1 LS release fault*)
	FLT124	AT %MX1.4007.11 :	BOOL;(*Boom hyd. brake#2 LS release fault*)
	FLT125	AT %MX1.4007.12 :	BOOL;(*Boom hyd. brake#3 LS release fault*)
	FLT126	AT %MX1.4007.13 :	BOOL;(*Boom hyd. brake#4 LS release fault*)
	FLT127	AT %MX1.4007.14 :	BOOL;(*Emergency brake hyd. pump contactor check fault*)
	FLT128	AT %MX1.4007.15 :	BOOL;(*Emergency brake hyd. pump run time check fault*)
	FLT129	AT %MX1.4008.0 :	BOOL;(*Emergency brake hyd. station oil low level alarm*)
	FLT130	AT %MX1.4008.1 :	BOOL;(*Emergency brake hyd. station oil low level fault*)
	FLT131	AT %MX1.4008.2 :	BOOL;(*Emergency brake hyd. station over temp. alarm*)
	FLT132	AT %MX1.4008.3 :	BOOL;(*Emergency brake hyd. station over temp. fault*)
	FLT133	AT %MX1.4008.4 :	BOOL;
	FLT134	AT %MX1.4008.5 :	BOOL;
	FLT135	AT %MX1.4008.6 :	BOOL;(*Snag load fault*)
	FLT136	AT %MX1.4008.7 :	BOOL;(*Snag cylinder#1 snag load fault*)
	FLT137	AT %MX1.4008.8 :	BOOL;(*Snag cylinder#2 snag load fault*)
	FLT138	AT %MX1.4008.9 :	BOOL;(*Snag cylinder#3 snag load fault*)
	FLT139	AT %MX1.4008.10 :	BOOL;(*Snag cylinder#4 snag load fault*)
	FLT140	AT %MX1.4008.11 :	BOOL;
	FLT141	AT %MX1.4008.12 :	BOOL;
	FLT142	AT %MX1.4008.13 :	BOOL;(*Spreader pump contactor check fault*)
	FLT143	AT %MX1.4008.14 :	BOOL;(*Spreader TTDS fault*)
	FLT144	AT %MX1.4008.15 :	BOOL;(*Spreader lock overtime check fault*)
	FLT145	AT %MX1.4009.0 :	BOOL;(*Spreader unlock overtime check fault*)
	FLT146	AT %MX1.4009.1 :	BOOL;(*Spreader extend overtime check fault*)
	FLT147	AT %MX1.4009.2 :	BOOL;(*Spreader retract overtime check fault*)
	FLT148	AT %MX1.4009.3 :	BOOL;(*Spreader twin down overtime check fault*)
	FLT149	AT %MX1.4009.4 :	BOOL;
	FLT150	AT %MX1.4009.5 :	BOOL;
	FLT151	AT %MX1.4009.6 :	BOOL;
	FLT152	AT %MX1.4009.7 :	BOOL;
	FLT153	AT %MX1.4009.8 :	BOOL;
	FLT154	AT %MX1.4009.9 :	BOOL;
	FLT155	AT %MX1.4009.10 :	BOOL;
	FLT156	AT %MX1.4009.11 :	BOOL;
	FLT157	AT %MX1.4009.12 :	BOOL;
	FLT158	AT %MX1.4009.13 :	BOOL;
	FLT159	AT %MX1.4009.14 :	BOOL;
	FLT160	AT %MX1.4009.15 :	BOOL;
	FLT161	AT %MX1.4010.0 :	BOOL;
	FLT162	AT %MX1.4010.1 :	BOOL;
	FLT163	AT %MX1.4010.2 :	BOOL;
	FLT164	AT %MX1.4010.3 :	BOOL;
	FLT165	AT %MX1.4010.4 :	BOOL;
	FLT166	AT %MX1.4010.5 :	BOOL;
	FLT167	AT %MX1.4010.6 :	BOOL;
	FLT168	AT %MX1.4010.7 :	BOOL;
	FLT169	AT %MX1.4010.8 :	BOOL;
	FLT170	AT %MX1.4010.9 :	BOOL;
	FLT171	AT %MX1.4010.10 :	BOOL;
	FLT172	AT %MX1.4010.11 :	BOOL;
	FLT173	AT %MX1.4010.12 :	BOOL;
	FLT174	AT %MX1.4010.13 :	BOOL;
	FLT175	AT %MX1.4010.14 :	BOOL;
	FLT176	AT %MX1.4010.15 :	BOOL;
	FLT177	AT %MX1.4011.0 :	BOOL;
	FLT178	AT %MX1.4011.1 :	BOOL;
	FLT179	AT %MX1.4011.2 :	BOOL;
	FLT180	AT %MX1.4011.3 :	BOOL;
	FLT181	AT %MX1.4011.4 :	BOOL;
	FLT182	AT %MX1.4011.5 :	BOOL;
	FLT183	AT %MX1.4011.6 :	BOOL;
	FLT184	AT %MX1.4011.7 :	BOOL;
	FLT185	AT %MX1.4011.8 :	BOOL;
	FLT186	AT %MX1.4011.9 :	BOOL;
	FLT187	AT %MX1.4011.10 :	BOOL;
	FLT188	AT %MX1.4011.11 :	BOOL;
	FLT189	AT %MX1.4011.12 :	BOOL;
	FLT190	AT %MX1.4011.13 :	BOOL;
	FLT191	AT %MX1.4011.14 :	BOOL;
	FLT192	AT %MX1.4011.15 :	BOOL;
	FLT193	AT %MX1.4012.0 :	BOOL;
	FLT194	AT %MX1.4012.1 :	BOOL;
	FLT195	AT %MX1.4012.2 :	BOOL;
	FLT196	AT %MX1.4012.3 :	BOOL;
	FLT197	AT %MX1.4012.4 :	BOOL;
	FLT198	AT %MX1.4012.5 :	BOOL;
	FLT199	AT %MX1.4012.6 :	BOOL;
	FLT200	AT %MX1.4012.7 :	BOOL;
	FLT201	AT %MX1.4012.8 :	BOOL;(*Hoist inv#1 sx communication error*)
	FLT202	AT %MX1.4012.9 :	BOOL;(*Hoist inv#2 sx communication error*)
	FLT203	AT %MX1.4012.10 :	BOOL;(*Hoist overspeed fault*)
	FLT204	AT %MX1.4012.11 :	BOOL;(*Hoist speed setting and feedback disagree fault*)
	FLT205	AT %MX1.4012.12 :	BOOL;(*Hoist inv#1 and inv#2 current not balance fault*)
	FLT206	AT %MX1.4012.13 :	BOOL;(*Hoist inv#1 and inv#2 speed not balance fault*)
	FLT207	AT %MX1.4012.14 :	BOOL;(*Hoist up over travel fault*)
	FLT208	AT %MX1.4012.15 :	BOOL;(*Hoist emergency stop relay check fault*)
	FLT209	AT %MX1.4013.0 :	BOOL;(*Hoist emergency stop contactor check fault*)
	FLT210	AT %MX1.4013.1 :	BOOL;(*Hoist brake contactor check fault*)
	FLT211	AT %MX1.4013.2 :	BOOL;(*Hoist brake#1 LS release fault*)
	FLT212	AT %MX1.4013.3 :	BOOL;(*Hoist brake#2 LS release fault*)
	FLT213	AT %MX1.4013.4 :	BOOL;(*Hoist run time check fault*)
	FLT214	AT %MX1.4013.5 :	BOOL;(*Hoist command lost while brake open fault*)
	FLT215	AT %MX1.4013.6 :	BOOL;(*Hoist brake#1 manual release LS open fault*)
	FLT216	AT %MX1.4013.7 :	BOOL;(*Hoist brake#2 manual release LS open fault*)
	FLT217	AT %MX1.4013.8 :	BOOL;(*Hoist brake#1 wear fault*)
	FLT218	AT %MX1.4013.9 :	BOOL;(*Hoist brake#2 wear fault*)
	FLT219	AT %MX1.4013.10 :	BOOL;(*Hoist drum#1 rope overlap fault*)
	FLT220	AT %MX1.4013.11 :	BOOL;(*Hoist drum#2 rope overlap fault*)
	FLT221	AT %MX1.4013.12 :	BOOL;(*Hoist masterswtich encoder fault*)
	FLT222	AT %MX1.4013.13 :	BOOL;(*Hoist position compare fault*)
	FLT223	AT %MX1.4013.14 :	BOOL;(*Hoist position not valid*)
	FLT224	AT %MX1.4013.15 :	BOOL;(*Hoist up slowdown LS position check fault*)
	FLT225	AT %MX1.4014.0 :	BOOL;(*Hoist up slowdown LS not active check fault*)
	FLT226	AT %MX1.4014.1 :	BOOL;(*Hoist up slowdown LS speed check fault*)
	FLT227	AT %MX1.4014.2 :	BOOL;(*Hoist down slowdown LS position check fault(Landside)*)
	FLT228	AT %MX1.4014.3 :	BOOL;(*Hoist down slowdown LS not active check fault(Landside)*)
	FLT229	AT %MX1.4014.4 :	BOOL;(*Hoist down slowdown LS speed check fault(Landside)*)
	FLT230	AT %MX1.4014.5 :	BOOL;(*Hoist motor#1 high temp. alarm*)
	FLT231	AT %MX1.4014.6 :	BOOL;(*Hoist motor#1 high temp. trip*)
	FLT232	AT %MX1.4014.7 :	BOOL;(*Hoist motor#2 high temp. alarm*)
	FLT233	AT %MX1.4014.8 :	BOOL;(*Hoist motor#2 high temp. trip*)
	FLT234	AT %MX1.4014.9 :	BOOL;(*Hoist motor#1 fan contactor check fault*)
	FLT235	AT %MX1.4014.10 :	BOOL;(*Hoist motor#2 fan contactor check fault*)
	FLT236	AT %MX1.4014.11 :	BOOL;(*Hoist motor heater contactor check fault*)
	FLT237	AT %MX1.4014.12 :	BOOL;(*Hoist ovet torque check fault*)
	FLT238	AT %MX1.4014.13 :	BOOL;(*Snag cylinder end stop check fault*)
	FLT239	AT %MX1.4014.14 :	BOOL;
	FLT240	AT %MX1.4014.15 :	BOOL;
	FLT241	AT %MX1.4015.0 :	BOOL;
	FLT242	AT %MX1.4015.1 :	BOOL;
	FLT243	AT %MX1.4015.2 :	BOOL;
	FLT244	AT %MX1.4015.3 :	BOOL;
	FLT245	AT %MX1.4015.4 :	BOOL;
	FLT246	AT %MX1.4015.5 :	BOOL;
	FLT247	AT %MX1.4015.6 :	BOOL;
	FLT248	AT %MX1.4015.7 :	BOOL;
	FLT249	AT %MX1.4015.8 :	BOOL;
	FLT250	AT %MX1.4015.9 :	BOOL;
	FLT251	AT %MX1.4015.10 :	BOOL;(*Trolley/boom inv sx communication error*)
	FLT252	AT %MX1.4015.11 :	BOOL;(*Trolley forward over travel fault*)
	FLT253	AT %MX1.4015.12 :	BOOL;(*Trolley reverse over travel fault*)
	FLT254	AT %MX1.4015.13 :	BOOL;(*Trolley changeover motor main contactor check fault*)
	FLT255	AT %MX1.4015.14 :	BOOL;(*Trolley changeover encoder relay check fault*)
	FLT256	AT %MX1.4015.15 :	BOOL;(*Trolley emergency stop relay check fault*)
	FLT257	AT %MX1.4016.0 :	BOOL;(*Trolley emergency stop contactor check fault*)
	FLT258	AT %MX1.4016.1 :	BOOL;(*Trolley brake contactor check fault*)
	FLT259	AT %MX1.4016.2 :	BOOL;(*Trolley brake#1 LS release fault*)
	FLT260	AT %MX1.4016.3 :	BOOL;(*Trolley brake#2 LS release fault*)
	FLT261	AT %MX1.4016.4 :	BOOL;(*Trolley brake#3 LS release fault*)
	FLT262	AT %MX1.4016.5 :	BOOL;(*Trolley brake#4 LS release fault*)
	FLT263	AT %MX1.4016.6 :	BOOL;(*Trolley run time check fault*)
	FLT264	AT %MX1.4016.7 :	BOOL;(*Trolley command lost while brake open fault*)
	FLT265	AT %MX1.4016.8 :	BOOL;(*Trolley brake#1 manual release LS open fault*)
	FLT266	AT %MX1.4016.9 :	BOOL;(*Trolley brake#2 manual release LS open fault*)
	FLT267	AT %MX1.4016.10 :	BOOL;(*Trolley brake#3 manual release LS open fault*)
	FLT268	AT %MX1.4016.11 :	BOOL;(*Trolley brake#4 manual release LS open fault*)
	FLT269	AT %MX1.4016.12 :	BOOL;(*Trolley brake#1 wear fault*)
	FLT270	AT %MX1.4016.13 :	BOOL;(*Trolley brake#2 wear fault*)
	FLT271	AT %MX1.4016.14 :	BOOL;(*Trolley brake#3 wear fault*)
	FLT272	AT %MX1.4016.15 :	BOOL;(*Trolley brake#4 wear fault*)
	FLT273	AT %MX1.4017.0 :	BOOL;(*Trolley masterswitch encoder fault*)
	FLT274	AT %MX1.4017.1 :	BOOL;(*Trolley position not valid*)
	FLT275	AT %MX1.4017.2 :	BOOL;(*Trolley forward slowdown LS position check fault*)
	FLT276	AT %MX1.4017.3 :	BOOL;(*Trolley forward slowdown LS not active check fault*)
	FLT277	AT %MX1.4017.4 :	BOOL;(*Trolley forward slowdown LS speed check fault*)
	FLT278	AT %MX1.4017.5 :	BOOL;(*Trolley reverse slowdown LS position check fault*)
	FLT279	AT %MX1.4017.6 :	BOOL;(*Trolley reverse slowdown LS not active check fault*)
	FLT280	AT %MX1.4017.7 :	BOOL;(*Trolley reverse slowdown LS speed check fault*)
	FLT281	AT %MX1.4017.8 :	BOOL;(*Trolley motor#1 high temp. alarm*)
	FLT282	AT %MX1.4017.9 :	BOOL;(*Trolley motor#1 high temp. trip*)
	FLT283	AT %MX1.4017.10 :	BOOL;(*Trolley motor#2 high temp. alarm*)
	FLT284	AT %MX1.4017.11 :	BOOL;(*Trolley motor#2 high temp. trip*)
	FLT285	AT %MX1.4017.12 :	BOOL;(*Trolley motor#3 high temp. alarm*)
	FLT286	AT %MX1.4017.13 :	BOOL;(*Trolley motor#3 high temp. trip*)
	FLT287	AT %MX1.4017.14 :	BOOL;(*Trolley motor#4 high temp. alarm*)
	FLT288	AT %MX1.4017.15 :	BOOL;(*Trolley motor#4 high temp. trip*)
	FLT289	AT %MX1.4018.0 :	BOOL;(*Trolley left anchor not released*)
	FLT290	AT %MX1.4018.1 :	BOOL;(*Trolley right anchor not released*)
	FLT291	AT %MX1.4018.2 :	BOOL;(*Trolley motor fan contactor check fault*)
	FLT292	AT %MX1.4018.3 :	BOOL;(*Trolley motor heater contactor check fault*)
	FLT293	AT %MX1.4018.4 :	BOOL;(*Trolley door gate LS#1 open fault*)
	FLT294	AT %MX1.4018.5 :	BOOL;(*Trolley door gate LS#2 open fault*)
	FLT295	AT %MX1.4018.6 :	BOOL;(*Trolley washing platform gate LS open fault*)
	FLT296	AT %MX1.4018.7 :	BOOL;
	FLT297	AT %MX1.4018.8 :	BOOL;
	FLT298	AT %MX1.4018.9 :	BOOL;
	FLT299	AT %MX1.4018.10 :	BOOL;
	FLT300	AT %MX1.4018.11 :	BOOL;
	FLT301	AT %MX1.4018.12 :	BOOL;(*Boom overspeed fault*)
	FLT302	AT %MX1.4018.13 :	BOOL;(*Boom speed setting and feedback disagree fault*)
	FLT303	AT %MX1.4018.14 :	BOOL;(*Boom up over travel fault*)
	FLT304	AT %MX1.4018.15 :	BOOL;(*Boom changeover motor main contactor check fault*)
	FLT305	AT %MX1.4019.0 :	BOOL;(*Boom changeover encoder relay check fault*)
	FLT306	AT %MX1.4019.1 :	BOOL;(*Boom emergency stop relay check fault*)
	FLT307	AT %MX1.4019.2 :	BOOL;(*Boom emergency stop contactor check fault*)
	FLT308	AT %MX1.4019.3 :	BOOL;(*Boom brake contactor check fault*)
	FLT309	AT %MX1.4019.4 :	BOOL;(*Boom brake LS release fault*)
	FLT310	AT %MX1.4019.5 :	BOOL;(*Boom run time check fault*)
	FLT311	AT %MX1.4019.6 :	BOOL;(*Boom command lost while brake open fault*)
	FLT312	AT %MX1.4019.7 :	BOOL;(*Boom brake manual release LS open fault*)
	FLT313	AT %MX1.4019.8 :	BOOL;(*Boom brake wear fault*)
	FLT314	AT %MX1.4019.9 :	BOOL;(*Boom drum rope overlap#1 fault*)
	FLT315	AT %MX1.4019.10 :	BOOL;(*Boom drum rope overlap#2 fault*)
	FLT316	AT %MX1.4019.11 :	BOOL;(*Boom left wire rope slack fault*)
	FLT317	AT %MX1.4019.12 :	BOOL;(*Boom right wire rope slack fault*)
	FLT318	AT %MX1.4019.13 :	BOOL;(*Boom position compare fault*)
	FLT319	AT %MX1.4019.14 :	BOOL;(*Boom position not valid*)
	FLT320	AT %MX1.4019.15 :	BOOL;(*Boom up slowdown LS position check fault*)
	FLT321	AT %MX1.4020.0 :	BOOL;(*Boom up slowdown LS not active check fault*)
	FLT322	AT %MX1.4020.1 :	BOOL;(*Boom up slowdown LS speed check fault*)
	FLT323	AT %MX1.4020.2 :	BOOL;(*Boom down slowdown LS position check fault*)
	FLT324	AT %MX1.4020.3 :	BOOL;(*Boom down slowdown LS not active check fault*)
	FLT325	AT %MX1.4020.4 :	BOOL;(*Boom down slowdown LS speed check fault*)
	FLT326	AT %MX1.4020.5 :	BOOL;(*Boom motor high temp. alarm*)
	FLT327	AT %MX1.4020.6 :	BOOL;(*Boom motor high temp. trip*)
	FLT328	AT %MX1.4020.7 :	BOOL;(*Boom left anti-collisiton fault*)
	FLT329	AT %MX1.4020.8 :	BOOL;(*Boom right anti-collisiton fault*)
	FLT330	AT %MX1.4020.9 :	BOOL;(*Boom latch#1 contactor check fault*)
	FLT331	AT %MX1.4020.10 :	BOOL;(*Boom latch#2 contactor check fault*)
	FLT332	AT %MX1.4020.11 :	BOOL;(*Boom latch#1 up LS check fault*)
	FLT333	AT %MX1.4020.12 :	BOOL;(*Boom latch#2 up LS check fault*)
	FLT334	AT %MX1.4020.13 :	BOOL;(*Boom motor fan contactor check fault*)
	FLT335	AT %MX1.4020.14 :	BOOL;(*Boom motor heater contactor check fault*)
	FLT336	AT %MX1.4020.15 :	BOOL;(*Boom left access gate LS open fault*)
	FLT337	AT %MX1.4021.0 :	BOOL;(*Boom right access gate LS open fault*)
	FLT338	AT %MX1.4021.1 :	BOOL;(*Boom latch in overtime check fault*)
	FLT339	AT %MX1.4021.2 :	BOOL;(*Boom latch in overtime trip*)
	FLT340	AT %MX1.4021.3 :	BOOL;
	FLT341	AT %MX1.4021.4 :	BOOL;
	FLT342	AT %MX1.4021.5 :	BOOL;
	FLT343	AT %MX1.4021.6 :	BOOL;
	FLT344	AT %MX1.4021.7 :	BOOL;
	FLT345	AT %MX1.4021.8 :	BOOL;
	FLT346	AT %MX1.4021.9 :	BOOL;
	FLT347	AT %MX1.4021.10 :	BOOL;
	FLT348	AT %MX1.4021.11 :	BOOL;
	FLT349	AT %MX1.4021.12 :	BOOL;
	FLT350	AT %MX1.4021.13 :	BOOL;
	FLT351	AT %MX1.4021.14 :	BOOL;(*Gantry inv sx communication error*)
	FLT352	AT %MX1.4021.15 :	BOOL;(*Gantry emergency stop relay check fault*)
	FLT353	AT %MX1.4022.0 :	BOOL;(*Gantry emergency stop contactor check fault*)
	FLT354	AT %MX1.4022.1 :	BOOL;(*Gantry brake contactor check fault*)
	FLT355	AT %MX1.4022.2 :	BOOL;(*Gantry brake#1 LS release fault*)
	FLT356	AT %MX1.4022.3 :	BOOL;(*Gantry brake#2 LS release fault*)
	FLT357	AT %MX1.4022.4 :	BOOL;(*Gantry brake#3 LS release fault*)
	FLT358	AT %MX1.4022.5 :	BOOL;(*Gantry brake#4 LS release fault*)
	FLT359	AT %MX1.4022.6 :	BOOL;(*Gantry brake#5 LS release fault*)
	FLT360	AT %MX1.4022.7 :	BOOL;(*Gantry brake#6 LS release fault*)
	FLT361	AT %MX1.4022.8 :	BOOL;(*Gantry brake#7 LS release fault*)
	FLT362	AT %MX1.4022.9 :	BOOL;(*Gantry brake#8 LS release fault*)
	FLT363	AT %MX1.4022.10 :	BOOL;(*Gantry brake#9 LS release fault*)
	FLT364	AT %MX1.4022.11 :	BOOL;(*Gantry brake#10 LS release fault*)
	FLT365	AT %MX1.4022.12 :	BOOL;(*Gantry brake#11 LS release fault*)
	FLT366	AT %MX1.4022.13 :	BOOL;(*Gantry brake#12 LS release fault*)
	FLT367	AT %MX1.4022.14 :	BOOL;(*Gantry brake#13 LS release fault*)
	FLT368	AT %MX1.4022.15 :	BOOL;(*Gantry brake#14 LS release fault*)
	FLT369	AT %MX1.4023.0 :	BOOL;(*Gantry brake#15 LS release fault*)
	FLT370	AT %MX1.4023.1 :	BOOL;(*Gantry brake#16 LS release fault*)
	FLT371	AT %MX1.4023.2 :	BOOL;(*Gantry brake#17 LS release fault*)
	FLT372	AT %MX1.4023.3 :	BOOL;(*Gantry brake#18 LS release fault*)
	FLT373	AT %MX1.4023.4 :	BOOL;(*Gantry brake#19 LS release fault*)
	FLT374	AT %MX1.4023.5 :	BOOL;(*Gantry brake#20 LS release fault*)
	FLT375	AT %MX1.4023.6 :	BOOL;(*Gantry run time check fault*)
	FLT376	AT %MX1.4023.7 :	BOOL;(*Gantry command lost while brake open fault*)
	FLT377	AT %MX1.4023.8 :	BOOL;(*Gantry brake#1 manual release LS open fault*)
	FLT378	AT %MX1.4023.9 :	BOOL;(*Gantry brake#2 manual release LS open fault*)
	FLT379	AT %MX1.4023.10 :	BOOL;(*Gantry brake#3 manual release LS open fault*)
	FLT380	AT %MX1.4023.11 :	BOOL;(*Gantry brake#4 manual release LS open fault*)
	FLT381	AT %MX1.4023.12 :	BOOL;(*Gantry brake#5 manual release LS open fault*)
	FLT382	AT %MX1.4023.13 :	BOOL;(*Gantry brake#6 manual release LS open fault*)
	FLT383	AT %MX1.4023.14 :	BOOL;(*Gantry brake#7 manual release LS open fault*)
	FLT384	AT %MX1.4023.15 :	BOOL;(*Gantry brake#8 manual release LS open fault*)
	FLT385	AT %MX1.4024.0 :	BOOL;(*Gantry brake#9 manual release LS open fault*)
	FLT386	AT %MX1.4024.1 :	BOOL;(*Gantry brake#10 manual release LS open fault*)
	FLT387	AT %MX1.4024.2 :	BOOL;(*Gantry brake#11 manual release LS open fault*)
	FLT388	AT %MX1.4024.3 :	BOOL;(*Gantry brake#12 manual release LS open fault*)
	FLT389	AT %MX1.4024.4 :	BOOL;(*Gantry brake#13 manual release LS open fault*)
	FLT390	AT %MX1.4024.5 :	BOOL;(*Gantry brake#14 manual release LS open fault*)
	FLT391	AT %MX1.4024.6 :	BOOL;(*Gantry brake#15 manual release LS open fault*)
	FLT392	AT %MX1.4024.7 :	BOOL;(*Gantry brake#16 manual release LS open fault*)
	FLT393	AT %MX1.4024.8 :	BOOL;(*Gantry brake#17 manual release LS open fault*)
	FLT394	AT %MX1.4024.9 :	BOOL;(*Gantry brake#18 manual release LS open fault*)
	FLT395	AT %MX1.4024.10 :	BOOL;(*Gantry brake#19 manual release LS open fault*)
	FLT396	AT %MX1.4024.11 :	BOOL;(*Gantry brake#20 manual release LS open fault*)
	FLT397	AT %MX1.4024.12 :	BOOL;(*Gantry brake#1 wear fault*)
	FLT398	AT %MX1.4024.13 :	BOOL;(*Gantry brake#2 wear fault*)
	FLT399	AT %MX1.4024.14 :	BOOL;(*Gantry brake#3 wear fault*)
	FLT400	AT %MX1.4024.15 :	BOOL;(*Gantry brake#4 wear fault*)
	FLT401	AT %MX1.4025.0 :	BOOL;(*Gantry brake#5 wear fault*)
	FLT402	AT %MX1.4025.1 :	BOOL;(*Gantry brake#6 wear fault*)
	FLT403	AT %MX1.4025.2 :	BOOL;(*Gantry brake#7 wear fault*)
	FLT404	AT %MX1.4025.3 :	BOOL;(*Gantry brake#8 wear fault*)
	FLT405	AT %MX1.4025.4 :	BOOL;(*Gantry brake#9 wear fault*)
	FLT406	AT %MX1.4025.5 :	BOOL;(*Gantry brake#10 wear fault*)
	FLT407	AT %MX1.4025.6 :	BOOL;(*Gantry brake#11 wear fault*)
	FLT408	AT %MX1.4025.7 :	BOOL;(*Gantry brake#12 wear fault*)
	FLT409	AT %MX1.4025.8 :	BOOL;(*Gantry brake#13 wear fault*)
	FLT410	AT %MX1.4025.9 :	BOOL;(*Gantry brake#14 wear fault*)
	FLT411	AT %MX1.4025.10 :	BOOL;(*Gantry brake#15 wear fault*)
	FLT412	AT %MX1.4025.11 :	BOOL;(*Gantry brake#16 wear fault*)
	FLT413	AT %MX1.4025.12 :	BOOL;(*Gantry brake#17 wear fault*)
	FLT414	AT %MX1.4025.13 :	BOOL;(*Gantry brake#18 wear fault*)
	FLT415	AT %MX1.4025.14 :	BOOL;(*Gantry brake#19 wear fault*)
	FLT416	AT %MX1.4025.15 :	BOOL;(*Gantry brake#20 wear fault*)
	FLT417	AT %MX1.4026.0 :	BOOL;(*Gantry masterswtich encoder fault*)
	FLT418	AT %MX1.4026.1 :	BOOL;(*Gantry motor#1 over temp. trip*)
	FLT419	AT %MX1.4026.2 :	BOOL;(*Gantry motor#2 over temp. trip*)
	FLT420	AT %MX1.4026.3 :	BOOL;(*Gantry motor#3 over temp. trip*)
	FLT421	AT %MX1.4026.4 :	BOOL;(*Gantry motor#4 over temp. trip*)
	FLT422	AT %MX1.4026.5 :	BOOL;(*Gantry motor#5 over temp. trip*)
	FLT423	AT %MX1.4026.6 :	BOOL;(*Gantry motor#6 over temp. trip*)
	FLT424	AT %MX1.4026.7 :	BOOL;(*Gantry motor#7 over temp. trip*)
	FLT425	AT %MX1.4026.8 :	BOOL;(*Gantry motor#8 over temp. trip*)
	FLT426	AT %MX1.4026.9 :	BOOL;(*Gantry motor#9 over temp. trip*)
	FLT427	AT %MX1.4026.10 :	BOOL;(*Gantry motor#10 over temp. trip*)
	FLT428	AT %MX1.4026.11 :	BOOL;(*Gantry motor#11 over temp. trip*)
	FLT429	AT %MX1.4026.12 :	BOOL;(*Gantry motor#12 over temp. trip*)
	FLT430	AT %MX1.4026.13 :	BOOL;(*Gantry motor#13 over temp. trip*)
	FLT431	AT %MX1.4026.14 :	BOOL;(*Gantry motor#14 over temp. trip*)
	FLT432	AT %MX1.4026.15 :	BOOL;(*Gantry motor#15 over temp. trip*)
	FLT433	AT %MX1.4027.0 :	BOOL;(*Gantry motor#16 over temp. trip*)
	FLT434	AT %MX1.4027.1 :	BOOL;(*Gantry motor#17 over temp. trip*)
	FLT435	AT %MX1.4027.2 :	BOOL;(*Gantry motor#18 over temp. trip*)
	FLT436	AT %MX1.4027.3 :	BOOL;(*Gantry motor#19 over temp. trip*)
	FLT437	AT %MX1.4027.4 :	BOOL;(*Gantry motor#20 over temp. trip*)
	FLT438	AT %MX1.4027.5 :	BOOL;(*Gantry landside stow pin not released*)
	FLT439	AT %MX1.4027.6 :	BOOL;(*Gantry waterside stow pin not released*)
	FLT440	AT %MX1.4027.7 :	BOOL;(*Gantry motor heater contactor check fault*)
	FLT441	AT %MX1.4027.8 :	BOOL;(*Gantry warning device contactor check fault*)
	FLT442	AT %MX1.4027.9 :	BOOL;
	FLT443	AT %MX1.4027.10 :	BOOL;
	FLT444	AT %MX1.4027.11 :	BOOL;
	FLT445	AT %MX1.4027.12 :	BOOL;
	FLT446	AT %MX1.4027.13 :	BOOL;
	FLT447	AT %MX1.4027.14 :	BOOL;
	FLT448	AT %MX1.4027.15 :	BOOL;
	FLT449	AT %MX1.4028.0 :	BOOL;
	FLT450	AT %MX1.4028.1 :	BOOL;
	FLT451	AT %MX1.4028.2 :	BOOL;(*RHC#1 synchronization contactor check fault*)
	FLT452	AT %MX1.4028.3 :	BOOL;(*RHC#1 ready contactor check fault*)
	FLT453	AT %MX1.4028.4 :	BOOL;(*RHC#1 charge failed*)
	FLT454	AT %MX1.4028.5 :	BOOL;(*RHC#1 main contactor check fault*)
	FLT455	AT %MX1.4028.6 :	BOOL;(*RHC#1 run check fault*)
	FLT456	AT %MX1.4028.7 :	BOOL;(*RHC#1 charge contactor run time check fault*)
	FLT457	AT %MX1.4028.8 :	BOOL;(*RHC#2 synchronization contactor check fault*)
	FLT458	AT %MX1.4028.9 :	BOOL;(*RHC#2 ready contactor check fault*)
	FLT459	AT %MX1.4028.10 :	BOOL;(*RHC#2 charge failed*)
	FLT460	AT %MX1.4028.11 :	BOOL;(*RHC#2 main contactor check fault*)
	FLT461	AT %MX1.4028.12 :	BOOL;(*RHC#2 run check fault*)
	FLT462	AT %MX1.4028.13 :	BOOL;(*RHC#2 charge contactor run time check fault*)
	FLT463	AT %MX1.4028.14 :	BOOL;
	FLT464	AT %MX1.4028.15 :	BOOL;
	FLT465	AT %MX1.4029.0 :	BOOL;
	FLT466	AT %MX1.4029.1 :	BOOL;
	FLT467	AT %MX1.4029.2 :	BOOL;
	FLT468	AT %MX1.4029.3 :	BOOL;
	FLT469	AT %MX1.4029.4 :	BOOL;
	FLT470	AT %MX1.4029.5 :	BOOL;
	FLT471	AT %MX1.4029.6 :	BOOL;(*TLS inv#1 sx communication error*)
	FLT472	AT %MX1.4029.7 :	BOOL;(*TLS inv#2 sx communication error*)
	FLT473	AT %MX1.4029.8 :	BOOL;(*TLS inv#3 sx communication error*)
	FLT474	AT %MX1.4029.9 :	BOOL;(*TLS inv#4 sx communication error*)
	FLT475	AT %MX1.4029.10 :	BOOL;(*TLS brake#1 contactor check fault*)
	FLT476	AT %MX1.4029.11 :	BOOL;(*TLS brake#2 contactor check fault*)
	FLT477	AT %MX1.4029.12 :	BOOL;(*TLS brake#3 contactor check fault*)
	FLT478	AT %MX1.4029.13 :	BOOL;(*TLS brake#4 contactor check fault*)
	FLT479	AT %MX1.4029.14 :	BOOL;(*TLS brake#1 LS release check fault*)
	FLT480	AT %MX1.4029.15 :	BOOL;(*TLS brake#2 LS release check fault*)
	FLT481	AT %MX1.4030.0 :	BOOL;(*TLS brake#3 LS release check fault*)
	FLT482	AT %MX1.4030.1 :	BOOL;(*TLS brake#4 LS release check fault*)
	FLT483	AT %MX1.4030.2 :	BOOL;(*TLS motor#1 over temp. fault*)
	FLT484	AT %MX1.4030.3 :	BOOL;(*TLS motor#2 over temp. fault*)
	FLT485	AT %MX1.4030.4 :	BOOL;(*TLS motor#3 over temp. fault*)
	FLT486	AT %MX1.4030.5 :	BOOL;(*TLS motor#4 over temp. fault*)
	FLT487	AT %MX1.4030.6 :	BOOL;(*TLS#1 run time check fault*)
	FLT488	AT %MX1.4030.7 :	BOOL;(*TLS#2 run time check fault*)
	FLT489	AT %MX1.4030.8 :	BOOL;(*TLS#3 run time check fault*)
	FLT490	AT %MX1.4030.9 :	BOOL;(*TLS#4 run time check fault*)
	FLT491	AT %MX1.4030.10 :	BOOL;(*TLS motor heater check fault*)
	FLT492	AT %MX1.4030.11 :	BOOL;(*TLS#1 position not valid*)
	FLT493	AT %MX1.4030.12 :	BOOL;(*TLS#2 position not valid*)
	FLT494	AT %MX1.4030.13 :	BOOL;(*TLS#3 position not valid*)
	FLT495	AT %MX1.4030.14 :	BOOL;(*TLS#4 position not valid*)
	FLT496	AT %MX1.4030.15 :	BOOL;
	FLT497	AT %MX1.4031.0 :	BOOL;
	FLT498	AT %MX1.4031.1 :	BOOL;
	FLT499	AT %MX1.4031.2 :	BOOL;
	FLT500	AT %MX1.4031.3 :	BOOL;
	FLT501	AT %MX1.4031.4 :	BOOL;(*Bypass active: Overtravel LS bypass at +01F41*)
	FLT502	AT %MX1.4031.5 :	BOOL;(*Bypass active: Snag bypass at +01F41*)
	FLT503	AT %MX1.4031.6 :	BOOL;(*Bypass active: High wind bypass at +01F41*)
	FLT504	AT %MX1.4031.7 :	BOOL;(*Bypass active: Spreader land bypass at +10S02*)
	FLT505	AT %MX1.4031.8 :	BOOL;(*Bypass active: TTDS bypass at +10S02*)
	FLT506	AT %MX1.4031.9 :	BOOL;(*Bypass active: Gantry anchor bypass at +06F01*)
	FLT507	AT %MX1.4031.10 :	BOOL;(*Bypass active: High wind bypass at +06F01*)
	FLT508	AT %MX1.4031.11 :	BOOL;(*Bypass active: Eccentric load bypass at +01F41*)
	FLT509	AT %MX1.4031.12 :	BOOL;
	FLT510	AT %MX1.4031.13 :	BOOL;
	FLT511	AT %MX1.4031.14 :	BOOL;
	FLT512	AT %MX1.4031.15 :	BOOL;
	FLT513	AT %MX1.4032.0 :	BOOL;
	FLT514	AT %MX1.4032.1 :	BOOL;
	FLT515	AT %MX1.4032.2 :	BOOL;
	FLT516	AT %MX1.4032.3 :	BOOL;
	FLT517	AT %MX1.4032.4 :	BOOL;
	FLT518	AT %MX1.4032.5 :	BOOL;
	FLT519	AT %MX1.4032.6 :	BOOL;
	FLT520	AT %MX1.4032.7 :	BOOL;
	FLT521	AT %MX1.4032.8 :	BOOL;
	FLT522	AT %MX1.4032.9 :	BOOL;
	FLT523	AT %MX1.4032.10 :	BOOL;
	FLT524	AT %MX1.4032.11 :	BOOL;
	FLT525	AT %MX1.4032.12 :	BOOL;
	FLT526	AT %MX1.4032.13 :	BOOL;
	FLT527	AT %MX1.4032.14 :	BOOL;
	FLT528	AT %MX1.4032.15 :	BOOL;
	FLT529	AT %MX1.4033.0 :	BOOL;
	FLT530	AT %MX1.4033.1 :	BOOL;
	FLT531	AT %MX1.4033.2 :	BOOL;
	FLT532	AT %MX1.4033.3 :	BOOL;
	FLT533	AT %MX1.4033.4 :	BOOL;
	FLT534	AT %MX1.4033.5 :	BOOL;
	FLT535	AT %MX1.4033.6 :	BOOL;
	FLT536	AT %MX1.4033.7 :	BOOL;
	FLT537	AT %MX1.4033.8 :	BOOL;
	FLT538	AT %MX1.4033.9 :	BOOL;
	FLT539	AT %MX1.4033.10 :	BOOL;
	FLT540	AT %MX1.4033.11 :	BOOL;
	FLT541	AT %MX1.4033.12 :	BOOL;
	FLT542	AT %MX1.4033.13 :	BOOL;
	FLT543	AT %MX1.4033.14 :	BOOL;
	FLT544	AT %MX1.4033.15 :	BOOL;
	FLT545	AT %MX1.4034.0 :	BOOL;
	FLT546	AT %MX1.4034.1 :	BOOL;
	FLT547	AT %MX1.4034.2 :	BOOL;
	FLT548	AT %MX1.4034.3 :	BOOL;
	FLT549	AT %MX1.4034.4 :	BOOL;
	FLT550	AT %MX1.4034.5 :	BOOL;
	FLT551	AT %MX1.4034.6 :	BOOL;(*CB OFF_AUXILIARY POWER MAIN CB SH.003.M+01F11/1*)
	FLT552	AT %MX1.4034.7 :	BOOL;(*CB ON_SHORE POWER MAIN CB SH.003.M+01F11/1*)
	FLT553	AT %MX1.4034.8 :	BOOL;(*CB OFF_SURGE PROTECT DEVICE CB SH.003.M+01F11/1*)
	FLT554	AT %MX1.4034.9 :	BOOL;(*CB OFF_CABIN AUXILIARY POWER CB SH.003.M+01F11/1*)
	FLT555	AT %MX1.4034.10 :	BOOL;(*CB OFF_+01F13 AUXILIARY POWER CB SH.003.M+01F11/1*)
	FLT556	AT %MX1.4034.11 :	BOOL;(*CB OFF_+01F15 AUXILIARY POWER CB SH.003.M+01F11/1*)
	FLT557	AT %MX1.4034.12 :	BOOL;(*CB OFF_+01F18 AUXILIARY POWER CB SH.003.M+01F11/1*)
	FLT558	AT %MX1.4034.13 :	BOOL;
	FLT559	AT %MX1.4034.14 :	BOOL;(*CB OFF_DRIVER CONTROL POWER R0 T0 CB SH.003.M+01F11/2*)
	FLT560	AT %MX1.4034.15 :	BOOL;(*CB OFF_HOIST INVERTER#1 FAN POWER R1 T1 CB SH.003.M+01F11/2*)
	FLT561	AT %MX1.4035.0 :	BOOL;(*CB OFF_HOIST INVERTER#2 FAN POWER R1 T1 CB SH.003.M+01F11/2*)
	FLT562	AT %MX1.4035.1 :	BOOL;
	FLT563	AT %MX1.4035.2 :	BOOL;(*CB OFF_RHC#1 CONTROL POWER R0 T0 CB SH.003.M+01F11/2*)
	FLT564	AT %MX1.4035.3 :	BOOL;(*CB OFF_RHC#2 CONTROL POWER R0 T0 CB SH.003.M+01F11/2*)
	FLT565	AT %MX1.4035.4 :	BOOL;(*CB OFF_HOIST INVERTER#1 CONTROL POWER R0 T0 CB SH.003.M+01F11/2*)
	FLT566	AT %MX1.4035.5 :	BOOL;(*CB OFF_+06F01 PANEL CONTROL POWER CB SH.003.M+01F11/3*)
	FLT567	AT %MX1.4035.6 :	BOOL;(*CB OFF_+01F13 PANEL CONTROL POWER CB SH.003.M+01F11/3*)
	FLT568	AT %MX1.4035.7 :	BOOL;(*CB OFF_+01F15 PANEL CONTROL POWER CB SH.003.M+01F11/3*)
	FLT569	AT %MX1.4035.8 :	BOOL;(*CB OFF_+01F18 PANEL CONTROL POWER CB SH.003.M+01F11/3*)
	FLT570	AT %MX1.4035.9 :	BOOL;(*CB OFF_+01F41 PANEL CONTROL POWER CB SH.003.M+01F11/3*)
	FLT571	AT %MX1.4035.10 :	BOOL;(*CB OFF_RHC CONTROL POWER CB SH.003.M+01F11/3*)
	FLT572	AT %MX1.4035.11 :	BOOL;(*CB OFF_+01F11 PANEL INSIDE I/O POWER CB SH.003.M+01F11/4*)
	FLT573	AT %MX1.4035.12 :	BOOL;(*CB OFF_GANTRY INVERTER FAN POWER R1 T1 CB SH.003.M+01F11/4*)
	FLT574	AT %MX1.4035.13 :	BOOL;(*CB OFF_+01F11 OUTPUT MODULE POWER CB SH.003.M+01F11/4*)
	FLT575	AT %MX1.4035.14 :	BOOL;(*CB OFF_EMERGENCY STOP SAFETY RELAY POWER CB SH.003.M+01F11/4*)
	FLT576	AT %MX1.4035.15 :	BOOL;(*CB OFF_HOIST INVERTER#2 CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT577	AT %MX1.4036.0 :	BOOL;(*CB OFF_T/B INVERTER CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT578	AT %MX1.4036.1 :	BOOL;(*CB OFF_T/B INVERTER FAN POWER R1 T1 CB SH.003.M+01F11/5*)
	FLT579	AT %MX1.4036.2 :	BOOL;(*CB OFF_GANTRY INVERTER CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT580	AT %MX1.4036.3 :	BOOL;(*CB OFF_TLS INVERTER#1 CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT581	AT %MX1.4036.4 :	BOOL;(*CB OFF_TLS INVERTER#2 CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT582	AT %MX1.4036.5 :	BOOL;(*CB OFF_TLS INVERTER#3 CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT583	AT %MX1.4036.6 :	BOOL;(*CB OFF_TLS INVERTER#4 CONTROL POWER R0 T0 CB SH.003.M+01F11/5*)
	FLT584	AT %MX1.4036.7 :	BOOL;(*CB OFF_+01F11 PANEL OUTSIDE I/O POWER CB SH.003.M+01F11/9*)
	FLT585	AT %MX1.4036.8 :	BOOL;(*CB OFF_CABIN AUXILIARY POWER CB SH.003.M+10F01/11*)
	FLT586	AT %MX1.4036.9 :	BOOL;(*CB OFF_+10F01 OUTPUT MODULE POWER CB SH.003.M+10F01/11*)
	FLT587	AT %MX1.4036.10 :	BOOL;(*CB OFF_DISPLAY POWER CB SH.003.M+10F01/11*)
	FLT588	AT %MX1.4036.11 :	BOOL;(*CB OFF_+10F01 PANEL OUTSIDE I/O POWER CB SH.003.M+10F01/11*)
	FLT589	AT %MX1.4036.12 :	BOOL;(*CB OFF_+10F01 PANEL INSIDE I/O POWER CB SH.003.M+10F01/11*)
	FLT590	AT %MX1.4036.13 :	BOOL;
	FLT591	AT %MX1.4036.14 :	BOOL;(*CB OFF_RHC#1 MAIN POWER CB SH.005.M+01F11/1*)
	FLT592	AT %MX1.4036.15 :	BOOL;(*CB OFF_RHC#1 SYNCHRONIZATION POWER CB SH.005.M+01F11/1*)
	FLT593	AT %MX1.4037.0 :	BOOL;(*CB OFF_RHC#2 MAIN POWER CB SH.006.M+01F11/1*)
	FLT594	AT %MX1.4037.1 :	BOOL;(*CB OFF_RHC#2 SYNCHRONIZATION POWER CB SH.006.M+01F11/1*)
	FLT595	AT %MX1.4037.2 :	BOOL;
	FLT596	AT %MX1.4037.3 :	BOOL;(*CB OFF_+01F13 AUXILIARY POWER CB SH.010.M+01F13/1*)
	FLT597	AT %MX1.4037.4 :	BOOL;(*CB OFF_HOIST BRAKE POWER CB SH.010.M+01F13/1*)
	FLT598	AT %MX1.4037.5 :	BOOL;(*CB OFF_HOIST MOTOR#1 FAN POWER CB SH.010.M+01F13/1*)
	FLT599	AT %MX1.4037.6 :	BOOL;(*CB OFF_HOIST MOTOR HEATER POWER CB SH.010.M+01F13/1*)
	FLT600	AT %MX1.4037.7 :	BOOL;(*CB OFF_+01F13 PANEL CONTROL POWER CB SH.010.M+01F13/1*)
	FLT601	AT %MX1.4037.8 :	BOOL;(*CB OFF_H/G SAFETY RELAY CONTROL POWER CB SH.010.M+01F13/1*)
	FLT602	AT %MX1.4037.9 :	BOOL;(*CB OFF_+01F13 OUTPUT MODULE POWER CB SH.010.M+01F13/1*)
	FLT603	AT %MX1.4037.10 :	BOOL;(*CB OFF_HOIST EMERGENCY CIRCUIT POWER CB SH.010.M+01F13/3*)
	FLT604	AT %MX1.4037.11 :	BOOL;(*CB OFF_+01F13 PANEL INSIDE I/O POWER CB SH.010.M+01F13/3*)
	FLT605	AT %MX1.4037.12 :	BOOL;(*CB OFF_HOIST MOTOR#2 FAN POWER CB SH.010.M+01F13/3*)
	FLT606	AT %MX1.4037.13 :	BOOL;(*CB OFF_HOIST BRAKE#1 POWER CB SH.010.M+01F13/3*)
	FLT607	AT %MX1.4037.14 :	BOOL;(*CB OFF_HOIST BRAKE#2 POWER CB SH.010.M+01F13/3*)
	FLT608	AT %MX1.4037.15 :	BOOL;(*CB OFF_+01F13 PANEL OUTSIDE I/O POWER CB SH.010.M+01F13/5*)
	FLT609	AT %MX1.4038.0 :	BOOL;(*CB OFF_HOIST EMERGENCY MOTOR HEATER POWER CB SH.015.M+01F13/1*)
	FLT610	AT %MX1.4038.1 :	BOOL;
	FLT611	AT %MX1.4038.2 :	BOOL;(*CB OFF_TLS BRAKE#1 POWER CB SH.019.M+01F11/20*)
	FLT612	AT %MX1.4038.3 :	BOOL;(*CB OFF_TLS BRAKE#2 POWER CB SH.019.M+01F11/20*)
	FLT613	AT %MX1.4038.4 :	BOOL;(*CB OFF_TLS BRAKE#3 POWER CB SH.019.M+01F11/20*)
	FLT614	AT %MX1.4038.5 :	BOOL;(*CB OFF_TLS BRAKE#4 POWER CB SH.019.M+01F11/20*)
	FLT615	AT %MX1.4038.6 :	BOOL;
	FLT616	AT %MX1.4038.7 :	BOOL;(*CB OFF_+01F15 AUXILIARY POWER CB SH.020.M+01F15/1*)
	FLT617	AT %MX1.4038.8 :	BOOL;(*CB OFF_TROLLEY BRAKE POWER CB SH.020.M+01F15/1*)
	FLT618	AT %MX1.4038.9 :	BOOL;(*CB OFF_+01F15 PANEL CONTROL POWER CB SH.020.M+01F15/1*)
	FLT619	AT %MX1.4038.10 :	BOOL;(*CB OFF_T/B SAFETY RELAY CONTROL POWER CB SH.020.M+01F15/1*)
	FLT620	AT %MX1.4038.11 :	BOOL;(*CB OFF_+01F15 OUTPUT MODULE POWER CB SH.020.M+01F15/1*)
	FLT621	AT %MX1.4038.12 :	BOOL;(*CB OFF_T/B MOTOR CONTACTOR CHANGEOVER POWER CB SH.020.M+01F15/1*)
	FLT622	AT %MX1.4038.13 :	BOOL;(*CB OFF_+01F15 PANEL INSIDE I/O POWER CB SH.020.M+01F15/1*)
	FLT623	AT %MX1.4038.14 :	BOOL;(*CB OFF_TROLLEY BRAKE#1 POWER CB SH.020.M+10F01/10*)
	FLT624	AT %MX1.4038.15 :	BOOL;(*CB OFF_TROLLEY BRAKE#2 POWER CB SH.020.M+10F01/10*)
	FLT625	AT %MX1.4039.0 :	BOOL;(*CB OFF_TROLLEY BRAKE#3 POWER CB SH.020.M+10F01/10*)
	FLT626	AT %MX1.4039.1 :	BOOL;(*CB OFF_TROLLEY BRAKE#4 POWER CB SH.020.M+10F01/10*)
	FLT627	AT %MX1.4039.2 :	BOOL;(*CB OFF_TROLLEY MOTOR FAN POWER CB SH.020.M+10F01/11*)
	FLT628	AT %MX1.4039.3 :	BOOL;(*CB OFF_TROLLEY MOTOR#1 FAN POWER CB SH.020.M+10F01/11*)
	FLT629	AT %MX1.4039.4 :	BOOL;(*CB OFF_TROLLEY MOTOR#2 FAN POWER CB SH.020.M+10F01/11*)
	FLT630	AT %MX1.4039.5 :	BOOL;(*CB OFF_TROLLEY MOTOR#3 FAN POWER CB SH.020.M+10F01/11*)
	FLT631	AT %MX1.4039.6 :	BOOL;(*CB OFF_TROLLEY MOTOR#4 FAN POWER CB SH.020.M+10F01/11*)
	FLT632	AT %MX1.4039.7 :	BOOL;(*CB OFF_TROLLEY MOTOR HEATER POWER CB SH.020.M+10F01/11*)
	FLT633	AT %MX1.4039.8 :	BOOL;
	FLT634	AT %MX1.4039.9 :	BOOL;(*CB OFF_BACKREACH HYDRAULIC STATION PUMP CB SH.029M+01F11/1*)
	FLT635	AT %MX1.4039.10 :	BOOL;(*CB OFF_BACKREACH HYDRAULIC STATION PUMP HEATER CB SH.029M+01F11/1*)
	FLT636	AT %MX1.4039.11 :	BOOL;(*CB OFF_BACKREACH HYDRAULIC STATION SOLENOID POWER CB SH.029M+01F11/1*)
	FLT637	AT %MX1.4039.12 :	BOOL;(*CB OFF_TLS MOTOR HEATER POWER CB SH.029M+01F11/1*)
	FLT638	AT %MX1.4039.13 :	BOOL;
	FLT639	AT %MX1.4039.14 :	BOOL;(*CB OFF_BOOM BRAKE POWER CB SH.030M+01F15/1*)
	FLT640	AT %MX1.4039.15 :	BOOL;(*CB OFF_BOOM MOTOR FAN POWER CB SH.030M+01F15/1*)
	FLT641	AT %MX1.4040.0 :	BOOL;(*CB OFF_BOOM MOTOR HEATER POWER CB SH.030M+01F15/1*)
	FLT642	AT %MX1.4040.1 :	BOOL;(*CB OFF_BOOM LATCH THRUSTER#1 CB SH.030M+01F15/1*)
	FLT643	AT %MX1.4040.2 :	BOOL;(*CB OFF_BOOM LATCH THRUSTER#2 CB SH.030M+01F15/1*)
	FLT644	AT %MX1.4040.3 :	BOOL;(*CB OFF_TROLLEY MOTOR#1 POWER CB SH.030M+01F15/3*)
	FLT645	AT %MX1.4040.4 :	BOOL;(*CB OFF_TROLLEY MOTOR#2 POWER CB SH.030M+01F15/3*)
	FLT646	AT %MX1.4040.5 :	BOOL;(*CB OFF_TROLLEY MOTOR#3 POWER CB SH.030M+01F15/3*)
	FLT647	AT %MX1.4040.6 :	BOOL;(*CB OFF_TROLLEY MOTOR#4 POWER CB SH.030M+01F15/3*)
	FLT648	AT %MX1.4040.7 :	BOOL;(*CB OFF_+01F15 PANEL OUTSIDE I/O POWER CB  SH.030M+01F15/6*)
	FLT649	AT %MX1.4040.8 :	BOOL;
	FLT650	AT %MX1.4040.9 :	BOOL;(*CB OFF_GANTRY BRAKE POWER CB SH.040M+01F13/1*)
	FLT651	AT %MX1.4040.10 :	BOOL;(*CB OFF_GANTRY BRAKE#1 POWER CB SH.040M+01F13/1*)
	FLT652	AT %MX1.4040.11 :	BOOL;(*CB OFF_GANTRY BRAKE#2 POWER CB SH.040M+01F13/1*)
	FLT653	AT %MX1.4040.12 :	BOOL;(*CB OFF_GANTRY BRAKE#3 POWER CB SH.040M+01F13/1*)
	FLT654	AT %MX1.4040.13 :	BOOL;(*CB OFF_GANTRY BRAKE#4 POWER CB SH.040M+01F13/1*)
	FLT655	AT %MX1.4040.14 :	BOOL;(*CB OFF_GANTRY BRAKE#6 POWER CB SH.040M+01F13/1*)
	FLT656	AT %MX1.4040.15 :	BOOL;(*CB OFF_GANTRY BRAKE#7 POWER CB SH.040M+01F13/1*)
	FLT657	AT %MX1.4041.0 :	BOOL;(*CB OFF_GANTRY BRAKE#8 POWER CB SH.040M+01F13/2*)
	FLT658	AT %MX1.4041.1 :	BOOL;(*CB OFF_GANTRY BRAKE#9 POWER CB SH.040M+01F13/2*)
	FLT659	AT %MX1.4041.2 :	BOOL;(*CB OFF_GANTRY BRAKE#11 POWER CB SH.040M+01F13/2*)
	FLT660	AT %MX1.4041.3 :	BOOL;(*CB OFF_GANTRY BRAKE#12 POWER CB SH.040M+01F13/2*)
	FLT661	AT %MX1.4041.4 :	BOOL;(*CB OFF_GANTRY BRAKE#13 POWER CB SH.040M+01F13/2*)
	FLT662	AT %MX1.4041.5 :	BOOL;(*CB OFF_GANTRY BRAKE#14 POWER CB SH.040M+01F13/2*)
	FLT663	AT %MX1.4041.6 :	BOOL;(*CB OFF_GANTRY MOTOR#1~10 HEATER POWER CB SH.040M+01F13/2*)
	FLT664	AT %MX1.4041.7 :	BOOL;(*CB OFF_GANTRY BRAKE#16 POWER CB SH.040M+01F13/3*)
	FLT665	AT %MX1.4041.8 :	BOOL;(*CB OFF_GANTRY BRAKE#17 POWER CB SH.040M+01F13/3*)
	FLT666	AT %MX1.4041.9 :	BOOL;(*CB OFF_GANTRY BRAKE#18 POWER CB SH.040M+01F13/3*)
	FLT667	AT %MX1.4041.10 :	BOOL;(*CB OFF_GANTRY BRAKE#19 POWER CB SH.040M+01F13/3*)
	FLT668	AT %MX1.4041.11 :	BOOL;(*CB OFF_GANTRY BRAKE#20 POWER CB SH.040M+01F13/3*)
	FLT669	AT %MX1.4041.12 :	BOOL;(*CB OFF_GANTRY MOTOR#11~20 HEATER POWER CB SH.040M+01F13/3*)
	FLT670	AT %MX1.4041.13 :	BOOL;(*CB OFF_GANTRY WARNING DEVICE POWER CB SH.040M+01F13/3*)
	FLT671	AT %MX1.4041.14 :	BOOL;(*CB OFF_GANTRY BRAKE#5 POWER CB SH.040M+01F13/4*)
	FLT672	AT %MX1.4041.15 :	BOOL;(*CB OFF_GANTRY BRAKE#10 POWER CB SH.040M+01F13/4*)
	FLT673	AT %MX1.4042.0 :	BOOL;(*CB OFF_GANTRY BRAKE#15 POWER CB SH.040M+01F13/4*)
	FLT674	AT %MX1.4042.1 :	BOOL;(*CB OFF_+06F01 STATION OUTSIDE I/O POWER CB SH.040M+06F01/7*)
	FLT675	AT %MX1.4042.2 :	BOOL;(*CB OFF_GANTRY MOTOR#1 POWER CB SH.041M+01F13/1*)
	FLT676	AT %MX1.4042.3 :	BOOL;(*CB OFF_GANTRY MOTOR#2 POWER CB SH.041M+01F13/1*)
	FLT677	AT %MX1.4042.4 :	BOOL;(*CB OFF_GANTRY MOTOR#3 POWER CB SH.041M+01F13/1*)
	FLT678	AT %MX1.4042.5 :	BOOL;(*CB OFF_GANTRY MOTOR#4 POWER CB SH.041M+01F13/1*)
	FLT679	AT %MX1.4042.6 :	BOOL;(*CB OFF_GANTRY MOTOR#5 POWER CB SH.041M+01F13/1*)
	FLT680	AT %MX1.4042.7 :	BOOL;(*CB OFF_GANTRY MOTOR#6 POWER CB SH.041M+01F13/1*)
	FLT681	AT %MX1.4042.8 :	BOOL;(*CB OFF_GANTRY MOTOR#7 POWER CB SH.041M+01F13/1*)
	FLT682	AT %MX1.4042.9 :	BOOL;(*CB OFF_GANTRY MOTOR#8 POWER CB SH.041M+01F13/1*)
	FLT683	AT %MX1.4042.10 :	BOOL;(*CB OFF_GANTRY MOTOR#9 POWER CB SH.041M+01F13/2*)
	FLT684	AT %MX1.4042.11 :	BOOL;(*CB OFF_GANTRY MOTOR#10 POWER CB SH.041M+01F13/2*)
	FLT685	AT %MX1.4042.12 :	BOOL;(*CB OFF_GANTRY MOTOR#11 POWER CB SH.041M+01F13/2*)
	FLT686	AT %MX1.4042.13 :	BOOL;(*CB OFF_GANTRY MOTOR#12 POWER CB SH.041M+01F13/2*)
	FLT687	AT %MX1.4042.14 :	BOOL;(*CB OFF_GANTRY MOTOR#13 POWER CB SH.041M+01F13/2*)
	FLT688	AT %MX1.4042.15 :	BOOL;(*CB OFF_GANTRY MOTOR#14 POWER CB SH.041M+01F13/2*)
	FLT689	AT %MX1.4043.0 :	BOOL;(*CB OFF_GANTRY MOTOR#15 POWER CB SH.041M+01F13/2*)
	FLT690	AT %MX1.4043.1 :	BOOL;(*CB OFF_GANTRY MOTOR#16 POWER CB SH.041M+01F13/2*)
	FLT691	AT %MX1.4043.2 :	BOOL;(*CB OFF_GANTRY MOTOR#17 POWER CB SH.041M+01F13/3*)
	FLT692	AT %MX1.4043.3 :	BOOL;(*CB OFF_GANTRY MOTOR#18 POWER CB SH.041M+01F13/3*)
	FLT693	AT %MX1.4043.4 :	BOOL;(*CB OFF_GANTRY MOTOR#19 POWER CB SH.041M+01F13/3*)
	FLT694	AT %MX1.4043.5 :	BOOL;(*CB OFF_GANTRY MOTOR#20 POWER CB SH.041M+01F13/3*)
	FLT695	AT %MX1.4043.6 :	BOOL;(*CB OFF_LS WHEEL CLAMP HYDRAULIC STATION PUMP CB SH.041M+01F13/3*)
	FLT696	AT %MX1.4043.7 :	BOOL;(*CB OFF_WS WHEEL CLAMP HYDRAULIC STATION PUMP CB SH.041M+01F13/3*)
	FLT697	AT %MX1.4043.8 :	BOOL;
	FLT698	AT %MX1.4043.9 :	BOOL;(*CB OFF_SPREADER PUMP POWER CB SH.050M+10F01/2*)
	FLT699	AT %MX1.4043.10 :	BOOL;(*CB OFF_SPREADER CONTROL POWER CB SH.050M+10F01/2*)
	FLT700	AT %MX1.4043.11 :	BOOL;
	FLT701	AT %MX1.4043.12 :	BOOL;(*CB OFF_+01F12 AUXILIARY POWER SUPPLY CB SH.080M+01F12/1*)
	FLT702	AT %MX1.4043.13 :	BOOL;(*CB OFF_BOOM FLOODLIGHT POWER CB#1 SH.080M+01F12/1*)
	FLT703	AT %MX1.4043.14 :	BOOL;(*CB OFF_BOOM FLOODLIGHT POWER CB#2 SH.080M+01F12/1*)
	FLT704	AT %MX1.4043.15 :	BOOL;(*CB OFF_BOOM FLOODLIGHT POWER CB#3 SH.080M+01F12/1*)
	FLT705	AT %MX1.4044.0 :	BOOL;(*CB OFF_GIRDER FLOODLIGHT POWER CB SH.080M+01F12/1*)
	FLT706	AT %MX1.4044.1 :	BOOL;(*CB OFF_PORTAL BEAM LEFT FLOODLIGHT POWER CB SH.080M+01F12/1*)
	FLT707	AT %MX1.4044.2 :	BOOL;(*CB OFF_PORTAL BEAM RIGHT FLOODLIGHT POWER CB SH.080M+01F12/1*)
	FLT708	AT %MX1.4044.3 :	BOOL;(*CB OFF_GANTRY FLOODLIGHT POWER CB SH.080M+01F12/1*)
	FLT709	AT %MX1.4044.4 :	BOOL;(*CB OFF_LATCH AREA FLOODLIGHT POWER CB SH.080M+01F12/2*)
	FLT710	AT %MX1.4044.5 :	BOOL;
	FLT711	AT %MX1.4044.6 :	BOOL;(*CB OFF_PYLON WELDING OUTLET POWER CB SH.080M+01F12/2*)
	FLT712	AT %MX1.4044.7 :	BOOL;(*CB OFF_MAIN STAIRS WALKWAY LIGHT POWER CB SH.080M+01F12/2*)
	FLT713	AT %MX1.4044.8 :	BOOL;(*CB OFF_BOOM WALKWAY LIGHT POWER CB SH.080M+01F12/2*)
	FLT714	AT %MX1.4044.9 :	BOOL;(*CB OFF_GIRDER WALKWAY LIGHT POWER CB SH.080M+01F12/2*)
	FLT715	AT %MX1.4044.10 :	BOOL;(*CB OFF_LATCH AREA WALKWAY LIGHT POWER CB SH.080M+01F12/2*)
	FLT716	AT %MX1.4044.11 :	BOOL;(*CB OFF_BACKREACH WALKWAY LIGHT POWER CB SH.080M+01F12/2*)
	FLT717	AT %MX1.4044.12 :	BOOL;(*CB OFF_PORTAL BEAM/FASTOON FLATFORM WALKWAY LIGHT POWER CB SH.080M+01F12/3*)
	FLT718	AT %MX1.4044.13 :	BOOL;(*CB OFF_E-ROOM LIGHT POWER CB SH.080M+01F12/3*)
	FLT719	AT %MX1.4044.14 :	BOOL;(*CB OFF_M-HOUSE LIGHT POWER CB SH.080M+01F12/3*)
	FLT720	AT %MX1.4044.15 :	BOOL;(*CB OFF_GIRDER WELDING OUTLET POWER CB SH.080M+01F12/3*)
	FLT721	AT %MX1.4045.0 :	BOOL;(*CB OFF_CHECK ROOM LIGHT POWER CB SH.080M+01F12/3*)
	FLT722	AT %MX1.4045.1 :	BOOL;(*CB OFF_PANEL LIGHT/OUTLET POWER CB SH.080M+01F12/3*)
	FLT723	AT %MX1.4045.2 :	BOOL;(*CB OFF_LIGHT/FAN CONTROL CIRCUIT POWER CB SH.080M+01F12/3*)
	FLT724	AT %MX1.4045.3 :	BOOL;(*CB OFF_E-ROOM OUTLET POWER CB SH.080M+01F12/3*)
	FLT725	AT %MX1.4045.4 :	BOOL;(*CB OFF_M-HOUSE OUTLET POWER CB SH.080M+01F12/4*)
	FLT726	AT %MX1.4045.5 :	BOOL;(*CB OFF_BOOM OUTLET POWER CB SH.080M+01F12/4*)
	FLT727	AT %MX1.4045.6 :	BOOL;(*CB OFF_GIRDER OUTLET POWER CB SH.080M+01F12/4*)
	FLT728	AT %MX1.4045.7 :	BOOL;(*CB OFF_CHECK ROOM OUTLET POWER CB SH.080M+01F12/4*)
	FLT729	AT %MX1.4045.8 :	BOOL;(*CB OFF_PYLON OUTLET POWER CB SH.080M+01F12/4*)
	FLT730	AT %MX1.4045.9 :	BOOL;(*CB OFF_GANTRY LANDSIDE OUTLET POWER CB SH.080M+01F12/4*)
	FLT731	AT %MX1.4045.10 :	BOOL;(*CB OFF_GANTRY WATERSIDE OUTLET POWER CB SH.080M+01F12/4*)
	FLT732	AT %MX1.4045.11 :	BOOL;(*CB OFF_M-HOUSE EMERGENCY OUTLET POWER CB SH.080M+01F12/4*)
	FLT733	AT %MX1.4045.12 :	BOOL;(*CB OFF_E-ROOM EMERGENCY OUTLET POWER CB SH.080M+01F12/5*)
	FLT734	AT %MX1.4045.13 :	BOOL;(*CB OFF_BOOM TIP WELDING OUTLET POWER CB SH.080M+01F12/5*)
	FLT735	AT %MX1.4045.14 :	BOOL;(*CB OFF_CHECK ROOM 3P OUTLET POWER CB SH.080M+01F12/5*)
	FLT736	AT %MX1.4045.15 :	BOOL;(*CB OFF_M-HOUSE 3P OUTLET POWER CB SH.080M+01F12/5*)
	FLT737	AT %MX1.4046.0 :	BOOL;(*CB OFF_GANTRY LANDSIDE 3P OUTLET POWER CB SH.080M+01F12/5*)
	FLT738	AT %MX1.4046.1 :	BOOL;(*CB OFF_GANTRY WATERSIDE 3P OUTLET POWER CB SH.080M+01F12/5*)
	FLT739	AT %MX1.4046.2 :	BOOL;(*CB OFF_TRANSFORMER THERMAL CONTROL POWER CB SH.080M+01F12/5*)
	FLT740	AT %MX1.4046.3 :	BOOL;(*CB OFF_PANEL HEATER POWER CB SH.080M+01F12/6*)
	FLT741	AT %MX1.4046.4 :	BOOL;(*CB OFF_+01F12 PANEL CONTROL POWER CB SH.080M+01F12/6*)
	FLT742	AT %MX1.4046.5 :	BOOL;(*CB OFF_TRANSFORMER TEMP. CONTROL POWER CB SH.080M+01F12/6*)
	FLT743	AT %MX1.4046.6 :	BOOL;(*CB OFF_+01F12 OUTPUT MODULE POWER CB SH.080M+01F12/6*)
	FLT744	AT %MX1.4046.7 :	BOOL;(*CB OFF_TROLLEY FLOODLIGHT POWER CB#1 SH.080M+10F01/15*)
	FLT745	AT %MX1.4046.8 :	BOOL;(*CB OFF_TROLLEY FLOODLIGHT POWER CB#2 SH.080M+10F01/15*)
	FLT746	AT %MX1.4046.9 :	BOOL;(*CB OFF_TROLLEY WALKWAY LIGHT POWER CB SH.080M+10F01/15*)
	FLT747	AT %MX1.4046.10 :	BOOL;(*CB OFF_CABIN LIGHT POWER CB SH.080M+10F01/15*)
	FLT748	AT %MX1.4046.11 :	BOOL;(*CB OFF_CABIN PANEL LIGHT/OUTLET POWER CB SH.080M+10F01/15*)
	FLT749	AT %MX1.4046.12 :	BOOL;(*CB OFF_CABIN OUTLET POWER CB SH.080M+10F01/15*)
	FLT750	AT %MX1.4046.13 :	BOOL;(*CB OFF_CABIN PANEL HEATER POWER CB SH.080M+10F01/15*)
	FLT751	AT %MX1.4046.14 :	BOOL;(*CB OFF_CABIN AIR-CONDITIONING POWER CB SH.080M+10F01/15*)
	FLT752	AT %MX1.4046.15 :	BOOL;(*CB OFF_E-HOUSE AIR-CONDITIONING#1 POWER CB SH.081M+01F12/1*)
	FLT753	AT %MX1.4047.0 :	BOOL;(*CB OFF_E-HOUSE AIR-CONDITIONING#2 POWER CB SH.081M+01F12/1*)
	FLT754	AT %MX1.4047.1 :	BOOL;(*CB OFF_E-HOUSE AIR-CONDITIONING#3 POWER CB SH.081M+01F12/1*)
	FLT755	AT %MX1.4047.2 :	BOOL;(*CB OFF_E-HOUSE AIR-CONDITIONING#4 POWER CB SH.081M+01F12/1*)
	FLT756	AT %MX1.4047.3 :	BOOL;(*CB OFF_M-HOUSE FAN#1 POWER CB SH.081M+01F12/1*)
	FLT757	AT %MX1.4047.4 :	BOOL;(*CB OFF_M-HOUSE FAN#2 POWER CB SH.081M+01F12/1*)
	FLT758	AT %MX1.4047.5 :	BOOL;(*CB OFF_CHECK ROOM FAN POWER CB SH.081M+01F12/1*)
	FLT759	AT %MX1.4047.6 :	BOOL;(*CB OFF_E-HOUSE AIR-CONDITIONING#5 POWER CB SH.081M+01F12/2*)
	FLT760	AT %MX1.4047.7 :	BOOL;(*CB OFF_CHECK ROOM AIR-CONDITIONING POWER CB SH.081M+01F12/2*)
	FLT761	AT %MX1.4047.8 :	BOOL;(*CB OFF_+01F12 PANEL INSIDE I/O POWER CB SH.081M+01F12/2*)
	FLT762	AT %MX1.4047.9 :	BOOL;(*CB OFF_ROPE REEVING MOTOR#1 POWER CB SH.082M+01F12/1*)
	FLT763	AT %MX1.4047.10 :	BOOL;(*CB OFF_ROPE REEVING MOTOR#2 POWER CB SH.082M+01F12/1*)
	FLT764	AT %MX1.4047.11 :	BOOL;
	FLT765	AT %MX1.4047.12 :	BOOL;(*CB OFF_EMERGENCY BRAKE HYDRAULIC STATION PUMP CB SH.082M+01F12/1*)
	FLT766	AT %MX1.4047.13 :	BOOL;(*CB OFF_EMERGENCY BRAKE HYDRAULIC SOLENOID CB SH.082M+01F12/1*)
	FLT767	AT %MX1.4047.14 :	BOOL;(*CB OFF_ELEVATOR POWER CB SH.082M+01F12/2*)
	FLT768	AT %MX1.4047.15 :	BOOL;(*CB OFF_ELEVATOR CONTROL POWER CB SH.082M+01F12/2*)
	FLT769	AT %MX1.4048.0 :	BOOL;(*CB OFF_M-HOUSE SEVICE CRANE POWER CB SH.082M+01F12/2*)
	FLT770	AT %MX1.4048.1 :	BOOL;(*CB OFF_AIR-COMPRESS POWER CB SH.082M+01F12/2*)
	FLT771	AT %MX1.4048.2 :	BOOL;(*CB OFF_AIR-CRAFT UPS POWER CB SH.082M+01F12/2*)
	FLT772	AT %MX1.4048.3 :	BOOL;(*CB OFF_AIR-CRAFT POWER CB SH.082M+01F12/2*)
	FLT773	AT %MX1.4048.4 :	BOOL;(*CB OFF_CMS POWER CB SH.082M+01F12/2*)
	FLT774	AT %MX1.4048.5 :	BOOL;
	FLT775	AT %MX1.4048.6 :	BOOL;(*CB OFF_+01F12 PANEL OUTSIDE I/O POWER CB SH.082M+01F12/3*)
	FLT776	AT %MX1.4048.7 :	BOOL;(*CB OFF_CABIN POD/PT100 POWER CB SH.082M+10F01/20*)
	FLT777	AT %MX1.4048.8 :	BOOL;
	FLT778	AT %MX1.4048.9 :	BOOL;(*CB OFF_CONSOLE I/O POWER CB SH.100M+10S01/6*)
	FLT779	AT %MX1.4048.10 :	BOOL;
	FLT780	AT %MX1.4048.11 :	BOOL;
	FLT781	AT %MX1.4048.12 :	BOOL;
	FLT782	AT %MX1.4048.13 :	BOOL;
	FLT783	AT %MX1.4048.14 :	BOOL;
	FLT784	AT %MX1.4048.15 :	BOOL;
	FLT785	AT %MX1.4049.0 :	BOOL;
	FLT786	AT %MX1.4049.1 :	BOOL;
	FLT787	AT %MX1.4049.2 :	BOOL;
	FLT788	AT %MX1.4049.3 :	BOOL;
	FLT789	AT %MX1.4049.4 :	BOOL;
	FLT790	AT %MX1.4049.5 :	BOOL;
	FLT791	AT %MX1.4049.6 :	BOOL;
	FLT792	AT %MX1.4049.7 :	BOOL;
	FLT793	AT %MX1.4049.8 :	BOOL;
	FLT794	AT %MX1.4049.9 :	BOOL;
	FLT795	AT %MX1.4049.10 :	BOOL;
	FLT796	AT %MX1.4049.11 :	BOOL;
	FLT797	AT %MX1.4049.12 :	BOOL;
	FLT798	AT %MX1.4049.13 :	BOOL;
	FLT799	AT %MX1.4049.14 :	BOOL;
	FLT800	AT %MX1.4049.15 :	BOOL;
	FLT801	AT %MX1.4050.0 :	BOOL;(*SX bus communication not ok*)
	FLT802	AT %MX1.4050.1 :	BOOL;(*SX station communication abnormal at +01F41*)
	FLT803	AT %MX1.4050.2 :	BOOL;(*SX station communication abnormal at +10F01*)
	FLT804	AT %MX1.4050.3 :	BOOL;(*SX station communication abnormal at +10S01*)
	FLT805	AT %MX1.4050.4 :	BOOL;(*SX station communication abnormal at +10S02*)
	FLT806	AT %MX1.4050.5 :	BOOL;(*SX station communication abnormal at +06F01*)
	FLT807	AT %MX1.4050.6 :	BOOL;(*SX station communication abnormal at +01F12*)
	FLT808	AT %MX1.4050.7 :	BOOL;(*SX station communication abnormal at +01F13*)
	FLT809	AT %MX1.4050.8 :	BOOL;(*SX station communication abnormal at +01F15*)
	FLT810	AT %MX1.4050.9 :	BOOL;(*SX station communication abnormal at +01F11*)
	FLT811	AT %MX1.4050.10 :	BOOL;(*SX station communication abnormal at RHC#1*)
	FLT812	AT %MX1.4050.11 :	BOOL;(*SX station communication abnormal at RHC#2*)
	FLT813	AT %MX1.4050.12 :	BOOL;(*SX station communication abnormal at Hoist#1 inverter*)
	FLT814	AT %MX1.4050.13 :	BOOL;(*SX station communication abnormal at Hoist#2 inverter*)
	FLT815	AT %MX1.4050.14 :	BOOL;(*SX station communication abnormal at Trolley/Boom inverter*)
	FLT816	AT %MX1.4050.15 :	BOOL;(*SX station communication abnormal at Gantry inverter*)
	FLT817	AT %MX1.4051.0 :	BOOL;(*SX station communication abnormal at TLS#1 inverter*)
	FLT818	AT %MX1.4051.1 :	BOOL;(*SX station communication abnormal at TLS#2 inverter*)
	FLT819	AT %MX1.4051.2 :	BOOL;(*SX station communication abnormal at TLS#3 inverter*)
	FLT820	AT %MX1.4051.3 :	BOOL;(*SX station communication abnormal at TLS#4 inverter*)
	FLT821	AT %MX1.4051.4 :	BOOL;(*SX station DP communication fault at boom absolute encoder*)
	FLT822	AT %MX1.4051.5 :	BOOL;(*SX station DP communication fault at hoist absolute encoder*)
	FLT823	AT %MX1.4051.6 :	BOOL;(*SX station DP communication fault at GCR interface*)
	FLT824	AT %MX1.4051.7 :	BOOL;(*SX station DP communication fault at TLS#1 absolute encoder*)
	FLT825	AT %MX1.4051.8 :	BOOL;(*SX station DP communication fault at TLS#2 absolute encoder*)
	FLT826	AT %MX1.4051.9 :	BOOL;(*SX station DP communication fault at TLS#3 absolute encoder*)
	FLT827	AT %MX1.4051.10 :	BOOL;(*SX station DP communication fault at TLS#4 absolute encoder*)
	FLT828	AT %MX1.4051.11 :	BOOL;
	FLT829	AT %MX1.4051.12 :	BOOL;
	FLT830	AT %MX1.4051.13 :	BOOL;
	FLT831	AT %MX1.4051.14 :	BOOL;
	FLT832	AT %MX1.4051.15 :	BOOL;
	FLT833	AT %MX1.4052.0 :	BOOL;
	FLT834	AT %MX1.4052.1 :	BOOL;
	FLT835	AT %MX1.4052.2 :	BOOL;
	FLT836	AT %MX1.4052.3 :	BOOL;
	FLT837	AT %MX1.4052.4 :	BOOL;
	FLT838	AT %MX1.4052.5 :	BOOL;
	FLT839	AT %MX1.4052.6 :	BOOL;
	FLT840	AT %MX1.4052.7 :	BOOL;
	FLT841	AT %MX1.4052.8 :	BOOL;
	FLT842	AT %MX1.4052.9 :	BOOL;
	FLT843	AT %MX1.4052.10 :	BOOL;
	FLT844	AT %MX1.4052.11 :	BOOL;
	FLT845	AT %MX1.4052.12 :	BOOL;
	FLT846	AT %MX1.4052.13 :	BOOL;
	FLT847	AT %MX1.4052.14 :	BOOL;
	FLT848	AT %MX1.4052.15 :	BOOL;
	FLT849	AT %MX1.4053.0 :	BOOL;
	FLT850	AT %MX1.4053.1 :	BOOL;
	FLT851	AT %MX1.4053.2 :	BOOL;
	FLT852	AT %MX1.4053.3 :	BOOL;
	FLT853	AT %MX1.4053.4 :	BOOL;
	FLT854	AT %MX1.4053.5 :	BOOL;
	FLT855	AT %MX1.4053.6 :	BOOL;
	FLT856	AT %MX1.4053.7 :	BOOL;
	FLT857	AT %MX1.4053.8 :	BOOL;
	FLT858	AT %MX1.4053.9 :	BOOL;
	FLT859	AT %MX1.4053.10 :	BOOL;
	FLT860	AT %MX1.4053.11 :	BOOL;
	FLT861	AT %MX1.4053.12 :	BOOL;
	FLT862	AT %MX1.4053.13 :	BOOL;
	FLT863	AT %MX1.4053.14 :	BOOL;
	FLT864	AT %MX1.4053.15 :	BOOL;
	FLT865	AT %MX1.4054.0 :	BOOL;
	FLT866	AT %MX1.4054.1 :	BOOL;
	FLT867	AT %MX1.4054.2 :	BOOL;
	FLT868	AT %MX1.4054.3 :	BOOL;
	FLT869	AT %MX1.4054.4 :	BOOL;
	FLT870	AT %MX1.4054.5 :	BOOL;
	FLT871	AT %MX1.4054.6 :	BOOL;
	FLT872	AT %MX1.4054.7 :	BOOL;
	FLT873	AT %MX1.4054.8 :	BOOL;
	FLT874	AT %MX1.4054.9 :	BOOL;
	FLT875	AT %MX1.4054.10 :	BOOL;
	FLT876	AT %MX1.4054.11 :	BOOL;
	FLT877	AT %MX1.4054.12 :	BOOL;
	FLT878	AT %MX1.4054.13 :	BOOL;
	FLT879	AT %MX1.4054.14 :	BOOL;
	FLT880	AT %MX1.4054.15 :	BOOL;
	FLT881	AT %MX1.4055.0 :	BOOL;
	FLT882	AT %MX1.4055.1 :	BOOL;
	FLT883	AT %MX1.4055.2 :	BOOL;
	FLT884	AT %MX1.4055.3 :	BOOL;
	FLT885	AT %MX1.4055.4 :	BOOL;
	FLT886	AT %MX1.4055.5 :	BOOL;
	FLT887	AT %MX1.4055.6 :	BOOL;
	FLT888	AT %MX1.4055.7 :	BOOL;
	FLT889	AT %MX1.4055.8 :	BOOL;
	FLT890	AT %MX1.4055.9 :	BOOL;
	FLT891	AT %MX1.4055.10 :	BOOL;
	FLT892	AT %MX1.4055.11 :	BOOL;
	FLT893	AT %MX1.4055.12 :	BOOL;
	FLT894	AT %MX1.4055.13 :	BOOL;
	FLT895	AT %MX1.4055.14 :	BOOL;
	FLT896	AT %MX1.4055.15 :	BOOL;
	FLT897	AT %MX1.4056.0 :	BOOL;
	FLT898	AT %MX1.4056.1 :	BOOL;
	FLT899	AT %MX1.4056.2 :	BOOL;
	FLT900	AT %MX1.4056.3 :	BOOL;
	DRVFLT_CMS001	AT %MW1.4100 :	WORD;
	DRVFLT_CMS002	AT %MW1.4101 :	WORD;
	DRVFLT_CMS003	AT %MW1.4102 :	WORD;(*Hoist inv#1 fault code*)
	DRVFLT_CMS004	AT %MW1.4103 :	WORD;(*Hoist inv#2 fault code*)
	DRVFLT_CMS005	AT %MW1.4104 :	WORD;(*Trolley/boom inv fault code for trolley*)
	DRVFLT_CMS006	AT %MW1.4105 :	WORD;(*Trolley/boom inv fault code for boom*)
	DRVFLT_CMS007	AT %MW1.4106 :	WORD;(*Gantry inv fault code*)
	DRVFLT_CMS008	AT %MW1.4107 :	WORD;(*TLS#1 inv fault code*)
	DRVFLT_CMS009	AT %MW1.4108 :	WORD;(*TLS#2 inv fault code*)
	DRVFLT_CMS010	AT %MW1.4109 :	WORD;(*TLS#3 inv fault code*)
	DRVFLT_CMS011	AT %MW1.4110 :	WORD;(*TLS#4 inv fault code*)
END_VAR

