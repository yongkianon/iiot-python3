
(*Group:Loadcell_Variables*)


VAR_GLOBAL
	LCR1 :	REAL;(*LOADCELL#1 REAL VALUE*)
	LCR2 :	REAL;(*LOADCELL#2 REAL VALUE*)
	LCR3 :	REAL;(*LOADCELL#3 REAL VALUE*)
	LCR4 :	REAL;(*LOADCELL#4 REAL VALUE*)
	LCLFT :	REAL;(*LEFT SIDE LOAD*)
	LCRHT :	REAL;(*RIGHT SIDE LOAD*)
	LOAD :	REAL;(*GROSS LOAD*)
	LCDIF :	REAL;(*DIFFERENCE LOAD*)
	LC1WBFLT :	BOOL;(*LOADCELL#1 WIRE BREAK FAULT*)
	LC2WBFLT :	BOOL;(*LOADCELL#2 WIRE BREAK FAULT*)
	LC3WBFLT :	BOOL;(*LOADCELL#3 WIRE BREAK FAULT*)
	LC4WBFLT :	BOOL;(*LOADCELL#4 WIRE BREAK FAULT*)
	LCWBFLT :	BOOL;(*LOADCELL WIRE BREAK FAULT*)
	DEADWEG :	REAL;(*DEAD WEIGHT*)
	WEG :	REAL;(*LOAD WEIGHT*)
	WEGRATED :	REAL;(*LOAD WEIGHT RATED*)
	WEGLIMIT :	REAL;(*LOAD WEIGHT LIMIT*)
	OVLDALM :	BOOL;(*HOIST OVERLOAD ALARM*)
	OVLDFLT :	BOOL;(*HOIST OVERLOAD FAULT*)
	OVLDFLT2 :	BOOL;(*HOIST OVERLOAD FAULT LEVEL2*)
	ECLDFLT :	BOOL;(*HOIST ECCENTRIC LOAD FAULT*)
	SNGTRIP :	BOOL;(*SNAG TRIP BY LOADCELL*)
	SPRSLK :	BOOL;(*SPREADER ROPE SLACK BY LOADCELL*)
	V132 :	BOOL;
	V145 :	BOOL;
	V146 :	BOOL;
	V147 :	BOOL;
END_VAR

