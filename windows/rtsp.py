import os
import time

import cv2


def back(*args):
    pass


def info(s):
    print(time.strftime("%Y-%m-%d %H:%M:%S INFO ") + str(s))


while True:
    try:
        input = "rtsp://root:pass@192.168.0.90/axis-media/media.amp"
        input = "rtsp://root:Admin123@192.168.0.91/MediaInput/h264/stream_2"
        input = 0  # default camera
        info(input)
        vs = cv2.VideoCapture(input)
        width = int(vs.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vs.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(vs.get(cv2.CAP_PROP_FPS))  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
        info("FPS " + str(fps) + " (" + str(width) + ", " + str(height) + ")")
        cv2.namedWindow("AZ", cv2.WINDOW_FULLSCREEN)
        cv2.moveWindow("AZ", 0, 0)
        while vs.isOpened():
            ret, frame = vs.read()
            cv2.imshow("AZ", frame)
            # cv2.createButton("Back", back, None, cv2.QT_PUSH_BUTTON, 1)  # NOK
            key = cv2.waitKey(20) & 0xFF
            if key == ord(str(chr(27))) or key == ord("q") or key == ord("x"):  # ESC|q|x
                os._exit(0)  # break
        vs.release()
    except Exception as e:
        info("Error " + str(e))
    finally:
        cv2.destroyAllWindows()
