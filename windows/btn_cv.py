import cv2
import numpy as np

# button dimensions (y1, y2, x1, x2)
btn1 = [10, 70, 30, 370]
btn2 = [120, 70, 30, 250]


def do_click_btn01(event, x, y, flags, params):
    # check if the click is within the dimensions of the button
    if event == cv2.EVENT_LBUTTONDOWN:
        # print("(" + str(x) + ", " + str(y) + ")")
        if btn1[0] < y < btn1[1] and btn1[2] < x < 180:
            print("BTN01 (" + str(x) + ", " + str(y) + ")")
        elif btn1[0] < y < btn1[1] and 180 < x < btn1[3]:
            print("BTN02 (" + str(x) + ", " + str(y) + ")")


def start_capture(val):
    # check if the value of the slider
    if val == 1:
        print("Capture started!")
    else:
        print("Capture stopped!")


cv2.namedWindow("Control")
cv2.setMouseCallback("Control", do_click_btn01)
cv2.createTrackbar("Capture", "Control", 0, 1, start_capture)

# create image 80 x 400
img01 = np.zeros((80, 400), np.uint8)
img01[btn1[0]:btn1[1], btn1[2]:btn1[3]] = 180

# img02 = np.zeros((200, 300), np.uint8)
# img02[btn2[0]:btn2[1], btn2[2]:btn2[3]] = 100

cv2.putText(img01, "Btn01", (50, 50), cv2.FONT_HERSHEY_PLAIN, 2, 0, 3)
cv2.putText(img01, "Btn02", (250, 50), cv2.FONT_HERSHEY_PLAIN, 2, 0, 3)

cv2.imshow("Control", img01)
cv2.waitKey(0)
cv2.destroyAllWindows()
