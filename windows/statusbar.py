import time

from tkinter import *


def update():
    txt.delete(1.0, END)
    txt.insert(INSERT, dat.get())
    dat.delete(0, END)
    dat.insert(INSERT, time.strftime("%Y-%m-%d %H:%M:%S"))


app = Tk()

# create needed widgets
lbl = Label(app, text="Time")
dat = Entry(app, width=25)
btn = Button(app, text="Update", command=update)
txt = Text(app, width=25, height=1)

# place the widgets in a grid
lbl.grid(row=1, column=1)
dat.grid(row=1, column=2)
btn.grid(row=1, column=3)
txt.grid(row=2, column=1)

# put the cursor into entry field
dat.insert(INSERT, time.strftime("%Y-%m-%d %H:%M:%S"))
dat.focus()
app.mainloop()
