import time

import cv2

# vs = cv2.VideoCapture("rtsp://root:Admin123@192.168.0.90/MediaInput/h264/stream_1")
vs = cv2.VideoCapture("rtsp://root:pass@192.168.0.90/axis-media/media.amp")
width = int(vs.get(cv2.CAP_PROP_FRAME_WIDTH) + 0.5)
height = int(vs.get(cv2.CAP_PROP_FRAME_HEIGHT) + 0.5)
fourcc = cv2.VideoWriter_fourcc(*"XVID")
out = cv2.VideoWriter(time.strftime("%Y.%m.%d_%H%M") + ".avi", fourcc, 20.0, (width, height))

while True:
    _, frame = vs.read()
    cv2.imshow("AZ", frame)
    out.write(frame)
    if cv2.waitKey(10) & 0xFF == ord("q"):
        break

vs.release()
out.release()
cv2.destroyAllWindows()
