import argparse

import cv2

refPt = []
cropping = False


def click_and_crop(event, x, y, flags, param):
    global refPt, cropping
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True
    # elif cropping and event == cv2.EVENT_MOUSEMOVE:
    #     refPt.append((x, y))
    #     # draw a rectangle around the region of interest
    #     cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
    #     cv2.imshow("image", image)
    elif event == cv2.EVENT_LBUTTONUP:
        # record the end (x, y) and indicate that the cropping operation is finished
        refPt.append((x, y))
        cropping = False
        # draw a rectangle around the region of interest
        cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
        cv2.imshow("image", image)


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
# load the image, clone it, and setup the mouse callback function
image = cv2.imread(args["image"])
clone = image.copy()
cv2.namedWindow("image")
cv2.setMouseCallback("image", click_and_crop)
while True:
    cv2.imshow("image", image)
    key = cv2.waitKey(20) & 0xFF
    if key == ord("r"):  # reset the cropping region
        image = clone.copy()
    elif key == ord("c"):
        break

# crop the region of interest from the image and display it
if len(refPt) == 2:
    roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
    cv2.imshow("ROI", roi)
    cv2.waitKey(0)

cv2.destroyAllWindows()
