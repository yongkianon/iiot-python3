import argparse
import datetime
import os
import threading
import time
import tkinter as tki

import cv2
import imutils
import requests
from PIL import Image
from PIL import ImageTk
from requests.auth import HTTPDigestAuth


def info(s):
    print(time.strftime("%Y-%m-%d %H:%M:%S INFO ") + s)


def conn():
    # return VideoStream(usePiCamera=args["camera"] > 0).start()
    # return cv2.VideoCapture("rtsp://root:pass@192.168.0.90/axis-media/media.amp")
    return cv2.VideoCapture("rtsp://root:Admin123@192.168.0.90/MediaInput/h264/stream_1")


def http_get(url):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
    }
    res = requests.get(url, auth=HTTPDigestAuth("root", "Admin123"), headers=headers)
    info(url + " " + str(res.status_code))


def start_thread(url):
    t = threading.Thread(target=http_get, args=(url,))
    t.start()


def live_check():
    print(".", end="")
    start_thread("http://192.168.0.90/js/capi.js?Language=0&Rnd=" + str(int(time.time())))


class PhotoBoothApp:
    loop = 1

    def frame_properties(self):
        width = self.vs.get(cv2.CAP_PROP_FRAME_WIDTH)  # float `width`
        height = self.vs.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height`
        count = self.vs.get(cv2.CAP_PROP_FRAME_COUNT)
        fps = self.vs.get(cv2.CAP_PROP_FPS)
        print("W=" + str(width) + ", H=" + str(height) + ", FPS=" + str(fps) + ", count=" + str(count))

    def __init__(self, vs, out_path):
        # store the video stream object and output path, then initialize
        # the most recently read frame, thread for reading frames, and
        # the thread stop event
        self.vs = vs
        self.out_path = out_path
        self.frame = None
        self.thread = None
        self.stopEvent = None
        # initialize the root window and image panel
        self.root = tki.Tk()
        self.panel = tki.Label()

        # create a button, that when pressed, will take the current frame and save it to file
        btn = tki.Button(self.root, text="Snapshot!", command=self.take_snapshot)

        btn_ul = tki.Button(self.root, text="01 UL", command=self.go_ul)
        btn_up = tki.Button(self.root, text="02 UP", command=self.go_up)
        btn_ur = tki.Button(self.root, text="03 UR", command=self.go_ur)

        btn_left = tki.Button(self.root, text="04 LEFT", command=self.go_left)
        btn_right = tki.Button(self.root, text="05 RIGHT", command=self.go_right)

        btn_bl = tki.Button(self.root, text="06 BL", command=self.go_bl)
        btn_down = tki.Button(self.root, text="07 DOWN", command=self.go_down)
        btn_br = tki.Button(self.root, text="08 BR", command=self.go_br)

        self.panel.grid(row=1, column=1)
        btn.grid(row=2, column=2)

        btn_ul.grid(row=3, column=1)
        btn_up.grid(row=3, column=2)
        btn_ur.grid(row=3, column=3)

        btn_left.grid(row=4, column=1)
        btn_right.grid(row=4, column=3)

        btn_bl.grid(row=5, column=1)
        btn_down.grid(row=5, column=2)
        btn_br.grid(row=5, column=3)

        # start a thread that constantly pools the video sensor for the most recently read frame
        self.stopEvent = threading.Event()
        self.thread = threading.Thread(target=self.video_looping, args=())
        self.thread.start()
        # set a callback to handle when the window is closed
        self.root.wm_title("PhotoBooth")
        self.root.wm_protocol("WM_DELETE_WINDOW", self.on_close)

    def video_looping(self):
        try:
            # keep looping over frames until we are instructed to stop
            while not self.stopEvent.is_set():
                # grab the frame from the video stream and resize to max width of 300 pixels
                ret, frame = self.vs.read()
                if frame is None:
                    info("frame is None. retry read again")
                    time.sleep(1)
                    self.vs.release()
                    self.vs = conn()
                    continue
                self.frame = frame
                self.frame = imutils.resize(self.frame)
                if self.loop == 1:
                    self.frame_properties()
                    self.loop = 9

                # OpenCV represents images in BGR order; however PIL
                # represents images in RGB order, so we need to swap
                # the channels, then convert to PIL and ImageTk format
                image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                image = Image.fromarray(image)
                image = ImageTk.PhotoImage(image)

                # if the panel is not None, we need to initialize it
                if self.panel is None:
                    self.panel = tki.Label(image=image)
                    self.panel.image = image
                    self.panel.pack(side="left", padx=10, pady=10)

                # otherwise, simply update the panel
                else:
                    self.panel.configure(image=image)
                    self.panel.image = image
                # live_check()
        except RuntimeError as e:
            info("RuntimeError " + str(e))

    def take_snapshot(self):
        # grab the current timestamp and use it to construct the output path
        ts = datetime.datetime.now()
        filename = "{}.jpg".format(ts.strftime("%Y-%m-%d_%H_%M_%S"))
        p = os.path.sep.join((self.out_path, filename))
        # save the file
        cv2.imwrite(p, self.frame.copy())
        info("saved {}".format(filename))

    def go_ul(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=-1&tilt=-1&Language=0")

    def go_up(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=0&tilt=-1&Language=0")

    def go_ur(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=1&tilt=-1&Language=0")

    def go_left(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=-1&tilt=0&Language=0")

    def go_right(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=1&tilt=0&Language=0")

    def go_bl(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=-1&tilt=1&Language=0")

    def go_down(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=0&tilt=1&Language=0")

    def go_br(self):
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=1&tilt=1&Language=0")

    def on_close(self):
        info("closing...")
        self.stopEvent.set()
        self.vs.release()
        cv2.destroyAllWindows()
        self.root.quit()
        os._exit(0)


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", required=True, help="path to directory to store snapshots")
ap.add_argument("-p", "--camera", type=int, default=-1, help="whether or not the Raspberry Pi camera should be used")
args = vars(ap.parse_args())
# initialize the video stream and allow the camera sensor to warmup
info("starting...")
time.sleep(2)
# start the app
app = PhotoBoothApp(conn(), args["output"])
app.root.mainloop()
