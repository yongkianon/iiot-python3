def isfloat(num):
    try:
        float(num)
        return True
    except Exception as e:
        return False


def over_flow(value):
    # 2^32 === 4294967296
    value = float(value)
    if value > 4200000000:
        return value - 4294967296
    else:
        return value


def test_case(value):
    if isfloat(value):
        print(str(value) + str("    ") + str(over_flow(value)))
    else:
        print(str(value) + " NaN")


if __name__ == "__main__":
    test_case(4294967200)
    test_case(4294967296)
    test_case(4294967260)
    test_case("4294967275")
    test_case(4294967275)
    test_case(7171)
    test_case("ABC")
    test_case("")
    test_case(True)
    test_case(None)
