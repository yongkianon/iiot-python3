import libTimeOut


def main():
    count = -999
    try:
        count = libTimeOut.test()
        print("RETURN", count)
    except Exception as e:
        print("ERROR", count, e)


if __name__ == '__main__':
    main()
