import matplotlib.pyplot as g
import numpy as np
from scipy import pi
from scipy.fftpack import fft


def debug(o):
    print(o)


sample_rate = 1024
N = 2 * sample_rate
time = np.linspace(0, 2, N)

freq1 = 100
magnitude1 = 25
freq2 = 200
magnitude2 = 15

waveform1 = magnitude1 * np.sin(2 * pi * freq1 * time)
waveform2 = magnitude2 * np.sin(2 * pi * freq2 * time)

noise = np.random.normal(0, 10, N)

time_data = waveform1 + waveform2 + noise

# g.plot(time[0:30], waveform2[0:30])
g.plot(time[0:100], time_data[0:100])
g.title("Time Domain Signal")
g.xlabel("Time")
g.ylabel("Amplitude")
g.show()

frequency = np.linspace(0.0, 512, int(N / 2))

freq_data = fft(time_data)
y = 2 / N * np.abs(freq_data[0:np.int(N / 2)])

g.plot(frequency, y)
g.title("Frequency Domain Signal")
g.xlabel("Frequency (Hz)")
g.ylabel("Amplitude")
g.show()
