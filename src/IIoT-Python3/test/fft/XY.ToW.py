import matplotlib.pyplot as g
import mysql.connector
import numpy as np
from scipy.fftpack import fft


def debug(o):
    print(o)


sql = '''
SELECT STR_TO_DATE(DATE_FORMAT(ts_occurred, "%Y-%m-%d %H:00:00"), '%Y-%m-%d %T') AS HH
, @ROW:=@ROW+1 AS count
-- , DeviceId
, round(AVG(Temperature),2)       AS TEMP
, round(AVG(sqrt(acc_X*acc_X)),4) AS X
, round(AVG(sqrt(acc_Y*acc_Y)),4) AS Y
, round(AVG(sqrt(acc_Z*acc_Z)),4) AS Z
FROM pdm_tow_device WHERE DeviceId=545243000002
AND ts_occurred BETWEEN '2020-08-10 00:00:00' AND '2020-08-10 23:59:59'
GROUP BY STR_TO_DATE(DATE_FORMAT(ts_occurred, "%Y-%m-%d %H:00:00"), '%Y-%m-%d %T')
ORDER BY 1
'''

debug(sql)

mysql = mysql.connector.connect(host="localhost", user="root", passwd="P@ssw0rd", database="test")
cursor = mysql.cursor()
cursor.execute("SET @ROW=0")
cursor.execute(sql)
rs = cursor.fetchall()

TSs = []
RMs = []
for x in rs:
    (dt, cnt, temp, rmsX, rmsY, rmsZ) = x
    TSs.append(cnt)
    RMs.append(rmsZ)
cursor.close()
debug("PLOT rmsZ")

g.plot(TSs, RMs)
g.title("RMS")
g.show()

if len(RMs) == 0:
    debug("RMS no data")
    exit()

test = np.real(fft(RMs))
values = []
for t in test:
    values.append(np.abs(round(t / 1000)))

g.plot(TSs, values)
g.title("FFT")
g.show()
