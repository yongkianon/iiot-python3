import matplotlib.pyplot as g
import mysql.connector
import numpy as np
from scipy.fftpack import fft


def debug(o):
    print(o)


sql = '''
SELECT @ROW:=@ROW+1 AS count, UNIX_TIMESTAMP(ts_occurred) - 1577808080 AS t, ts_occurred AS dt, RMSSample AS rms
FROM pdm_xperanti_device
WHERE RMSSample IS NOT NULL
AND ts_occurred BETWEEN '2020-01-01' AND '2020-01-31'
AND CraneName='Z6'
ORDER BY ts_occurred
'''

debug(sql)

mysql = mysql.connector.connect(host="localhost", user="root", passwd="P@ssw0rd", database="test")
cursor = mysql.cursor()
cursor.execute("SET @ROW=0")
cursor.execute(sql)
rs = cursor.fetchall()

TSs = []
RMs = []
for x in rs:
    (c, t, dt, rms) = x
    TSs.append(t)
    RMs.append(rms)
cursor.close()

g.plot(TSs, RMs)
g.title("RMS")
g.show()

test = np.real(fft(RMs))
values = []
for t in test:
    if t < 42000:
        values.append(np.abs(round(t / 1000)))
    else:
        values.append(0)
g.plot(TSs, values)
g.title("FFT")
g.show()
