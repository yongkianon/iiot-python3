import sys
import time

import serial

comId = "COM3"
line = ""


def println(o):
    # print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ") + str(o))
    print(time.strftime("%Y-%m-%d %H:%M:%S ") + str(o))


if len(sys.argv) == 2:
    comId = sys.argv[1]

try:
    com = serial.Serial(timeout=3, port=comId, baudrate=9600, bytesize=serial.SEVENBITS)
    # com=serial.Serial(timeout=3, port=comId, baudrate=9600, bytesize=serial.SEVENBITS, parity=serial.PARITY_ODD, stopbits=serial.STOPBITS_TWO)
    println(com)
    com.write(b"HELLO\n")
    line = com.readline()
    if "" != line:
        println(line)
    com.write("X\n")
    println(com.readline())
    com.close()
    println(str(com))
except Exception as e:
    println("ERROR " + str(e))
