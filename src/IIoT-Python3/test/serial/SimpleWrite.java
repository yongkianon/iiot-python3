import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.OutputStream;
import java.util.Enumeration;

public class SimpleWrite {
    static String msg = "Hello, world!\n";

    public static void main(String[] args) {
        CommPortIdentifier portId = null;
        boolean found = false;
        String defaultPort = "COM10";

        if (args.length == 1) defaultPort = args[0];

        Enumeration portIds = CommPortIdentifier.getPortIdentifiers();
        while (portIds.hasMoreElements()) {
            portId = (CommPortIdentifier) portIds.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(defaultPort)) {
                    println("Found " + defaultPort);
                    found = true;

                    try {
                        SerialPort comm = (SerialPort) portId.open("SimpleWriteApp", 2000);
                        OutputStream os = comm.getOutputStream();
                        comm.setSerialPortParams(9600, SerialPort.DATABITS_7, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                        comm.notifyOnOutputEmpty(true);
                        println("Writing <" + msg + "> to " + portId.getName());
                        os.write(msg.getBytes());
                        //Thread.sleep(2000);  // Be sure data transferred before closing
                        os.write("X\n".getBytes());
                        comm.close();
                    } catch (Exception e) {
                        println(e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        }

        if (!found) {
            println("port " + defaultPort + " not found.");
        }
    }

    private static void println(Object o) {
        System.out.println(o);
    }
}
