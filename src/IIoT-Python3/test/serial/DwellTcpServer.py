import socket
import time

BUZZ = bytes(b'\x00\x01\xff\xff')
CID1 = bytes(b'\x01\xff\xff\xff')


def println(o):
    localtime = time.asctime(time.localtime(time.time()))
    print(localtime + " " + str(o))


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 50000))
s.listen(5)
println("Listen on port 50000")
client, address = s.accept()
client.settimeout(1)
client.send(BUZZ)
client.send(CID1)
res = client.recv(10).hex()
println("reader " + str(res) + " " + str(address))

while True:
    try:
        res = client.recv(10).hex()  # timeout 1 second
        client.send(BUZZ)
        hex_data = res[12:20]
        no = int(hex_data, 16)
        println("LAN CARD " + hex_data + " " + str(no))
    except Exception as e:
        client.send(CID1)
        client.recv(10)
