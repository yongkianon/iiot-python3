import atexit
import configparser
import json
import logging
import os
import re
import socket
import sys
import threading
import time

import requests

BUZZ = bytes(b'\x00\x01\xff\xff')
CID1 = bytes(b'\x01\xff\xff\xff')


class ScanCard:
    name = ""
    imei = ""
    card = ""


def println(o):
    # print(time.strftime("%Y-%m-%d %H:%M:%S ") + str(o))
    logging.info(str(o))


def delete_old_logs(days):
    path = "/opt/reader/logs"
    current_time = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < current_time - (days * 86400):
            os.remove(os.path.join(path, f))
            println("DELETED" + str(f))


def start_logger():
    filename = "/opt/reader/logs/" + time.strftime('%Y-%m-%d') + ".log"
    logging.basicConfig(filename=filename, filemode="a", format="%(asctime)s %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")
    delete_old_logs(365)


def shutdown_hook():
    println("TERMINATE " + str(sys.argv[1]).upper())


def numbers(s):
    rv = re.search(r"\d+", s)
    if rv is not None:
        return int(rv.group(0))


def http_post(url, msg):
    headers = {
        "Accept": "application/json",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
    }
    response = requests.post(url, headers=headers, data=msg)
    println("POST " + url)
    return str(response.status_code)


def scan_card(card, name):
    sc = ScanCard()
    sc.card = card
    sc.imei = ""
    sc.name = name
    msg = json.dumps(sc.__dict__)
    status = "TEST"
    # status = http_post("http://127.0.0.1:8080/rest/card/reader/scancard/post", msg)
    # status = http_post("http://172.21.77.19/rest/card/reader/scancard/post", msg)
    # status = http_post("http://172.21.95.190:8081/apps/pages/ttvmt.cardreader", msg)
    status = http_post("http://172.21.30.72/apps/pages/ttvmt.cardreader", msg)
    println("HTTP " + status + " " + msg + "\n")


def accept_port(client, rtg):
    while True:
        try:
            res = client.recv(10).hex()
            client.send(BUZZ)
            hex_data = res[12:20]
            no = int(hex_data, 16)
            println(rtg + " " + hex_data + " " + str(no))
            # scan_card(no, rtg)
        except Exception as e:
            client.send(CID1)
            client.recv(10)


def start_thread(client, rtg):
    try:
        t = threading.Thread(target=accept_port, args=(client, rtg,))
        t.start()
    except Exception as e:
        println("ERROR " + str(e))


def listen_port(port, name):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('', port))
    server.listen(5)
    server.settimeout(60)
    println(name + " listen on port " + str(port))
    while True:
        try:
            client, address = server.accept()
            client.settimeout(60)
            client.send(BUZZ)
            client.send(CID1)
            res = client.recv(10).hex()
            rtg = str(res[9:12])
            if rtg != "FFF":
                rtg = name.replace("XXX", rtg)
            println("Connected reader " + str(res) + " " + str(address) + " from " + rtg)
            start_thread(client, rtg)
        except Exception as e:
            # println("ERROR server " + str(e))
            time.sleep(10)


def main(name):
    # python scancard.py TEST card_no
    if len(sys.argv) == 3 and "TEST" == str(sys.argv[1]).upper():
        println(sys.argv[1] + " " + sys.argv[2])
        scan_card(sys.argv[2], "TEST")
        os._exit(0)

    listen_port(50000, name)


if __name__ == "__main__":
    atexit.register(shutdown_hook)
    start_logger()
    config = configparser.ConfigParser()
    config.read(r"/opt/reader/config/config.ini")
    name = config.get("name", "name.id")
    main(name)
