import atexit
import configparser
import json
import logging
import os
import socket
import sys
import threading
import time

import requests

T_00 = bytes(b'\x00')
T_01 = bytes(b'\x01')
T_03 = bytes(b'\x03')
B_OK = bytes(b'\x02\x30\x00\x00\x13\x00\x17\x01') + T_03 + "   SCAN  CARD   ".encode("UTF-8")
B_FL = bytes(b'\x02\x30\x00\x00\x13\x00\x17\x02') + T_01 + "PLEASE TRY AGAIN".encode("UTF-8")
B_AK = bytes(b'\x02\x30\x00\x00\x13\x00\x17\x02') + T_00 + "                ".encode("UTF-8")

global server
global shutdown
last_scan = 0


class ScanCard:
    name = ""
    imei = ""
    card = ""


def ack():
    bcc = 0
    for b in B_AK:
        bcc ^= b
    return B_AK + bytes([bcc]) + bytes(b'\x03')


def ok():
    bcc = 0
    for b in B_OK:
        bcc ^= b
    return B_OK + bytes([bcc]) + bytes(b'\x03')


def fail():
    bcc = 0
    for b in B_FL:
        bcc ^= b
    return B_FL + bytes([bcc]) + bytes(b'\x03')


def info(o):
    logging.info(str(o))
    print(time.strftime("%Y-%m-%d %H:%M:%S ") + str(o))


def delete_old_logs(days):
    path = "/opt/reader/logs"
    current_time = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < current_time - (days * 86400):
            os.remove(os.path.join(path, f))
            info("DELETED" + str(f))


def start_logger():
    filename = "/opt/reader/logs/" + time.strftime('%Y-%m-%d') + ".log"
    logging.basicConfig(filename=filename, filemode="a", format="%(asctime)s %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")
    delete_old_logs(365)


def http_post(url, msg):
    headers = {
        "Accept": "application/json",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
    }
    response = requests.post(url, headers=headers, data=msg)
    info("POST " + url)
    return str(response.status_code)


def scan_card(card, name):
    sc = ScanCard()
    sc.card = card
    sc.imei = ""
    sc.name = name
    msg = json.dumps(sc.__dict__)
    status = "TEST"
    # status = http_post("http://127.0.0.1:8080/rest/card/reader/scancard/post", msg)
    # status = http_post("http://172.21.77.19/rest/card/reader/scancard/post", msg)
    # status = http_post("http://172.21.95.190:8081/apps/pages/ttvmt.cardreader", msg)
    # status = http_post("http://172.21.30.72/apps/pages/ttvmt.cardreader", msg)
    info("HTTP " + status + " " + msg + "\n")
    global last_scan
    last_scan = time.time()


def shutdown_hook():
    global server
    global shutdown
    shutdown = True
    info("Server close")
    if shutdown:
        server.close()


def main():
    start_logger()
    atexit.register(shutdown_hook)
    config = configparser.ConfigParser()
    config.read(r"/opt/reader/config/config.ini")
    name = config.get("name", "name.id")
    global server
    global shutdown
    shutdown = False
    port = 50000
    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('', port))
    server.listen(5)
    server.settimeout(10)
    info(name + " listen on port " + str(port))
    while not shutdown:
        try:
            client, address = server.accept()
            client.settimeout(10)
            info("reader " + str(address))
            while not shutdown:
                try:
                    res = client.recv(32).hex()  # timeout 60 second
                    client.send(ok())
                    hex_data = res[0:32]
                    info(str(hex_data) + " " + str(len(hex_data)))
                    if len(hex_data) == 32:
                        chn_id = int(hex_data[4:6], 16)
                        rdr_id = int(hex_data[6:8], 16)
                        hex_no = hex_data[24:26] + hex_data[22:24] + hex_data[20:22] + hex_data[18:20]
                        no = str(int(hex_no, 16))
                        info(str(chn_id) + str(rdr_id) + " CARD " + hex_no + " " + no)
                        # scan_card(no, name)
                except Exception as e:
                    client.send(ack())
        except Exception as e:
            shutdown = False


if __name__ == "__main__":
    t = threading.Thread(target=main, args=())
    # t.start()
    main()
