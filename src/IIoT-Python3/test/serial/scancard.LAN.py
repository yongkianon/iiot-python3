import atexit
import configparser
import json
import logging
import os
import re
import socket
import sys
import time

import requests

BUZZ = bytes(b'\x00\x01\xff\xff')
CID1 = bytes(b'\x01\xff\xff\xff')


class ScanCard:
    name = ""
    imei = ""
    card = ""


def println(o):
    print(time.strftime("%Y-%m-%d %H:%M:%S ") + str(o))
    logging.info(str(o))


def delete_old_logs(days):
    path = "/opt/reader/logs"
    current_time = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < current_time - (days * 86400):
            os.remove(os.path.join(path, f))
            println("DELETED" + str(f))


def start_logger():
    filename = "/opt/reader/logs/" + time.strftime('%Y-%m-%d') + ".log"
    logging.basicConfig(filename=filename, filemode="a", format="%(asctime)s %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")
    delete_old_logs(365)


def shutdown_hook():
    println("TERMINATE " + str(sys.argv[1]).upper())


def numbers(s):
    rv = re.search(r"\d+", s)
    if rv is not None:
        return int(rv.group(0))


def http_post(url, msg):
    headers = {
        "Accept": "application/json",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
    }
    response = requests.post(url, headers=headers, data=msg)
    println("POST " + url)
    return str(response.status_code)


def scan_card(card, name):
    sc = ScanCard()
    sc.card = card
    sc.imei = ""
    sc.name = name
    msg = json.dumps(sc.__dict__)
    status = "TEST"
    # status = http_post("http://127.0.0.1:8080/rest/card/reader/scancard/post", msg)
    # status = http_post("http://172.21.77.19/rest/card/reader/scancard/post", msg)
    # status = http_post("http://172.21.95.190:8081/apps/pages/ttvmt.cardreader", msg)
    status = http_post("http://172.21.30.72/apps/pages/ttvmt.cardreader", msg)
    println("HTTP " + status + " " + msg + "\n")


def accept_port(server, name):
    client, address = server.accept()
    client.settimeout(1)
    client.send(BUZZ)
    client.send(CID1)
    res = client.recv(10).hex()
    println("Connected reader " + str(res) + " " + str(address))
    while True:
        try:
            res = client.recv(10).hex()  # timeout 1 second
            client.send(BUZZ)
            hex_data = res[12:20]
            no = int(hex_data, 16)
            println("LAN CARD " + hex_data + " " + str(no))
            scan_card(no, name)
        except Exception as e:
            client.send(CID1)
            client.recv(10)


def listen_port(port, name):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('', port))
    server.listen(5)
    server.settimeout(1)
    println(name + " listen on port " + str(port))
    while True:
        try:
            accept_port(server, name)
        except Exception as e:
            # println("ERROR " + str(e))
            time.sleep(10)


def main(name):
    # python scancard.py TEST card_no
    if len(sys.argv) == 3 and "TEST" == str(sys.argv[1]).upper():
        println(sys.argv[1] + " " + sys.argv[2])
        scan_card(sys.argv[2], "TEST")
        os._exit(0)

    listen_port(50000, name)


if __name__ == "__main__":
    start_logger()
    atexit.register(shutdown_hook)
    config = configparser.ConfigParser()
    config.read(r"/opt/reader/config/config.ini")
    name = config.get("name", "name.id")
    main(name)
