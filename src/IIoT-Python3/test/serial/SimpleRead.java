import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

public class SimpleRead implements SerialPortEventListener {
    private InputStream in;

    public static void main(String[] args) {
        CommPortIdentifier portId = null;
        boolean found = false;
        String defaultPort = "/dev/ttyS0";

        if (args.length == 1) defaultPort = args[0];

        Enumeration portIds = CommPortIdentifier.getPortIdentifiers();
        while (portIds.hasMoreElements()) {
            portId = (CommPortIdentifier) portIds.nextElement();
            println(portId.getName() + " " + portId.getPortType());
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(defaultPort)) {
                    println("Found " + defaultPort);
                    found = true;
                    break;
                }
            }
        }

        if (found) {
            println("Read  " + defaultPort);
            new SimpleRead(portId);
        } else {
            println("port " + defaultPort + " not found.");
        }
    }


    public SimpleRead(CommPortIdentifier portId) {
        SerialPort comm;
        try {
            comm = (SerialPort) portId.open("SimpleReadApp", 2000);
            comm.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            in = comm.getInputStream();
            comm.addEventListener(this);
            comm.notifyOnDataAvailable(true);
        } catch (Exception e) {
            println(e.getMessage());
        }
    }

    public void serialEvent(SerialPortEvent event) {
        byte[] buffer = new byte[1024];
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                println("ETC");
                break;

            case SerialPortEvent.DATA_AVAILABLE:
                try {
                    while (in.available() > 0) {
                        int size = in.read(buffer);
                    }
                    println(new String(buffer));
                } catch (IOException e) {
                    println("EIR");
                }
                break;

            default:
                throw new IllegalStateException("Unexpected event " + event.getEventType());
        }
    }

    private static void println(Object o) {
        System.out.println(o);
    }
}
