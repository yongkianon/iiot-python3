import platform
import sys

import cv2
import tensorflow as tf

if tf.test.is_gpu_available():
    print("tensorflow " + tf.__version__ + " with GPU")
else:
    print("tensorflow " + tf.__version__ + " without GPU")
print("opencv     " + cv2.__version__)
print("platform   " + platform.python_version())
print("python     " + sys.version)
print("python     " + str(sys.version_info))
print("where      " + sys.executable)
