from wrapt_timeout_decorator import *
from time import sleep


# use_signals = False is not really necessary here, it is set automatically under Windows
# but You can force NOT to use Signals under Linux
@timeout(5, use_signals=False)
def test():
    print("TEST timeout")
    count = -1
    for i in range(1, 10):
        sleep(1)
        print("{} seconds have passed".format(i))
        count = i
    return count
