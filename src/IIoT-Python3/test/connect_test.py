import time

import OpenOPC
import pywintypes

pywintypes.datetime = pywintypes.TimeType

opc = OpenOPC.client()


def connect_till_success():
    success = False
    while not success:
        try:
            opc.connect("Prosys.OPC.Simulation")
            success = True
        except Exception as ex:
            print("ERROR connect failed")


try:
    connect_till_success()
    while True:
        try:
            # C:/OpenOPC/bin/opc.exe -f
            print(opc.list(), end="\n")
            print(opc.read("Static.PsInteger1"))
            print(opc.read("Random.PsInteger1"))
            time.sleep(1)
            print(opc.read("Random.PsInteger1"))
            exit(0)
        except Exception as e:
            print("ERROR ", e)
            connect_till_success()
        time.sleep(1)
finally:
    opc.close()
