import argparse
import logging
import os
import threading
import time
from datetime import datetime
from tkinter import *

import requests
from requests.auth import HTTPDigestAuth

axis_camera = False
auto_mode = False
zoom = 999
root = Tk()
btn_azmz = None
ico_left = PhotoImage(file="left.png").subsample(12, 12)
ico_home = PhotoImage(file="home.png").subsample(12, 12)
ico_right = PhotoImage(file="right.png").subsample(12, 12)
ico_up = PhotoImage(file="up.png").subsample(12, 12)
ico_down = PhotoImage(file="down.png").subsample(12, 12)
ico_zoom_in = PhotoImage(file="zoom-in.png").subsample(12, 12)
ico_zoom_out = PhotoImage(file="zoom-out.png").subsample(12, 12)


def delete_old_logs(days):
    path = "/opt/tomcat/logs"
    current_time = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < current_time - (days * 86400):
            os.remove(os.path.join(path, f))
            print("DELETED", f)


def start_logger():
    file = fr"/opt/tomcat/logs/cpanel_{datetime.now().strftime('%Y-%m-%d')}.log"
    print(file)
    logging.basicConfig(filename=file, filemode="a", format="%(asctime)s %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")
    delete_old_logs(30)


def info(s):
    print(time.strftime("%Y-%m-%d %H:%M:%S ") + s)
    logging.info(s)


def conn():
    # return cv2.VideoCapture("rtsp://root:pass@192.168.0.90/axis-media/media.amp")
    info("connected")


def http_get(url):
    try:
        headers = {
            "Accept": "*/*",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Content-Type": "application/json; text/plain",
        }
        res = requests.get(url, auth=HTTPDigestAuth("root", "Admin123"), headers=headers)
        info(url + " " + str(res.status_code))
        return res
    except Exception as e:
        info("ERROR " + url + " " + str(e))


def axis_control(url):
    http_get(url)
    time.sleep(0.5)
    http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,0")


def axis_zoom():
    global auto_mode, zoom
    value = -9
    url = "http://172.21.77.19/rest/pdm/crane/plc/get/C45/D_HOIST_POSITION"
    while auto_mode:
        time.sleep(2)
        try:
            res = http_get(url)
            if res.status_code != 200:
                info("ERROR " + str(res.status_code) + " " + res.content.decode())
            else:  # res.status_code == 200:
                s = res.content.decode()
                if s is None or s == "None" or s == "":
                    info(url + " " + s)
                else:
                    value = float(s) / 100
                    if zoom != 800 and 1 < value < 21:
                        zoom = 800
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 300 and 21 < value < 30:
                        zoom = 300
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 100 and 30 < value < 100:
                        zoom = 100
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 1000 and 400000 < value:  # below sea level
                        zoom = 1000
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                info("Z=" + str(zoom) + " " + str(value))
        except Exception as e:
            info("ERROR " + url + " " + str(e))


def start_auto_zoom():
    if axis_camera:
        t = threading.Thread(target=axis_zoom, args=())
    else:
        t = threading.Thread(target=axis_zoom, args=())
    t.start()


def axis_thread(url):
    t = threading.Thread(target=axis_control, args=(url,))
    t.start()


def start_thread(url):
    t = threading.Thread(target=http_get, args=(url,))
    t.start()


def go_ul():
    start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=-1&tilt=-1&Language=0")


def go_up():
    if axis_camera:
        axis_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,50")
    else:
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=0&tilt=-1&Language=0")


def go_ur():
    start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=1&tilt=-1&Language=0")


def go_left():
    if axis_camera:
        axis_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=-50,0")
    else:
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=-1&tilt=0&Language=0")


def go_right():
    if axis_camera:
        axis_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=50,0")
    else:
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=1&tilt=0&Language=0")


def go_bl():
    start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=-1&tilt=1&Language=0")


def go_down():
    if axis_camera:
        axis_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,-50")
    else:
        start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=0&tilt=1&Language=0")


def go_br():
    start_thread("http://192.168.0.90/cgi-bin/camctrl?pan=1&tilt=1&Language=0")


def zoom_out():
    global zoom
    if zoom > 100:
        zoom = zoom - 100
        start_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))


def zoom_in():
    global zoom
    if zoom < 30000:
        zoom = zoom + 100
        start_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))


def on_close():
    global root
    root.quit()
    info("closing...")
    os._exit(0)


def go_home():
    start_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&gotoserverpresetno=1")


def go_reset():
    start_thread("http://192.168.0.90/axis-cgi/com/ptz.cgi?auxiliary=reset")


def switch_az_mz():
    global auto_mode
    global btn_azmz
    if auto_mode:
        auto_mode = False
        info("Manual Zoom")
        btn_azmz["text"] = "AUTO"
    else:
        auto_mode = True
        info("Auto Zoom")
        start_auto_zoom()
        btn_azmz["text"] = "MANUAL"


start_logger()
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", required=False, help="path to directory to store snapshots")
ap.add_argument("-p", "--camera", type=int, default=-1, help="whether or not the Raspberry Pi camera should be used")
args = vars(ap.parse_args())
# initialize the video stream and allow the camera sensor to warmup
info("starting...")
time.sleep(1)
# start the app
conn()
start_auto_zoom()
try:
    root.attributes("-topmost", True)
    root.attributes('-toolwindow', True)
    # root.overrideredirect(1)
    root.resizable(False, False)

    btn_reset = Button(root, text="RESET", command=go_reset)
    btn_azmz = Button(root, text="MANUAL", command=switch_az_mz)
    btn_home = Button(root, text="HOME", command=go_home, image=ico_home)

    # btn_ul = Button(root, text="01 UL", command=go_ul)
    btn_up = Button(root, text="UP", command=go_up, image=ico_up)
    # btn_ur = Button(root, text="03 UR", command=go_ur)

    btn_left = Button(root, text="LEFT", command=go_left, image=ico_left)
    btn_right = Button(root, text="RIGHT", command=go_right, image=ico_right)

    # btn_bl = Button(root, text="06 BL", command=go_bl)
    btn_down = Button(root, text="DOWN", command=go_down, image=ico_down)
    # btn_br = Button(root, text="08 BR", command=go_br)

    btn_zin = Button(root, text="ZOOM +", command=zoom_in, image=ico_zoom_in)
    btn_zout = Button(root, text="ZOOM -", command=zoom_out, image=ico_zoom_out)

    btn_reset.grid(row=1, column=1)
    btn_azmz.grid(row=3, column=1)

    btn_zout.grid(row=1, column=5)
    btn_zin.grid(row=3, column=5)

    # btn_ul.grid(row=1, column=2)
    btn_up.grid(row=1, column=3)
    # btn_ur.grid(row=1, column=4)

    btn_left.grid(row=2, column=2)
    btn_home.grid(row=2, column=3)
    btn_right.grid(row=2, column=4)

    # btn_bl.grid(row=3, column=2)
    btn_down.grid(row=3, column=3)
    # btn_br.grid(row=3, column=4)

    # start a thread that constantly pools the video sensor for the most recently read frame
    stop_event = threading.Event()
    # set a callback to handle when the window is closed
    root.wm_title("AZ")
    root.wm_protocol("WM_DELETE_WINDOW", on_close)
    mainloop()
except Exception as e:
    info("ERROR " + str(e))
finally:
    os._exit(0)
