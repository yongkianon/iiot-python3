import logging
import os
import time
from datetime import datetime

import requests
from requests.auth import HTTPBasicAuth
from requests.auth import HTTPDigestAuth


def delete_old_logs(days):
    path = "/opt/tomcat/logs"
    current_time = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < current_time - (days * 86400):
            os.remove(os.path.join(path, f))
            print("DELETED", f)


def start_logger():
    file = fr"/opt/tomcat/logs/axis_{datetime.now().strftime('%Y-%m-%d')}.log"
    print(file)
    logging.basicConfig(filename=file, filemode="a", format="%(asctime)s %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")
    delete_old_logs(30)


def info(s):
    print(time.strftime("%Y-%m-%d %H:%M:%S ") + s)
    logging.info(s)


def http_get_auth_digest(url):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
    }
    return requests.get(url, auth=HTTPDigestAuth("root", "pass"), headers=headers)


def http_get_auth_basic(url, msg):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
    }
    return requests.get(url, auth=HTTPBasicAuth("root", "pass"), headers=headers, data=msg)


def go_to(x, y):
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&center=" + str(x) + "%2C" + str(y) + "&imagewidth=1280&imageheight=720&imagerotation=0")


def go_down(v):
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,-" + str(v))
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,0")


def go_up(v):
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0," + str(v))
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,0")


def go_left(v):
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=-" + str(v) + ",0")
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,0")


def go_right(v):
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=" + str(v) + ",0")
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,0")


def test(url):
    res = http_get_auth_digest(url)
    # for key in res.headers:
    #     print(key + ": " + res.headers[key])
    info(res.status_code)
    print(res.content.decode())


def main(url):
    value = -9
    zoom = 999
    while True:
        time.sleep(2)
        try:
            res = http_get_auth_digest(url)
            if res.status_code != 200:
                info("ERROR " + str(res.status_code) + " " + res.content.decode())
            else:  # res.status_code == 200:
                s = res.content.decode()
                if s is None or s == "None" or s == "":
                    info(url + " " + s)
                else:
                    value = float(s) / 100
                    if zoom != 800 and 1 < value < 21:
                        zoom = 800
                        http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 300 and 21 < value < 30:
                        zoom = 300
                        http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 100 and 30 < value < 100:
                        zoom = 100
                        http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 1000 and 400000 < value:  # below sea level
                        zoom = 1000
                        http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                info("Z=" + str(zoom) + " " + str(value))
        except Exception as e:
            info("ERROR " + str(e))


if __name__ == '__main__':
    start_logger()
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?auxiliary=reset")
    http_get_auth_digest("http://192.168.0.90/axis-cgi/com/ptz.cgi?move=home")
    time.sleep(2)
    # test("http://192.168.1.102/axis-cgi/param.cgi?action=list&group=Properties.PTZ.PTZ")
    # test("http://192.168.1.102/axis-cgi/com/ptz.cgi?query=position")
    main("http://172.21.77.19/rest/pdm/crane/plc/get/C48/D_HOIST_POSITION")
