import os
import random
import sys
import time

cmd = "ffmpeg -rtsp_transport tcp -i rtsp://root:pass@192.168.0.90/axis-media/media.amp -vframes 1 -q:v 2 "
if len(sys.argv) == 2 and sys.argv[1].lower() == "loop":
    while True:
        cmd = "ffmpeg -rtsp_transport tcp -i rtsp://root:pass@192.168.0.90/axis-media/media.amp -vframes 1 -q:v 2 " + time.strftime("%Y.%m.%d.%H%M%S") + ".jpg"
        os.system(cmd)
        time.sleep(random.randint(5, 11) * random.randint(1, 5))
elif len(sys.argv) == 2:
    cmd = cmd + str(sys.argv[1]) + ".jpg"
else:
    cmd = cmd + time.strftime("%Y.%m.%d.%H%M%S") + ".jpg"

print(cmd)
os.system(cmd)
