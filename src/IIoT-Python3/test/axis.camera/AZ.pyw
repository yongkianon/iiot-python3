import atexit
import logging
import os
import threading
import time
from datetime import datetime

import cv2
import requests
from requests.auth import HTTPDigestAuth

axis_camera = False
debugMode = False
exit_mode = False
hoist = -9
text = ""


def shutdown_hook():
    global exit_mode
    exit_mode = True
    info("Exit")


def start_logger():
    file = fr"/opt/tomcat/logs/camera_{datetime.now().strftime('%Y-%m-%d')}.log"
    print(file)
    logging.basicConfig(filename=file, filemode="a", format="%(asctime)s %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")


def info(s):
    logging.info(s)
    print(time.strftime("%Y-%m-%d %H:%M:%S ") + s)


def http_get(url):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
    }
    return requests.get(url, auth=HTTPDigestAuth("root", "Admin123"), headers=headers)


def axis_zoom():
    global hoist
    hoist = -9
    zoom = 999
    url = "http://172.21.77.19/rest/pdm/crane/plc/get/C45/D_HOIST_POSITION"
    while not exit_mode:
        time.sleep(2)
        try:
            res = http_get(url)
            if res.status_code != 200:
                info("ERROR " + str(res.status_code) + " " + res.content.decode())
            else:  # res.status_code == 200:
                s = res.content.decode()
                if s is None or s == "None" or s == "":
                    info(url + " " + s)
                else:
                    hoist = float(s) / 100
                    if zoom != 800 and 1 < hoist < 21:
                        zoom = 800
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 300 and 21 < hoist < 30:
                        zoom = 300
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 100 and 30 < hoist < 100:
                        zoom = 100
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
                    elif zoom != 1000 and 400000 < hoist:  # below sea level
                        zoom = 1000
                        http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?zoom=" + str(zoom))
            if debugMode:
                info("Z=" + str(zoom) + " " + str(hoist))
        except Exception as e:
            info("ERROR " + str(e))


def pana_zoom():
    global hoist
    hoist = -9
    zoom = 999
    url = "http://172.21.77.19/rest/pdm/crane/plc/get/C45/D_HOIST_POSITION"
    while not exit_mode:
        time.sleep(2)
        try:
            res = http_get(url)
            if res.status_code != 200:
                info("ERROR " + str(res.status_code) + " " + res.content.decode())
            else:  # res.status_code == 200:
                s = res.content.decode()
                if s is None or s == "None" or s == "":
                    info(url + " " + s)
                else:
                    hoist = float(s) / 100
                    if zoom != 2.6 and 1 < hoist < 21:
                        zoom = 2.6
                        http_get("http://192.168.0.90/cgi-bin/directctrl?zoom=" + str(zoom))
                        http_get("http://192.168.0.90/cgi-bin/directctrl?zoom=0")
                    elif zoom != 2 and 21 < hoist < 30:
                        zoom = 2
                        http_get("http://192.168.0.90/cgi-bin/directctrl?zoom=" + str(zoom))
                        http_get("http://192.168.0.90/cgi-bin/directctrl?zoom=0")
                    elif zoom != 0 and 30 < hoist < 100:
                        zoom = 0
                        http_get("http://192.168.0.90/cgi-bin/camctrl?zoom=0")
                    elif zoom != 4 and 400000 < hoist:  # below sea level
                        zoom = 4
                        http_get("http://192.168.0.90/cgi-bin/directctrl?zoom=" + str(zoom))
                        http_get("http://192.168.0.90/cgi-bin/directctrl?zoom=0")
            if debugMode:
                info("Z=" + str(zoom) + " " + str(hoist))
        except Exception as e:
            info("ERROR " + str(e))


def pana_txt_view():
    global text, hoist
    text = "HOIST " + str(hoist) + " m"
    url = "http://172.21.77.19/rest/pdm/crane/plc/get/C45"
    while not exit_mode:
        time.sleep(2)
        try:
            res = http_get(url)
            if res.status_code != 200:
                info("ERROR " + str(res.status_code) + " " + res.content.decode())
            else:  # res.status_code == 200:
                text = res.content.decode()
                if text is None or text == "None" or text == "":
                    info(url + " " + text)
                    text = "HOIST " + str(hoist) + " m"
        except Exception as e:
            info("ERROR " + str(e))


def start_auto_zoom():
    if axis_camera:
        t = threading.Thread(target=axis_zoom, args=())
    else:
        threading.Thread(target=pana_txt_view, args=()).start()
        t = threading.Thread(target=pana_zoom, args=())
    t.start()


def adjust_area(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        url = "http://192.168.0.90/axis-cgi/com/ptz.cgi?camera=1&center=" + str(int(x)) + "," + str(int(y)) + "&imagewidth=1920&imageheight=1080&imagerotation=0"
        info(url)
        http_get(url)


start_logger()
atexit.register(shutdown_hook)
if axis_camera:
    http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?auxiliary=reset")
    http_get("http://192.168.0.90/axis-cgi/com/ptz.cgi?move=home")
time.sleep(1)
start_auto_zoom()
font = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1
color = (255, 255, 255)
thickness = 2
org = (1024, 50)
y0, dy = 50, 40
while not exit_mode:
    try:
        if axis_camera:
            vs = cv2.VideoCapture("rtsp://root:pass@192.168.0.90/axis-media/media.amp")
        else:
            vs = cv2.VideoCapture("rtsp://root:Admin123@192.168.0.90/MediaInput/h264/stream_1")
        cv2.namedWindow("AZ", cv2.WINDOW_FULLSCREEN)
        cv2.moveWindow("AZ", 0, 0)
        if axis_camera:
            cv2.setMouseCallback("AZ", adjust_area)
        while vs.isOpened():
            ret, frame = vs.read()
            for i, line in enumerate(text.split("\n")):
                y = y0 + i * dy
                frame = cv2.putText(frame, line, (1024, y), font, fontScale, color, thickness, cv2.LINE_AA, False)
            cv2.imshow("AZ", frame)
            # cv2.createButton("Back", back, None, cv2.QT_PUSH_BUTTON, 1)  # NOK
            key = cv2.waitKey(20) & 0xFF
            if key == ord("q") or key == ord("x"):  # ESC|q|x
                os._exit(0)  # break
        vs.release()
    except Exception as e:
        info("Error " + str(e))
    finally:
        exit_mode = True
        cv2.destroyAllWindows()
