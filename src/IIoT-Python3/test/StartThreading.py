import os
import threading
import time

from datetime import datetime


def println(str):
    print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), str)


def run_thread(count):
    # println("\tThread :  starting thread {}".format(count))
    time.sleep(5)
    println("\tThread : finished thread {}".format(count))


def start_thread():
    t = threading.Thread(target=run_thread, args=(count_sec,))
    t.start()
    # t.join(timeout=3)


if __name__ == "__main__":
    count_sec = 1
    while count_sec <= 3:
        println(" MAIN  : T-{}".format(count_sec))
        start_thread()
        count_sec = count_sec + 1
    println("Exiting")
    os._exit(0)
    println("Exited")
