import atexit
import configparser
import json
import logging
import math
import os
import sys
import threading
import time
import tracemalloc
# win32timezone *MUST* import for PyInstaller AND run in EXE mode
import win32timezone, datetime
from datetime import datetime

import OpenOPC
import pywintypes
import requests

pywintypes.datetime = pywintypes.TimeType
opc = OpenOPC.client()

# global variable
last_alert_none_sec = 0
exit_mode = False
test_mode = False
MAX_TIMES = 60
dictTag = {}
factors = {}
data_dt = {}
mem_data = {}
pdmTags = []
rpsTags = []
cmpTags = []

crane_id = "ID"
opc_server = ""

# web configuration
PDM_HOST = "172.21.77.19"
PDM_URL = "http://172.21.77.19/rest/pdm/crane/plc/put/data"

RPS_HOST = "172.21.30.72"
RPS_URL = "http://172.21.30.72/apps/pages/qc.plc.rest&type=wincc"

now = datetime.now()
logFileName = f"app_{now.strftime('%Y%m%d')}.log"
logFileName = fr"logs/{logFileName}"
cfgFileName = r"config/cfgTags.csv"


def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False


def over_flow(value):
    # 2^32 === 4294967296
    value = float(value)
    if value > 4200000000:
        return value - 4294967296
    else:
        return value


def delete_old_logs(days):
    path = "logs"
    current_time = time.time()
    for f in os.listdir(path):
        if os.stat(os.path.join(path, f)).st_mtime < current_time - (days * 86400):
            os.remove(os.path.join(path, f))
            print("DELETED", f)


def start_logger():
    logging.basicConfig(filename=logFileName, filemode="a", format="%(asctime)s-%(message)s", level=logging.INFO, datefmt="%d-%b-%y %H:%M:%S")
    delete_old_logs(10)


def shutdown_hook():
    exit_mode = True
    try:
        print("DEBUG OPC closing... ", end="")
        opc.close()
        print("Closed")
        print("TERMINATE OPC Server")
        logging.info("TERMINATE OPC Server")
    except Exception as e:
        print("ERROR ", e)
        logging.error("ERROR %s", e)


def telegram_bot(msg):
    token = "1889488832:AAHnoDK0zGJo_cDGK8kMi8Dn0cEk7Madu6k"
    chat_id = "@plcalert"
    try:
        url = "https://api.telegram.org/bot" + token + "/sendMessage?chat_id=" + chat_id + "&parse_mode=Markdown&text=" + msg
        response = requests.get(url)
        logging.info(response.json())
    except Exception as e:
        logging.error("ERROR %s", e)


def alert_none_data():
    global last_alert_none_sec
    if time.time() - last_alert_none_sec > 60:
        last_alert_none_sec = time.time()
        telegram_bot(time.strftime("%Y-%m-%d %H:%M:%S ") + crane_id + " data None")


def read_file(filename):
    content = []
    file = None
    try:
        file = open(filename, "r")
        content = file.readlines()
    except Exception as e:
        logging.error("ERROR reading file %s %s", filename, e)
    finally:
        if file is not None:
            file.close()
    return content


def connect_retry_3():
    count_conn = 1
    success = False
    while not success and count_conn <= 3:
        try:
            opc.connect(opc_server)
            success = True
            print("CONNECTED OPC Server")
            logging.info("CONNECTED OPC Server")
        except Exception as ex:
            count_conn = count_conn + 1
            print("ERROR connect failed retry-", count_conn)
            logging.error("ERROR connect failed retry-", count_conn)
            time.sleep(5)


def read_cfg_tag():
    for line in read_file(cfgFileName):
        line = line.strip()
        if not line.startswith("#"):
            values = line.split(",")
            key = values[2].strip()
            hdr = values[0].strip()
            dictTag[key] = values[1].strip()
            factors[key] = values[3].strip()
            if hdr == "CMP":
                cmpTags.append(key)
                rpsTags.append(key)
            if hdr == "PDM":
                pdmTags.append(key)
            if hdr == "RPS":
                rpsTags.append(key)
    # print("DICTTAG\n", dictTag)
    # print("FACTORS\n", factors)


def read_rps():
    total_value = 0.0
    print("DEBUG before... ", end="")
    datas = opc.read(group="RPS")
    print("after reading")
    # print("READ\n", datas, "\n\n")
    if len(datas) == 0:
        logging.error("ERROR READ EMPTY data, RE-CONNECT AGAIN")
        opc.remove("RPS")
        opc.remove("PDM")
        time.sleep(1)
        connect_retry_3()
        datas = opc.read(tags=rpsTags, group="RPS", update=500)
        # opc.read(tags=pdmTags, group="PDM", update=999)
    try:
        for data in datas:
            (key, val, good, date_time) = data
            mem_data[key] = val
            data_dt[key] = date_time
            if key in cmpTags:
                # print(key, val)
                if val is not None:
                    total_value += float(val)
    except Exception as e:
        print("ERROR READ data ", e)
        logging.error("ERROR READ data %s", e)
        # raise Exception("while reading data. Retry connection")
    # print("MEM\n", mem_data)
    return total_value


def http_get(url, msg):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
    }
    response = requests.get(url, headers=headers, data=msg)
    return response.status_code


def http_put(url, msg):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
    }
    response = requests.put(url, headers=headers, data=msg)
    return response.status_code


def rps_put(msg):
    headers = {
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Content-Type": "application/json; text/plain",
        "Host": RPS_HOST,
    }
    response = requests.put(RPS_URL, headers=headers, data=msg)
    return response.status_code


def http_put_thread(url, msg):
    try:
        print("\tThread starting... ", end="")
        t = threading.Thread(target=http_put, args=(url, msg,))
        t.start()
        print("STARTED")
    except Exception as e:
        print("ERROR ", e)


def start_rps_thread(msg):
    try:
        print("\tThread starting... ", end="")
        t = threading.Thread(target=rps_put, args=(msg,))
        t.start()
        print("STARTED")
    except Exception as e:
        print("ERROR ", e)


def internal_calc():
    # calc or conversion here
    # mem_data["TCP CHANNEL>SIEMENS_M_OPC>DB901.bo_act_angle_encoder"] = mem_data["TCP CHANNEL>SIEMENS_M_OPC>DB2000.SKEWDEG90"] - mem_data["TCP CHANNEL>SIEMENS_M_OPC>DB2000.SKEWDEG"]
    mem_data["TCP CHANNEL>SIEMENS_M_OPC>DB300.TRposi_mm"] = (mem_data["TCP CHANNEL>SIEMENS_M_OPC>DB300.TRposi_mm"] * 0.00033854) - 24.68794


def rps_send():
    mode = "TEST"
    status_code = "200"
    fields = []
    try:
        # internal_calc()
        for key in mem_data:
            value = str(mem_data[key])
            if value is None:
                # alert_none_data()
                print("value=None ", end="")
            if isfloat(value):
                value = over_flow(value)
            if factors[key] != "999" and value is not None:
                value = float(factors[key]) * over_flow(value)
            fields.append({"Crane": crane_id, "N": dictTag[key], "V": value})
        print("DEBUG before... ", end="")
        msg = json.dumps(fields)
        print("after DUMP")
        fields.clear()
        if not test_mode:
            mode = "HTTP"
            print("DEBUG before RPS_PUT")
            # status_code = rps_put(msg)
            start_rps_thread(msg)
            print("DEBUG after  RPS_PUT")
        print(mode, status_code, msg)
    except Exception as e:
        logging.error("ERROR %s", e)
        logging.error(str(mem_data))


def pdm_send():
    mode = "TEST"
    status_code = "200"
    pdm_data = {}
    fields = []
    try:
        for key in pdmTags:
            pdm_data[key] = str(mem_data[key])
        for key in pdm_data:
            value = str(pdm_data[key])
            if factors[key] != "999" and value is not None:
                value = float(factors[key]) * over_flow(value)
            fields.append({"Crane": crane_id, "N": dictTag[key], "V": value})
        msg = json.dumps(fields)
        fields.clear()
        if not test_mode:
            mode = "HTTP"
            # status_code = http_put(PDM_URL, msg)
            http_put_thread(PDM_URL, msg)
        print(mode, status_code, msg)
    except Exception as e:
        logging.error("ERROR %s", e)


def print_mem():
    snapshot = tracemalloc.take_snapshot()
    top_stats = snapshot.statistics("lineno")
    logging.info("[ Top 10 ]")
    for stat in top_stats[:10]:
        logging.info(stat)


def start_rps_read_60():
    count_sec = 0
    while not exit_mode and count_sec < MAX_TIMES:
        try:
            value1 = read_rps()
            mem_data.clear()
            time.sleep(1)
            value2 = read_rps()
            diff = math.fabs(value2 - value1)
            print("VALUE DIFFERENCE ", diff)
            if diff > 0.01:
                rps_send()
                # pdm_send()
        except Exception as e:
            print("ERROR ", e)
            logging.error("ERROR %s", e)
            # NOK could not remove
            # opc.remove("RPS")
            time.sleep(1)
            connect_retry_3()
            opc.read(tags=rpsTags, group="RPS", update=500)
            # opc.read(tags=pdmTags, group="PDM", update=999)
        count_sec = count_sec + 1
        print("Second", count_sec, "\n")


# main program
# print(sys.argv)
config = configparser.ConfigParser()
config.read(r"config/config.ini")
crane_id = config.get("crane", "crane.id")
opc_server = config.get("opc", "opc.server")

if len(sys.argv) == 2 and "test" == str(sys.argv[1]).lower():
    test_mode = True
if len(sys.argv) == 3 and "test" == str(sys.argv[1]).lower():
    test_mode = True
    MAX_TIMES = int(sys.argv[2])

# tracemalloc.start()
start_logger()
atexit.register(shutdown_hook)
read_cfg_tag()
connect_retry_3()
opc.read(tags=rpsTags, group="RPS", update=500)
# opc.read(tags=pdmTags, group="PDM", update=999)
count_min = 0
while not exit_mode:
    try:
        start_rps_read_60()
        # must send in every 60 seconds
        rps_send()
        # pdm_send()
    except Exception as e:
        print("ERROR ", e)
        logging.error("ERROR %s", e)
    count_min = count_min + 1
    if count_min >= MAX_TIMES:
        shutdown_hook()
        logging.info("EXIT")
        os._exit(0)
# ########################End#####################################
