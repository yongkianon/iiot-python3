import time

import cv2
import numpy as np
import tensorflow as tf
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

# path to the frozen graph:
PATH_TO_FROZEN_GRAPH = "frozen_inference_graph.pb"

# path to the label map
PATH_TO_LABEL_MAP = "label_map.pbtxt"

NUM_CLASSES = 1
# /ML/TF/spreader/2021.02.16.01.night.OK.mp4
vs = cv2.VideoCapture("video/V003.mp4")
cv2.namedWindow("CAMERA", cv2.WINDOW_FULLSCREEN)
cv2.moveWindow("CAMERA", 0, 0)
vs.set(cv2.CAP_PROP_POS_AVI_RATIO, 0)
start = time.time()

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, "rb") as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name="")

label_map = label_map_util.load_labelmap(PATH_TO_LABEL_MAP)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

flag = 0
with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        while True:
            ret, frame = vs.read()
            # outer blue coloured rectangle
            image_np_expanded = np.expand_dims(frame, axis=0)
            image_tensor = detection_graph.get_tensor_by_name("image_tensor:0")
            boxes = detection_graph.get_tensor_by_name("detection_boxes:0")
            scores = detection_graph.get_tensor_by_name("detection_scores:0")
            classes = detection_graph.get_tensor_by_name("detection_classes:0")
            num_detections = detection_graph.get_tensor_by_name("num_detections:0")
            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})
            vis_util.visualize_boxes_and_labels_on_image_array(
                frame,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=3,
            )
            width, height = frame.shape[:2]
            # print(height)
            '''
            ymin = int((boxes[0][0][0]*height))
            xmin = int((boxes[0][0][1]*width))
            ymax = int((boxes[0][0][2]*height))
            xmax = int((boxes[0][0][3]*width))
            Result = np.array(frame[ymin:ymax,xmin:xmax])
            '''
            box = np.squeeze(boxes)
            for i in range(len(boxes)):
                xmin = (int(box[i, 0] * width))
                ymin = (int(box[i, 1] * height))
                xmax = (int(box[i, 2] * width))
                ymax = (int(box[i, 3] * height))
            # print(ymin, xmin, ymax, xmax)
            roi = frame[ymin:ymax, xmin:xmax].copy()
            offset_upper = xmin
            offset_lower = height - xmax
            # if offset_upper < 100 or offset_lower < 100:
            # print(time.strftime("%Y-%m-%d %H:%M:%S ") + "out of the frame")
            '''
            for i in range(0,len(Result)):
                if i==0 and flag==0:
                    flag=1
                    print(Result[i])
                else:
                    break
            '''
            cv2.imshow("CAMERA", cv2.resize(frame, (1280, 720)))
            if cv2.waitKey(10) & 0xFF == ord("q"):
                break

        diff = time.time() - start
        ms = vs.get(cv2.CAP_PROP_POS_MSEC)

        fps = vs.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
        total_frame = int(vs.get(cv2.CAP_PROP_FRAME_COUNT))
        duration = total_frame / fps
        minutes = int(duration / 60)
        seconds = duration % 60

        print("      fps        = " + str(fps))
        print("total frames     = " + str(total_frame))
        print("duration (S)     = " + str(duration) + " ms= " + str(ms) + " diff=" + str(diff))
        print("duration (MM:SS) = " + str(minutes) + ":" + str(seconds))
        vs.release()
        cv2.destroyAllWindows()
